//#define __AR_JIN__

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.IO;
using System.Data;

namespace MyMP3_X1
{
	/// <summary>
	/// Summary description for EditList.
	/// </summary>
	public class EditList : System.Windows.Forms.Form
	{
		private int m_iListViewSelected = -1; // ����Ң�й�����͡ index ����� listview
		private ListViewColumnSorter lvwColumnSorter = null;
		System.Collections.IDictionaryEnumerator idic = null;
		bool m_bLoadFirst = false;
		bool m_bSaveNExit = false; // ��Ǩ������͡�¡�� save ��͹�������
		int m_Count = 0;
		private System.Windows.Forms.ListView listView1;
		private System.Windows.Forms.ColumnHeader columnHeader1;
		private System.Windows.Forms.ColumnHeader columnHeader2;
		private System.Windows.Forms.ColumnHeader columnHeader3;
		private System.Windows.Forms.ColumnHeader columnHeader4;
		private System.Windows.Forms.ColumnHeader columnHeader5;
		private System.Windows.Forms.ColumnHeader columnHeader6;
		private System.Windows.Forms.ColumnHeader columnHeader7;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.NumericUpDown numericUpDown3;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox textBox4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox textBox3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.NumericUpDown numericUpDown1;
		private System.Windows.Forms.TextBox textBox2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.NumericUpDown numericUpDown2;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.CheckBox checkBox2;
		private System.Windows.Forms.ColumnHeader columnHeader8;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.Button button4;
		private System.Windows.Forms.ProgressBar progressBar1;
		private System.Windows.Forms.Timer timer1;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.ColumnHeader columnHeader9;
		private System.Windows.Forms.ColumnHeader columnHeader10;
		private System.Windows.Forms.ColumnHeader columnHeader11;
		private System.Windows.Forms.ColumnHeader columnHeader12;
		private System.Windows.Forms.ColumnHeader columnHeader13;
		private System.Windows.Forms.TextBox textBox6;
		private System.Windows.Forms.Label label10;
		private System.ComponentModel.IContainer components;

		public EditList()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			this.TransparencyKey = System.Drawing.Color.FromArgb(((System.Byte)(76)), ((System.Byte)(75)), ((System.Byte)(54)));
			
			lvwColumnSorter = new ListViewColumnSorter();
			this.listView1.ListViewItemSorter = lvwColumnSorter;
			this.listView1.Sorting = SortOrder.Ascending;
			this.listView1.AutoArrange = true;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.listView1 = new System.Windows.Forms.ListView();
			this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader4 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader5 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader6 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader7 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader8 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader9 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader10 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader11 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader12 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader13 = new System.Windows.Forms.ColumnHeader();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.label9 = new System.Windows.Forms.Label();
			this.numericUpDown3 = new System.Windows.Forms.NumericUpDown();
			this.label6 = new System.Windows.Forms.Label();
			this.textBox4 = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.textBox3 = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
			this.textBox2 = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.numericUpDown2 = new System.Windows.Forms.NumericUpDown();
			this.checkBox2 = new System.Windows.Forms.CheckBox();
			this.button2 = new System.Windows.Forms.Button();
			this.button3 = new System.Windows.Forms.Button();
			this.button4 = new System.Windows.Forms.Button();
			this.progressBar1 = new System.Windows.Forms.ProgressBar();
			this.timer1 = new System.Windows.Forms.Timer(this.components);
			this.button1 = new System.Windows.Forms.Button();
			this.textBox6 = new System.Windows.Forms.TextBox();
			this.label10 = new System.Windows.Forms.Label();
			this.groupBox3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).BeginInit();
			this.SuspendLayout();
			// 
			// listView1
			// 
			this.listView1.BackColor = System.Drawing.Color.LightCyan;
			this.listView1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.listView1.CheckBoxes = true;
			this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
																						this.columnHeader1,
																						this.columnHeader2,
																						this.columnHeader3,
																						this.columnHeader4,
																						this.columnHeader5,
																						this.columnHeader6,
																						this.columnHeader7,
																						this.columnHeader8,
																						this.columnHeader9,
																						this.columnHeader10,
																						this.columnHeader11,
																						this.columnHeader12,
																						this.columnHeader13});
			this.listView1.ForeColor = System.Drawing.Color.Blue;
			this.listView1.FullRowSelect = true;
			this.listView1.HideSelection = false;
			this.listView1.Location = new System.Drawing.Point(16, 44);
			this.listView1.MultiSelect = false;
			this.listView1.Name = "listView1";
			this.listView1.Size = new System.Drawing.Size(540, 188);
			this.listView1.Sorting = System.Windows.Forms.SortOrder.Ascending;
			this.listView1.TabIndex = 47;
			this.listView1.View = System.Windows.Forms.View.Details;
			this.listView1.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.onColumnClick);
			this.listView1.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
			// 
			// columnHeader1
			// 
			this.columnHeader1.Text = "�ӴѺ���";
			this.columnHeader1.Width = 50;
			// 
			// columnHeader2
			// 
			this.columnHeader2.Text = "�������";
			this.columnHeader2.Width = 0;
			// 
			// columnHeader3
			// 
			this.columnHeader3.Text = "����";
			this.columnHeader3.Width = 50;
			// 
			// columnHeader4
			// 
			this.columnHeader4.Text = "�����ŧ";
			this.columnHeader4.Width = 120;
			// 
			// columnHeader5
			// 
			this.columnHeader5.Text = "������ŻԹ";
			this.columnHeader5.Width = 120;
			// 
			// columnHeader6
			// 
			this.columnHeader6.Text = "��ź��";
			this.columnHeader6.Width = 120;
			// 
			// columnHeader7
			// 
			this.columnHeader7.Text = "����";
			this.columnHeader7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.columnHeader7.Width = 35;
			// 
			// columnHeader8
			// 
			this.columnHeader8.Text = "��ش";
			this.columnHeader8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.columnHeader8.Width = 35;
			// 
			// columnHeader9
			// 
			this.columnHeader9.Text = "�����˵�1";
			this.columnHeader9.Width = 0;
			// 
			// columnHeader10
			// 
			this.columnHeader10.Text = "�����˵�2";
			this.columnHeader10.Width = 0;
			// 
			// columnHeader11
			// 
			this.columnHeader11.Text = "�����˵�3";
			this.columnHeader11.Width = 0;
			// 
			// columnHeader12
			// 
			this.columnHeader12.Text = "�����˵�4";
			this.columnHeader12.Width = 0;
			// 
			// columnHeader13
			// 
			this.columnHeader13.Text = "�����˵�5";
			this.columnHeader13.Width = 0;
			// 
			// groupBox3
			// 
			this.groupBox3.BackColor = System.Drawing.Color.Transparent;
			this.groupBox3.Controls.AddRange(new System.Windows.Forms.Control[] {
																					this.label9,
																					this.numericUpDown3,
																					this.label6,
																					this.textBox4,
																					this.label5,
																					this.textBox3,
																					this.label4,
																					this.numericUpDown1,
																					this.textBox2,
																					this.label3,
																					this.label7,
																					this.numericUpDown2});
			this.groupBox3.ForeColor = System.Drawing.Color.Yellow;
			this.groupBox3.Location = new System.Drawing.Point(564, 44);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(220, 140);
			this.groupBox3.TabIndex = 46;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "[ �к��Ѵ����ŧ ]";
			// 
			// label9
			// 
			this.label9.ForeColor = System.Drawing.Color.FloralWhite;
			this.label9.Location = new System.Drawing.Point(124, 44);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(24, 16);
			this.label9.TabIndex = 96;
			this.label9.Text = "��ش";
			this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// numericUpDown3
			// 
			this.numericUpDown3.Location = new System.Drawing.Point(152, 40);
			this.numericUpDown3.Maximum = new System.Decimal(new int[] {
																		   999999,
																		   0,
																		   0,
																		   0});
			this.numericUpDown3.Name = "numericUpDown3";
			this.numericUpDown3.Size = new System.Drawing.Size(60, 20);
			this.numericUpDown3.TabIndex = 95;
			this.numericUpDown3.TextChanged += new System.EventHandler(this.onTextChanged);
			// 
			// label6
			// 
			this.label6.ForeColor = System.Drawing.Color.FloralWhite;
			this.label6.Location = new System.Drawing.Point(4, 116);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(52, 16);
			this.label6.TabIndex = 90;
			this.label6.Text = "��ź��";
			this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// textBox4
			// 
			this.textBox4.BackColor = System.Drawing.Color.LightCyan;
			this.textBox4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.textBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.textBox4.Location = new System.Drawing.Point(60, 112);
			this.textBox4.Name = "textBox4";
			this.textBox4.Size = new System.Drawing.Size(152, 20);
			this.textBox4.TabIndex = 30;
			this.textBox4.Text = "";
			this.textBox4.TextChanged += new System.EventHandler(this.onTextChanged);
			// 
			// label5
			// 
			this.label5.ForeColor = System.Drawing.Color.FloralWhite;
			this.label5.Location = new System.Drawing.Point(4, 92);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(52, 16);
			this.label5.TabIndex = 88;
			this.label5.Text = "��ŻԹ";
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// textBox3
			// 
			this.textBox3.BackColor = System.Drawing.Color.LightCyan;
			this.textBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.textBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.textBox3.Location = new System.Drawing.Point(60, 88);
			this.textBox3.Name = "textBox3";
			this.textBox3.Size = new System.Drawing.Size(152, 20);
			this.textBox3.TabIndex = 25;
			this.textBox3.Text = "";
			this.textBox3.TextChanged += new System.EventHandler(this.onTextChanged);
			// 
			// label4
			// 
			this.label4.ForeColor = System.Drawing.Color.FloralWhite;
			this.label4.Location = new System.Drawing.Point(4, 68);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(52, 16);
			this.label4.TabIndex = 86;
			this.label4.Text = "�����ŧ";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// numericUpDown1
			// 
			this.numericUpDown1.Location = new System.Drawing.Point(60, 16);
			this.numericUpDown1.Maximum = new System.Decimal(new int[] {
																		   999999,
																		   0,
																		   0,
																		   0});
			this.numericUpDown1.Name = "numericUpDown1";
			this.numericUpDown1.Size = new System.Drawing.Size(60, 20);
			this.numericUpDown1.TabIndex = 15;
			this.numericUpDown1.TextChanged += new System.EventHandler(this.onTextChanged);
			// 
			// textBox2
			// 
			this.textBox2.BackColor = System.Drawing.Color.LightCyan;
			this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.textBox2.Location = new System.Drawing.Point(60, 64);
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new System.Drawing.Size(152, 20);
			this.textBox2.TabIndex = 20;
			this.textBox2.Text = "";
			this.textBox2.TextChanged += new System.EventHandler(this.onTextChanged);
			// 
			// label3
			// 
			this.label3.ForeColor = System.Drawing.Color.FloralWhite;
			this.label3.Location = new System.Drawing.Point(4, 20);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(52, 16);
			this.label3.TabIndex = 83;
			this.label3.Text = "����";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label7
			// 
			this.label7.ForeColor = System.Drawing.Color.FloralWhite;
			this.label7.Location = new System.Drawing.Point(4, 44);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(52, 16);
			this.label7.TabIndex = 92;
			this.label7.Text = "����";
			this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// numericUpDown2
			// 
			this.numericUpDown2.Location = new System.Drawing.Point(60, 40);
			this.numericUpDown2.Maximum = new System.Decimal(new int[] {
																		   999999,
																		   0,
																		   0,
																		   0});
			this.numericUpDown2.Name = "numericUpDown2";
			this.numericUpDown2.Size = new System.Drawing.Size(60, 20);
			this.numericUpDown2.TabIndex = 35;
			this.numericUpDown2.TextChanged += new System.EventHandler(this.onTextChanged);
			// 
			// checkBox2
			// 
			this.checkBox2.BackColor = System.Drawing.Color.Transparent;
			this.checkBox2.Checked = true;
			this.checkBox2.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBox2.ForeColor = System.Drawing.Color.White;
			this.checkBox2.Location = new System.Drawing.Point(564, 188);
			this.checkBox2.Name = "checkBox2";
			this.checkBox2.Size = new System.Drawing.Size(76, 24);
			this.checkBox2.TabIndex = 98;
			this.checkBox2.Text = "ź������";
			// 
			// button2
			// 
			this.button2.BackColor = System.Drawing.Color.DimGray;
			this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button2.ForeColor = System.Drawing.Color.White;
			this.button2.Location = new System.Drawing.Point(636, 188);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(68, 20);
			this.button2.TabIndex = 49;
			this.button2.Text = "ź�ŧ";
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// button3
			// 
			this.button3.BackColor = System.Drawing.Color.DimGray;
			this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button3.ForeColor = System.Drawing.Color.White;
			this.button3.Location = new System.Drawing.Point(708, 212);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(76, 20);
			this.button3.TabIndex = 100;
			this.button3.Text = "�ѹ�֡ && ��";
			this.button3.Click += new System.EventHandler(this.button3_Click);
			// 
			// button4
			// 
			this.button4.BackColor = System.Drawing.Color.DimGray;
			this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button4.ForeColor = System.Drawing.Color.White;
			this.button4.Location = new System.Drawing.Point(708, 188);
			this.button4.Name = "button4";
			this.button4.Size = new System.Drawing.Size(76, 20);
			this.button4.TabIndex = 101;
			this.button4.Text = "����";
			this.button4.Click += new System.EventHandler(this.button4_Click);
			// 
			// progressBar1
			// 
			this.progressBar1.Location = new System.Drawing.Point(64, 240);
			this.progressBar1.Name = "progressBar1";
			this.progressBar1.Size = new System.Drawing.Size(676, 12);
			this.progressBar1.Step = 1;
			this.progressBar1.TabIndex = 102;
			this.progressBar1.Visible = false;
			// 
			// timer1
			// 
			this.timer1.Interval = 10;
			this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
			// 
			// button1
			// 
			this.button1.BackColor = System.Drawing.Color.DimGray;
			this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button1.ForeColor = System.Drawing.Color.White;
			this.button1.Location = new System.Drawing.Point(636, 212);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(68, 20);
			this.button1.TabIndex = 103;
			this.button1.Text = "refresh";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// textBox6
			// 
			this.textBox6.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(76)), ((System.Byte)(75)), ((System.Byte)(54)));
			this.textBox6.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.textBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold);
			this.textBox6.ForeColor = System.Drawing.Color.LightCyan;
			this.textBox6.Location = new System.Drawing.Point(48, 8);
			this.textBox6.Name = "textBox6";
			this.textBox6.ReadOnly = true;
			this.textBox6.Size = new System.Drawing.Size(144, 31);
			this.textBox6.TabIndex = 107;
			this.textBox6.TabStop = false;
			this.textBox6.Text = "����ŧ";
			// 
			// label10
			// 
			this.label10.BackColor = System.Drawing.Color.Transparent;
			this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(222)));
			this.label10.ForeColor = System.Drawing.Color.Red;
			this.label10.Location = new System.Drawing.Point(772, 24);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(12, 12);
			this.label10.TabIndex = 108;
			this.label10.Text = "X";
			this.label10.Click += new System.EventHandler(this.label10_Click);
			// 
			// EditList
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(76)), ((System.Byte)(75)), ((System.Byte)(54)));
			this.ClientSize = new System.Drawing.Size(800, 264);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.label10,
																		  this.textBox6,
																		  this.button1,
																		  this.progressBar1,
																		  this.button4,
																		  this.button3,
																		  this.button2,
																		  this.listView1,
																		  this.groupBox3,
																		  this.checkBox2});
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.KeyPreview = true;
			this.Name = "EditList";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.Text = "����ŧ";
			this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.onKeyDown);
			this.Closing += new System.ComponentModel.CancelEventHandler(this.onClosing);
			this.Load += new System.EventHandler(this.EditList_Load);
			this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.onKeyUp);
			this.groupBox3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion


		private bool save()
		{
			// ��ͧ�յ�� check ������������
			ArrayList ar = new ArrayList(listView1.Items.Count);
            MessageBox.Show("listview count = " + listView1.Items.Count.ToString());
            MessageBox.Show("song count = " + MainForm.m_hashSongIndex.Count.ToString());
            
			for (int x = 0; x < listView1.Items.Count; x ++)
			{
                //System.Diagnostics.Debug.Print(x.ToString() + "." + listView1.Items[x].SubItems[2].Text);
				if (!ar.Contains(listView1.Items[x].SubItems[2].Text))
				{
					ar.Add(listView1.Items[x].SubItems[2].Text);
				}
				else
				{
					listView1.Items[x].Selected = true;
                    listView1.Focus();
					MessageBox.Show("�����ŧ��� " + x.ToString(),"����͹",MessageBoxButtons.OK,MessageBoxIcon.Information);
					return false;
				}
			}


			// save listView ŧ Mem ��� save �ҡ mem ŧ���
			MainForm.m_hashSongIndex.Clear();
			for (int x = 0; x < listView1.Items.Count; x ++)
			{			
				string strTxt = ((MainForm)(this.Owner)).getTxtFromListView(listView1,x,listView1.Items[x].SubItems[1].Text);
				SongStructure ss = ((MainForm)(this.Owner)).songConvert(strTxt);
				MainForm.m_hashSongIndex.Add(listView1.Items[x].SubItems[2].Text,ss);
			}
			((MainForm)(this.Owner)).saveNSI();

			// �����Ŵ��� MainForm.m_hashSongIndex ����
			((MainForm)(this.Owner)).loadNSI();

			return true;			
		}

		private void label10_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}

		private void refreshList()
		{
			// progress bar
			progressBar1.Value = 0;
			progressBar1.Maximum = MainForm.m_hashSongIndex.Count;
			progressBar1.Visible = true;

			// ��Ҥ�Ң����Ũҡ mem ���� list
			listView1.Items.Clear();

			m_Count = 0;
			idic = MainForm.m_hashSongIndex.GetEnumerator();
			idic.Reset();

			timer1.Enabled = true;
		}

		private void EditList_Load(object sender, System.EventArgs e)
		{
			// 
			m_bSaveNExit = false;

			// ��Ŵ�����á��������
			if (m_bLoadFirst)
			{
				return;
			}
			else
			{
				m_bLoadFirst = true;
			}
			

			refreshList();			
		}

		private void listView1_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// ��Ҥ��� listview ��纵�����˹觵�ҧ� �ͧ control
			m_iListViewSelected = -1;			

			if (listView1.SelectedIndices.Count > 0)
			{			
				m_iListViewSelected = listView1.SelectedIndices[0];
			}
			else
			{
				return;
			}

			// ������ʴ���
			string strTmp = listView1.Items[m_iListViewSelected].SubItems[2].Text == "" ? "0" : listView1.Items[m_iListViewSelected].SubItems[2].Text;
			numericUpDown1.Value = Convert.ToDecimal(strTmp); // ����
			textBox2.Text = listView1.Items[m_iListViewSelected].SubItems[3].Text; // �����ŧ
			textBox3.Text = listView1.Items[m_iListViewSelected].SubItems[4].Text; // ������ŻԹ
			textBox4.Text = listView1.Items[m_iListViewSelected].SubItems[5].Text; // ��ź��
			strTmp = listView1.Items[m_iListViewSelected].SubItems[6].Text == "" ? "0" : listView1.Items[m_iListViewSelected].SubItems[6].Text;
			numericUpDown2.Value = Convert.ToDecimal(strTmp); // ���Ң���
			strTmp = listView1.Items[m_iListViewSelected].SubItems[7].Text == "" ? "0" : listView1.Items[m_iListViewSelected].SubItems[7].Text;
			numericUpDown3.Value = Convert.ToDecimal(strTmp); // ������ش
		}

		private void onTextChanged(object sender, System.EventArgs e)
		{
			if (m_iListViewSelected < 0)
			{
				MessageBox.Show("��س����͡�ŧ����ͧ�����䢡�͹","����͹",MessageBoxButtons.OK,MessageBoxIcon.Information);
				return;
			}
			if (sender == textBox2)
			{
				listView1.Items[m_iListViewSelected].SubItems[3].Text = textBox2.Text; // �����ŧ
			}
			else if (sender == textBox3)
			{
				listView1.Items[m_iListViewSelected].SubItems[4].Text = textBox3.Text; // ������ŻԹ
			}
			else if (sender == textBox4)
			{
				listView1.Items[m_iListViewSelected].SubItems[5].Text = textBox4.Text; // ��ź��
			}
			else if (sender == numericUpDown1)
			{
				// ����
				if (numericUpDown1.Value > 0)
				{
					listView1.Items[m_iListViewSelected].SubItems[2].Text = numericUpDown1.Value.ToString("000000");				
				}
			}
			else if (sender == numericUpDown2)
			{
				// ���Ң���
				listView1.Items[m_iListViewSelected].SubItems[6].Text = numericUpDown2.Value.ToString("###");
			}
			else if (sender == numericUpDown3)
			{
				// ������ش
				listView1.Items[m_iListViewSelected].SubItems[7].Text = numericUpDown3.Value.ToString("###");
			}
		}

		private void button2_Click(object sender, System.EventArgs e)
		{
			// ź
			//			if (listView1.SelectedIndices.Count <= 0)
			//			{
			//				MessageBox.Show("��س����͡�ŧ����ͧ���ź��͹","����͹",MessageBoxButtons.OK,MessageBoxIcon.Information);
			//				return;
			//			}

			if (DialogResult.No == MessageBox.Show("����������..","����͹",MessageBoxButtons.YesNo,MessageBoxIcon.Information))
			{
				return;
			}

			// ź���
			int x = 0;
			while (x < listView1.Items.Count)
			{
				// ź�����Ũҡ listView
				if (listView1.Items[x].Checked)
				{
					if (checkBox2.Checked)
					{
						// ǹ�ٻ��� ����͡�ҡ hdd �ç���
						int d = 0;
						bool cmdExit = false;
						while (!cmdExit && d < MainForm.m_strArrSongPath.Count)
						{
							string strTmpFile = ((string)(MainForm.m_strArrSongPath[d])) + ":\\Nueng\\" + listView1.Items[x].SubItems[1].Text;
							if (File.Exists(strTmpFile))
							{
								File.Delete(strTmpFile);
								cmdExit = true;
							}
							else
							{
								d++;
							}
						}
					}
					listView1.Items.RemoveAt(x); // ���� remove �͡��͹���ջѭ��
				}
				else
				{
					x++;
				}
			}
		}

		private void button3_Click(object sender, System.EventArgs e)
		{
			if (DialogResult.Yes == MessageBox.Show("��ͧ��úѹ�֡�������","����͹�ѹ�֡..",MessageBoxButtons.YesNo,MessageBoxIcon.Information))
			{
				// �ѹ�֡
				try
				{
					System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;
					if (save())
					{
						MessageBox.Show("�ѹ�֡���º��������","�ѹ�֡..",MessageBoxButtons.OK,MessageBoxIcon.Information);
						m_bSaveNExit = true;
					}
					else
					{
						System.Windows.Forms.Cursor.Current = Cursors.Default;
						return;
					}
					System.Windows.Forms.Cursor.Current = Cursors.Default;
				}
				catch(Exception ex)
				{
				}
			}
			this.Close();
		}

		private void onKeyUp(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			// keydown �Ѻ form
			if (sender == this)
			{
				switch(e.KeyData)
				{
					case Keys.Escape:
					case Keys.F9:
					{
						this.Close();
						break;
					}
				}
			}
		}

		private void onColumnClick(object sender, System.Windows.Forms.ColumnClickEventArgs e)
		{
			ListView myListView = (ListView)sender;

			// Determine if clicked column is already the column that is being sorted.
			if ( e.Column == lvwColumnSorter.SortColumn )
			{
				// Reverse the current sort direction for this column.
				if (lvwColumnSorter.Order == SortOrder.Ascending)
				{
					lvwColumnSorter.Order = SortOrder.Descending;
				}
				else
				{
					lvwColumnSorter.Order = SortOrder.Ascending;
				}
			}
			else
			{
				// Set the column number that is to be sorted; default to ascending.
				lvwColumnSorter.SortColumn = e.Column;
				lvwColumnSorter.Order = SortOrder.Ascending;
			}

			// Perform the sort with these new sort options.
			myListView.Sort();
		}

		private void button4_Click(object sender, System.EventArgs e)
		{
			if (listView1.Items.Count <= 0)
			{
				MessageBox.Show("������ŧ���¡��","����͹..",MessageBoxButtons.OK,MessageBoxIcon.Information);
				return;
			}
			
			if (listView1.SelectedIndices.Count <= 0 || listView1.SelectedIndices[0] >= listView1.Items.Count)
			{
				MessageBox.Show("��س����͡�ŧ����ͧ�����������͹","����͹..",MessageBoxButtons.OK,MessageBoxIcon.Information);
				return;
			}

			int x = listView1.SelectedIndices[0];
			listView1.Items.Insert(x
				,new ListViewItem(new String[]{
												  listView1.Items[x].SubItems[0].Text
												  ,listView1.Items[x].SubItems[1].Text
												  ,listView1.Items[x].SubItems[2].Text
												  ,listView1.Items[x].SubItems[3].Text
												  ,listView1.Items[x].SubItems[4].Text
												  ,listView1.Items[x].SubItems[5].Text
												  ,listView1.Items[x].SubItems[6].Text
												  ,listView1.Items[x].SubItems[7].Text
												  ,listView1.Items[x].SubItems[8].Text
												  ,listView1.Items[x].SubItems[9].Text
												  ,listView1.Items[x].SubItems[10].Text
												  ,listView1.Items[x].SubItems[11].Text
												  ,listView1.Items[x].SubItems[12].Text
											  }));
		}

		private void onClosing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			// ��Ҩ������ save �������͹
			if (!m_bSaveNExit)
			{
				MessageBox.Show("�س�ѧ�����ӡ�úѹ�֡ ���Ҥس��ͧ�ѹ�֡\r\n"
					+"������ö�������ö��Ѻ����Һѹ�֡������..","����͹�ѹ�֡..",MessageBoxButtons.OK,MessageBoxIcon.Information);
			}

			timer1.Enabled = false;
			//			if (DialogResult.Yes == MessageBox.Show("��ͧ��úѹ�֡�������","����͹�ѹ�֡..",MessageBoxButtons.YesNo,MessageBoxIcon.Information))
			//			{
			//				// �ѹ�֡
			//				try
			//				{
			//					System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;
			//					if (save())
			//					{
			//						MessageBox.Show("�ѹ�֡���º��������","�ѹ�֡..",MessageBoxButtons.OK,MessageBoxIcon.Information);
			//					}
			//				}
			//				catch(Exception ex)
			//				{
			//				}
			//				System.Windows.Forms.Cursor.Current = Cursors.Default;
			//			}
		}

		private void timer1_Tick(object sender, System.EventArgs e)
		{
			if (idic == null)
			{
				return;
			}
			
			timer1.Stop();

			// cursor
			System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;

		
			int divideChk = 0;
			if (MainForm.m_hashSongIndex.Count / 2 > 0)
			{
                // ¡��ԡ����觤�����Ŵ 24 �.�. 2550 
                //(MainForm.m_hashSongIndex.Count) / 2;
                divideChk = MainForm.m_hashSongIndex.Count;
			}
			else
			{
				return;
			}
			int count = 0; // �ͺ�� 100 ��¡��
			ListViewItem [] lvArr = new ListViewItem[divideChk];
			
			//while(count < divideChk)
			//{
				while(idic.MoveNext())
				{
					m_Count++;
					SongStructure ss = (SongStructure)idic.Value;
					lvArr[count] = new ListViewItem(new string[]{string.Format("{0}",m_Count)
																	,ss.m_strFileName
																	,ss.m_strNo
																	,ss.m_strSongName
																	,ss.m_strArtistName
																	,ss.m_strAlbumName
																	,ss.m_strTimejump
																	,ss.m_strTimeEnd
																	,ss.m_strEct1
																	,ss.m_strEct2
																	,ss.m_strEct3
																	,ss.m_strEct4
																	,ss.m_strEct5
																});
					// refresh progress bar
					progressBar1.Increment(1);
					progressBar1.Refresh();				
					if (++count < divideChk){continue;}
					else{break;}
				}
			//}

			try
			{
				listView1.Items.AddRange(lvArr);
			}
			catch(System.NullReferenceException exnull)
			{
				// progressbar and cursor
				progressBar1.Visible = false;
				System.Windows.Forms.Cursor.Current = Cursors.Default;
				MessageBox.Show("��Ŵ��ª������º��������","����͹..",MessageBoxButtons.OK,MessageBoxIcon.Information);
				return;
			}

			// ����ѧ����������������Ŵ�ա����
			if (listView1.Items.Count < MainForm.m_hashSongIndex.Count)
			{
				timer1.Start();
			}
			else
			{
				// progressbar and cursor
				progressBar1.Visible = false;
				System.Windows.Forms.Cursor.Current = Cursors.Default;
				MessageBox.Show("��Ŵ��ª������º��������","����͹..",MessageBoxButtons.OK,MessageBoxIcon.Information);
			}
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			if (DialogResult.OK == MessageBox.Show("��� refresh �ռŷ��������ŷ������٭���\r\n\r\n�� OK �����׹�ѹ\r\n�� Cancel ���͡�Ѻ仺ѹ�֡","���������� ?",MessageBoxButtons.OKCancel,MessageBoxIcon.Question))
			{
				refreshList();
			}
		}

		private void onKeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			if (e.KeyData == Keys.Escape ||
				e.KeyData == Keys.F1 ||
				e.KeyData == Keys.F2 ||				
				e.KeyData == Keys.F3 ||				

				e.KeyData == Keys.F5 ||
				e.KeyData == Keys.F6 ||
				e.KeyData == Keys.F7 ||
				e.KeyData == Keys.F8 || 
				//e.KeyData == Keys.F9 ||// Menu ������˹�Դ ��һ�������͡
				e.KeyData == Keys.F10)
			{
				this.Close();
			}
		}
	}
}
