using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace MyMP3_X1
{
	/// <summary>
	/// Summary description for AskForm.
	/// </summary>
	public class AskForm : System.Windows.Forms.Form
	{
		private bool m_bOk = false;

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.NumericUpDown numericUpDown1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public AskForm(string strLabel,long iMax,int iDefault)
		{
			InitializeComponent();
			label1.Text = strLabel;
			numericUpDown1.Maximum = iMax;
			numericUpDown1.Value = iDefault;
			numericUpDown1.Focus();
			numericUpDown1.Select(0,numericUpDown1.Value.ToString().Length);
		}

		public AskForm(string strLabel,long iMax)
		{
			InitializeComponent();
			label1.Text = strLabel;
			numericUpDown1.Maximum = iMax;
			numericUpDown1.Value = 0;
			numericUpDown1.Focus();
			numericUpDown1.Select(0,numericUpDown1.Value.ToString().Length);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(AskForm));
			this.label1 = new System.Windows.Forms.Label();
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.BackColor = System.Drawing.Color.Transparent;
			this.label1.ForeColor = System.Drawing.Color.Aqua;
			this.label1.Location = new System.Drawing.Point(156, 64);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(116, 20);
			this.label1.TabIndex = 0;
			this.label1.Text = "label1";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// button1
			// 
			this.button1.BackColor = System.Drawing.Color.Transparent;
			this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button1.ForeColor = System.Drawing.Color.Yellow;
			this.button1.Location = new System.Drawing.Point(216, 92);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(56, 24);
			this.button1.TabIndex = 2;
			this.button1.Text = "��ŧ";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// button2
			// 
			this.button2.BackColor = System.Drawing.Color.Transparent;
			this.button2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button2.ForeColor = System.Drawing.Color.Yellow;
			this.button2.Location = new System.Drawing.Point(280, 92);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(56, 24);
			this.button2.TabIndex = 3;
			this.button2.Text = "¡��ԡ";
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// numericUpDown1
			// 
			this.numericUpDown1.BackColor = System.Drawing.SystemColors.ControlDarkDark;
			this.numericUpDown1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.numericUpDown1.ForeColor = System.Drawing.SystemColors.Info;
			this.numericUpDown1.Location = new System.Drawing.Point(280, 64);
			this.numericUpDown1.Name = "numericUpDown1";
			this.numericUpDown1.Size = new System.Drawing.Size(104, 20);
			this.numericUpDown1.TabIndex = 1;
			// 
			// AskForm
			// 
			this.AcceptButton = this.button1;
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackgroundImage = ((System.Drawing.Bitmap)(resources.GetObject("$this.BackgroundImage")));
			this.CancelButton = this.button2;
			this.ClientSize = new System.Drawing.Size(560, 134);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.numericUpDown1,
																		  this.button2,
																		  this.button1,
																		  this.label1});
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.KeyPreview = true;
			this.Name = "AskForm";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "�ͺ���...";
			this.Load += new System.EventHandler(this.AskForm_Load);
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private void button1_Click(object sender, System.EventArgs e)
		{
			m_bOk = true;
			this.Close();
		}

		private void button2_Click(object sender, System.EventArgs e)
		{
			m_bOk = false;
		}

		public bool getOkButton()
		{
			return m_bOk;
		}
		
		public int getValue()
		{
			return (int) numericUpDown1.Value;
		}

		private void AskForm_Load(object sender, System.EventArgs e)
		{
		}
	}
}
