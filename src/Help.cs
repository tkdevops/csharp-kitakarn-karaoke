using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace MyMP3_X1
{
	/// <summary>
	/// Summary description for Help.
	/// </summary>
	public class Help : System.Windows.Forms.Form
	{
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.TextBox textBox2;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public Help()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			this.TransparencyKey = System.Drawing.Color.FromArgb(((System.Byte)(76)), ((System.Byte)(75)), ((System.Byte)(54)));
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.textBox2 = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// textBox1
			// 
			this.textBox1.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(76)), ((System.Byte)(75)), ((System.Byte)(54)));
			this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
			this.textBox1.ForeColor = System.Drawing.Color.White;
			this.textBox1.Location = new System.Drawing.Point(48, 48);
			this.textBox1.Multiline = true;
			this.textBox1.Name = "textBox1";
			this.textBox1.ReadOnly = true;
			this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.textBox1.Size = new System.Drawing.Size(716, 228);
			this.textBox1.TabIndex = 42;
			this.textBox1.Text = "";
			// 
			// textBox2
			// 
			this.textBox2.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(76)), ((System.Byte)(75)), ((System.Byte)(54)));
			this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold);
			this.textBox2.ForeColor = System.Drawing.Color.LightCyan;
			this.textBox2.Location = new System.Drawing.Point(48, 8);
			this.textBox2.Name = "textBox2";
			this.textBox2.ReadOnly = true;
			this.textBox2.Size = new System.Drawing.Size(144, 31);
			this.textBox2.TabIndex = 43;
			this.textBox2.TabStop = false;
			this.textBox2.Text = "���������";
			// 
			// Help
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(76)), ((System.Byte)(75)), ((System.Byte)(54)));
			this.ClientSize = new System.Drawing.Size(800, 300);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.textBox2,
																		  this.textBox1});
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.KeyPreview = true;
			this.Name = "Help";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.Text = "Help";
			this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.onKeyDown);
			this.Load += new System.EventHandler(this.Help_Load);
			this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.onKeyUp);
			this.ResumeLayout(false);

		}
		#endregion

		private void Help_Load(object sender, System.EventArgs e)
		{
			textBox1.Text = "\t�յ�ҹ�� �������� �������ö����ª������ѹ�ԧ �ú����ͧ����ͧ��˹ѧ �ѧ�ŧ "
				+"�ա����˹�觢ͧ��þѲ�ҷ�������ش��駵�ͨҡ \"˹�� ��������\" ����繷ҧ���͡���� �ͧ����������"
				+"��������ö�鹤��������дѺ��ͧ VIP �����к���ҧ� �ҡ��·���ͧ�Ѻ���������ͧ����� "
				+"���к������ŧ����ͧ�Ѻ��þ�����µç �������͡�ҡ��������� !!, �к��Ѵ���ŧ, �к�ʶԵ�"
				+", ����ö�¡��⾧���¢��, �Ѵ���§����ͧ������ͧ��������, �����������͡��ǹ������� "
				+"��Ф�������ö���� �ա�ҡ��� �������Ҵ��硷ӧҹ���� ���Թ��Ѿ�ҡ�����ͧ "
				+"����ö�ӧҹ��պ� Win2k ���� XP ���� DirectX 8.1 ��᡹��ѡ㹡�û����ż���� Multimedia ��ҧ� �� "
				+"MP3,AVI,DAT,MIDI ��� �͡�ҡ����ѧ���件֧����дѺ DVD �ա���� ������Ҿ���Ѵ ����ѧ�ͧ�Ѻ�ѭ�ҳ���§��֧ "
				+"5.1 Channel �ա����\r\n"
				+"\t�ѧ��鹤س�֧����ö���Ѻ�����ѹ�ԧ�����ҧ������ ����ٻẺ �������ͨӡѴ�� ��Һ��ҷ���������ö�ҧ"
				+"Hardware ���س�ը��ͧ�Ѻ ���ͻ���ª����觡����������٧�ش �س������¹�������觧ҹ�Ǻ��� ����դ���觴ѧ���..\r\n"
				+"\r\n"
				+"\t���ٵ�ҧ�\r\n"
				+"\tF1\t\t- �к����������\r\n"
				+"\tF2\t\t- �к��Ѵ��ä���ŧ\r\n"				
				+"\tF3\t\t- �к������ŧ\r\n"
				+"\tF5\t\t- �к������ŧ\r\n"
				+"\tF6\t\t- �к������ŧ�ҡ�����\r\n"
				+"\tF7\t\t- �к�ʶԵ��ŧ\r\n"
				+"\tF8\t\t- �к��Ѵ����ª����ŧ\r\n"
				+"\tF9\t\t- �к�����ŧ\r\n"
				+"\tF10\t\t- Credit\r\n"
				+"\tCtrl + F12\t\t- �͡�ҡ�����\r\n"
				+"\r\n"
				+"\t�����Ѵ��÷ӧҹ\r\n"
				+"\tF11\t\t- ���˹�Ҩ� (���˹�Ҩͤس����� 800x600)\r\n"
//				+"\tPgUp\t\t- ��觤��������ŧ\r\n"
//				+"\tPgDown\t\t- Ŵ���������ŧ\r\n"
				+"\tHome\t\t- ����ŧ�������\r\n"
				+"\tEnd\t\t- ���ŧ���\r\n"
				+"\tDelete\t\t- ¡��ԡ�ŧ�Ѩ�غѹ������¡�ŧ�Ѵ�\r\n"
				+"\tSpace\t\t- ��ش�ŧ\r\n"
				+"\t+\t\t- ������§\r\n"
				+"\t-\t\t- Ŵ���§\r\n"
				+"\t*\t\t- ��Ѻ���§ Stereo/Mono\r\n"
				+"\t/\t\t- Mute\r\n"
				+"\r\n"
				+"\t�բ��ʧ����������ô�Դ��ͼ��Ѵ��\r\n"
				+"\tEmail : BenNueng@Gmail.com\r\n"
				+"\r\n"
				+"\t\t��Һ�ͺ��Фس�ء��ҹ������͡�� \"�յ�ҹ�� ��������\" \r\n";
			
			textBox1.Select(0,0);
		}

		private void onKeyUp(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			switch (e.KeyData)
			{
				case Keys.F1:
				case Keys.Escape:
				{
					this.Close();
					break;
				}
			}	
		}

		private void onKeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			if (e.KeyData == Keys.Escape ||
				//e.KeyData == Keys.F1 ||// Menu ������˹�Դ ��һ�������͡
				e.KeyData == Keys.F2 ||				
				e.KeyData == Keys.F3 ||				

				e.KeyData == Keys.F5 || 
				e.KeyData == Keys.F6 ||
				e.KeyData == Keys.F7 ||
				e.KeyData == Keys.F8 || 
				e.KeyData == Keys.F9 ||
				e.KeyData == Keys.F10)
			{
				this.Close();
			}
		}
	}
}
