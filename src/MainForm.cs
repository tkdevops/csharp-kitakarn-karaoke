//#define __AR_JIN__ // @ EditList & CopyUtil
//#define __WIN2K__ // @ MyUtil

#define __SOR_AOR__

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.IO;
using System.Data;
using System.Threading;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Web.Security;

namespace MyMP3_X1
{
	/// <summary> MainForm for this sample media file player. </summary>
	public class MainForm : System.Windows.Forms.Form
	{
		public const string m_strProgramName = "Kitakarn Karaoke";
		
#if (__AR_JIN__)
		public const string m_strVersion = "2008 Professional (Siam Sound)";
#else		
		public const string m_strVersion = "2008 Professional";
#endif
		public const string m_strTitle = "BenNueng@hotmail.com";

		public static ArrayList m_arrSongList = new ArrayList(); // ���ӴѺ�ŧ		
		//private const int _MAX_SONG_PATH = 10; // ��˹���� path �٧�ش���͹حҵ������
		//public static string []m_strArrSongPath = new string[_MAX_SONG_PATH];
		public static ArrayList m_strArrSongPath = null; // �� drive ������ŧ��ҧ

		public static string m_strMAC = ""; // ������ MACAddress
		public static Hashtable m_hashStatSong = null; // ��ʶԵԡ����ҹ�ŧ
		public static Hashtable m_hashSongIndex = null; // ����������´�ŧ�����������������÷���˹��ҧ
		public static string m_strCurrentDir = ""; // �ŧ path directory �Ѩ�غѹ
		public static string m_strSystemDir = ""; // System dir
		public static bool m_bShowMsgBox = true; // �������ʴ���������ʴ� MessageBox

		private string m_strTimeJump = "0"; // ���Ң����ŧ
		private string m_strTimeEnd = "0"; // ��������ش

		private int m_iSpeaker = 3; // 1 = both, 2 = right, 3 = left
		private bool m_bNowPlaying = false; // ��й���ŧ���ѧ����������
		
		private int m_iSongSize = 6; // ��Ҵ�ͧ�����ŧ�٧�ش��Ҩ���ҡ����ѡ ��˹���µ�� = 6 
		private double m_dRate = 1.0; // ���������ŧ		
		public string m_strKey = "";	// �纤���ѡ�÷�衴

		private double m_dModifyRateInc = 0.055;
		private double m_dModifyRateDec = -0.055;

		// ADS
		private string m_strAdsPath = ""; // path �� �ɳ�
		private bool m_bAdsPlay = false;
		private int m_iAds = 0; // ���ӴѺ����ɳ�

        // Authorize Server URL
        private string m_strURLServerAUTH = "";
		

		public bool m_bPlayByInsertSong = false; // ����¡�������蹷ѹ�� (����ͧź�ŧ�á)
		
		// All Picture control
		public bool m_bStatusShowDialog = false; // ��Ǩ����� status �� Show dialog �������		

		//StatusPixel m_statusPixel;
		Status m_status = null;
		SongListControl m_songListControl = null;
		CopyUtil m_copyUtil = null;
		SearchSong m_searchSong = null;
		RemoteSong m_RemoteSearch = null;
		StatSong m_statSong = null;
		Convert2Book m_convert2Book = null;		
		EditList m_editList = null;		
		Credit m_credit= null;
		Help m_help= null;		
		MessageForm m_messageForm = null;
		MixForm m_mixForm = null;


		#region DirectShow Variable
		private System.Windows.Forms.MainMenu mainMenu1;
		private System.Windows.Forms.MenuItem menuTopFile;
		private System.Windows.Forms.MenuItem menuFileOpenClip;
		private System.Windows.Forms.MenuItem menuFileCloseClip;
		private System.Windows.Forms.MenuItem menuFileExit;
		private System.Windows.Forms.MenuItem menuTopControl;
		private System.Windows.Forms.MenuItem menuControlPause;
		private System.Windows.Forms.MenuItem menuControlStop;
		private System.Windows.Forms.MenuItem menuControlMute;
		private System.Windows.Forms.MenuItem menuControlStep;
		private System.Windows.Forms.MenuItem menuControlHalf;
		private System.Windows.Forms.MenuItem menuControlThreeQ;
		private System.Windows.Forms.MenuItem menuControlNormal;
		private System.Windows.Forms.MenuItem menuControlDouble;
		private System.Windows.Forms.MenuItem menuControlFullScr;
		private System.Windows.Forms.MenuItem menuTopRate;
		private System.Windows.Forms.MenuItem menuRateIncr;
		private System.Windows.Forms.MenuItem menuRateDecr;
		private System.Windows.Forms.MenuItem menuRateNormal;
		private System.Windows.Forms.MenuItem menuRateHalf;
		private System.Windows.Forms.MenuItem menuRateDouble;
		private System.Windows.Forms.MenuItem menuTopHelp;
		private System.Windows.Forms.MenuItem menuHelpAbout;
		private System.Windows.Forms.MenuItem menuFileSep1;
		private System.Windows.Forms.MenuItem menuControlSep1;
		private System.Windows.Forms.MenuItem menuControlSep2;
		private System.Windows.Forms.MenuItem menuControlSep3;
		private System.Windows.Forms.MenuItem menuRateSep1;
		private System.Windows.Forms.MenuItem menuItem1;
		private System.Windows.Forms.MenuItem menuItem5;
		private System.Windows.Forms.MenuItem menuItem8;
		private System.Windows.Forms.MenuItem leftSpeaker;
		private System.Windows.Forms.MenuItem bothSpeaker;
		private System.Windows.Forms.MenuItem rightSpeaker;
		private System.Windows.Forms.MenuItem incVolume;
		private System.Windows.Forms.MenuItem decVolume;
		private System.Windows.Forms.MenuItem midVolume;
		private System.Windows.Forms.MenuItem incLeftVolume;
		private System.Windows.Forms.MenuItem DecLeftVolume;
		private System.Windows.Forms.MenuItem incRightVolume;
		private System.Windows.Forms.MenuItem decRightVolume;
		private System.Windows.Forms.MenuItem menuItem2;
		private System.Windows.Forms.MenuItem menuItem3;
		private System.Windows.Forms.MenuItem menuItem6;
		private System.Windows.Forms.MenuItem menuItem4;
		private System.Windows.Forms.MenuItem menuItem7;
		private System.Windows.Forms.Timer timer1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.Timer UpdateSongTime;
        private Label label3;
		//private AxInfrared.AxIRConnect axIR;
						
		private System.ComponentModel.IContainer components = null;
		#endregion

		public MainForm()
		{
			// setup register
			Register();
			InitializeComponent();			
			mySetup();
		}

		// save list �ŧ�������ŧ˹��¤�����
		public void saveNSI(string strFileName)
		{
			try
			{
				Stream st = File.Open(MainForm.m_strCurrentDir + "\\" + strFileName,FileMode.Create);
				IFormatter bf = new BinaryFormatter();
				bf.Serialize(st,m_hashSongIndex);
				st.Close();
			}
			catch(Exception ex)
			{
				//MessageBox.Show(ex.Message,"Kitakarn Karaoke Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
				MessageForm("Error..\r\n" + ex.Message);
			}
		}

		public void saveNSI()
		{
			try
			{
				Stream st = File.Open(MainForm.m_strCurrentDir + "\\SongIndexSE.nsi",FileMode.Create);
				IFormatter bf = new BinaryFormatter();
				bf.Serialize(st,m_hashSongIndex);
				st.Close();
			}
			catch(Exception ex)
			{
				//MessageBox.Show(ex.Message,"Kitakarn Karaoke Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
				MessageForm("Error..\r\n" + ex.Message);
			}
		}

		// load list �ŧ�������ŧ˹��¤�����
		public void loadNSI()
		{
			try
			{
				if (File.Exists(MainForm.m_strCurrentDir + "\\SongIndexSE.nsi"))
				{
					Stream st = File.Open(MainForm.m_strCurrentDir + "\\SongIndexSE.nsi",FileMode.Open);
					IFormatter bf = new BinaryFormatter();
					m_hashSongIndex = (Hashtable)bf.Deserialize(st);
					st.Close();

					// �ŧ�� dataset
					SS2DSConvert m_ss2dsConvert = null; // ���ŧ hashtable �� dataset
					m_ss2dsConvert = new SS2DSConvert(m_hashSongIndex);
					
					// ���§�ӴѺ�ç������
					// ���ҵ�������ŧ ���§��������ŧ ��ŻԹ ��ź��
					// ������������� System Directory "C:\windows\system32"					
					int x = 0;
					
					string strExp = string.Format("m_strNo <> ''");
					string strSort = "m_strSongName Asc,m_strArtistName Asc,m_strAlbumName Asc";
					DataRow []dataRow = m_ss2dsConvert.select(strExp,strSort);
					StreamWriter sw = new StreamWriter(MainForm.m_strSystemDir + "\\SongName.asc",false,System.Text.Encoding.GetEncoding(874));
					for (x = 0; x < dataRow.Length; x++)
					{
						sw.WriteLine(dataRow[x][0].ToString() + "\t"
							+ dataRow[x][1].ToString() + "\t"
							+ dataRow[x][2].ToString() + "\t"
							+ dataRow[x][3].ToString() + "\t"
							+ dataRow[x][4].ToString() + "\t"
							+ dataRow[x][5].ToString() + "\t"
							+ dataRow[x][6].ToString() + "\t"
							+ dataRow[x][7].ToString() + "\t"
							+ dataRow[x][8].ToString() + "\t"
							+ dataRow[x][9].ToString() + "\t"
							+ dataRow[x][10].ToString() + "\t"
							+ dataRow[x][11].ToString() + "\t"
							+ dataRow[x][12].ToString());
					}
					sw.Close();

					// ���� ���§��� ��ŻԹ �����ŧ  ��ź��
					strSort = "m_strArtistName Asc,m_strSongName Asc,m_strAlbumName Asc";
					dataRow = m_ss2dsConvert.select(strExp,strSort);
					sw = new StreamWriter(MainForm.m_strSystemDir + "\\ArtistName.asc",false,System.Text.Encoding.GetEncoding(874));
					for (x = 0; x < dataRow.Length; x++)
					{
						sw.WriteLine(dataRow[x][0].ToString() + "\t"
							+ dataRow[x][1].ToString() + "\t"
							+ dataRow[x][2].ToString() + "\t"
							+ dataRow[x][3].ToString() + "\t"
							+ dataRow[x][4].ToString() + "\t"
							+ dataRow[x][5].ToString() + "\t"
							+ dataRow[x][6].ToString() + "\t"
							+ dataRow[x][7].ToString() + "\t"
							+ dataRow[x][8].ToString() + "\t"
							+ dataRow[x][9].ToString() + "\t"
							+ dataRow[x][10].ToString() + "\t"
							+ dataRow[x][11].ToString() + "\t"
							+ dataRow[x][12].ToString());
					}
					sw.Close();

					// ���� ���§��� ��ź�� �����ŧ  ��ŻԹ 
					strSort = "m_strAlbumName Asc,m_strSongName Asc,m_strArtistName Asc";
					dataRow = m_ss2dsConvert.select(strExp,strSort);
					sw = new StreamWriter(MainForm.m_strSystemDir + "\\AlbumName.asc",false,System.Text.Encoding.GetEncoding(874));
					for (x = 0; x < dataRow.Length; x++)
					{
						sw.WriteLine(dataRow[x][0].ToString() + "\t"
							+ dataRow[x][1].ToString() + "\t"
							+ dataRow[x][2].ToString() + "\t"
							+ dataRow[x][3].ToString() + "\t"
							+ dataRow[x][4].ToString() + "\t"
							+ dataRow[x][5].ToString() + "\t"
							+ dataRow[x][6].ToString() + "\t"
							+ dataRow[x][7].ToString() + "\t"
							+ dataRow[x][8].ToString() + "\t"
							+ dataRow[x][9].ToString() + "\t"
							+ dataRow[x][10].ToString() + "\t"
							+ dataRow[x][11].ToString() + "\t"
							+ dataRow[x][12].ToString());
					}
					sw.Close();

					// ���� ���§��� ���� �����ŧ ��ź�� ��ŻԹ 
					strSort = "m_strNo Asc,m_strSongName Asc,m_strArtistName Asc,m_strAlbumName Asc";
					dataRow = m_ss2dsConvert.select(strExp,strSort);
					sw = new StreamWriter(MainForm.m_strSystemDir + "\\No.asc",false,System.Text.Encoding.GetEncoding(874));
					for (x = 0; x < dataRow.Length; x++)
					{
						sw.WriteLine(dataRow[x][0].ToString() + "\t"
							+ dataRow[x][1].ToString() + "\t"
							+ dataRow[x][2].ToString() + "\t"
							+ dataRow[x][3].ToString() + "\t"
							+ dataRow[x][4].ToString() + "\t"
							+ dataRow[x][5].ToString() + "\t"
							+ dataRow[x][6].ToString() + "\t"
							+ dataRow[x][7].ToString() + "\t"
							+ dataRow[x][8].ToString() + "\t"
							+ dataRow[x][9].ToString() + "\t"
							+ dataRow[x][10].ToString() + "\t"
							+ dataRow[x][11].ToString() + "\t"
							+ dataRow[x][12].ToString());
					}
					sw.Close();

				}
			}
			catch(Exception ex)
			{
				//MessageBox.Show(ex.Message,"Kitakarn Karaoke Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
				MessageForm("Error..\r\n" + ex.Message);
			}
		}

		public void MessageForm(string str)
		{
            if (m_messageForm != null)
            {
                m_messageForm.show(str);
            }
            else
            {
                m_messageForm = new MessageForm();
                m_messageForm.Owner = this;
            }
		}

		private void deleteASC()
		{
			// No
			string strPath = MainForm.m_strSystemDir + "\\No.asc";
			if (File.Exists(strPath))
			{
				File.Delete(strPath);
			}

			// Song
			strPath = MainForm.m_strSystemDir + "\\SongName.asc";
			if (File.Exists(strPath))
			{
				File.Delete(strPath);
			}
			
			// Album
			strPath = MainForm.m_strSystemDir + "\\AlbumName.asc";
			if (File.Exists(strPath))
			{
				File.Delete(strPath);
			}

			// Artist
			strPath = MainForm.m_strSystemDir + "\\ArtistName.asc";
			if (File.Exists(strPath))
			{
				File.Delete(strPath);
			}
		}

		public void loadNSI4Convert()
		{
			m_hashSongIndex.Clear();
			if (File.Exists("songIndex.nsi"))
			{
				StreamReader sr = new StreamReader(MainForm.m_strCurrentDir + "\\songIndex.nsi",System.Text.Encoding.GetEncoding(874));
				string strReadSongIndex = "";
				while((strReadSongIndex = sr.ReadLine()) != null)
				{
					SongStructure ss = songConvert(strReadSongIndex);
					if (!m_hashSongIndex.ContainsKey(ss.m_strNo))
					{
						m_hashSongIndex.Add(ss.m_strNo,ss);
					}
					else
					{
						MessageBox.Show("�鹾����� " + ss.m_strNo + " ���","�բ�ͼԴ��Ҵ��������ª����ŧ",MessageBoxButtons.OK,MessageBoxIcon.Error);
					}
				}
				sr.Close();

				// convertHere !!!
				saveNSI();
			}
		}

		private void mySetup()
		{
			System.Windows.Forms.Cursor.Hide();

			// main form event
			this.Resize += new System.EventHandler(this.MainForm_Resize);
			this.Closing += new System.ComponentModel.CancelEventHandler(this.MainForm_Closing);
			this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.onKeyUp);
			this.Activated += new System.EventHandler(this.MainForm_Activated);
			
			// timer1 event
			this.timer1.Tick += new System.EventHandler(this.timer1_Tick);

			// timer2 event
			this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
			timer2.Enabled = true;
			timer2.Start();

			// version
			label1.Text = m_strProgramName + " " + m_strVersion;

//			// remote
//			try
//			{
//				axIR.OpenIR();
//			}
//			catch(Exception eir)
//			{
//			}

			m_strCurrentDir = System.Environment.CurrentDirectory;
			m_strSystemDir = System.Environment.SystemDirectory;
			
			// ��ͧ init dir song path ��͹			
//			for (int x = 0; x < m_strArrSongPath.Length; x++)
//			{
//				m_strArrSongPath[x] = "";
//			}

			// songIndex & Path
			m_strArrSongPath = new ArrayList();
			m_hashSongIndex = new Hashtable(); // ����������´�ŧ
			m_hashStatSong = new Hashtable();  // ��ʶԵԡ�����ŧ

			// load ʶԵ�
			if (File.Exists(MainForm.m_strCurrentDir + "\\Stat.nst"))
			{

				Stream st = null;
				try
				{
					if (File.Exists(MainForm.m_strCurrentDir + "\\Stat.nst"))
					{
						st = File.Open(MainForm.m_strCurrentDir + "\\Stat.nst",FileMode.Open);
						IFormatter bf = new BinaryFormatter();
						m_hashStatSong = (Hashtable)bf.Deserialize(st);
						st.Close();
					}
				}
				catch(Exception ex)
				{
					//MessageBox.Show(ex.Message,"Nueng Karaoke Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
				}
				finally
				{
					st.Close();
				}

				//				StreamReader sr = new StreamReader(MainForm.m_strCurrentDir + "\\Stat.nst",System.Text.Encoding.GetEncoding(874));
				//				string strTmp = "";
				//				while((strTmp = sr.ReadLine()) != null)
				//				{
				//					string[]strArrTmp = strTmp.Split('\t');
				//					try
				//					{
				//						MainForm.m_hashStatSong.Add(strArrTmp[0],strArrTmp[1]);
				//					}
				//					catch(Exception exc)
				//					{
				//
				//					}
				//				}
				//				sr.Close();
			}
			else
			{
				// �������ա���������ѹ���
				m_hashStatSong.Add("Start Date",System.DateTime.Now.ToString("dd/MM/yyyy"));
			}

			//loadNSI4Convert();

			// ��Ŵ songIndex
			// songIndexSE.nsi �������͡��������´�ͧ�ŧ������ʹ���
			loadNSI();		
						
			// ������ŧ�Ѩ�غѹ
			if (File.Exists("config.cfg"))
			{
				try
				{
					StreamReader sr = new StreamReader(MainForm.m_strCurrentDir + "\\config.cfg",System.Text.Encoding.GetEncoding(874));
					string strRead;
					while ((strRead = sr.ReadLine()) != null)
					{
						string []strArr = strRead.Split('=');
//						if (strArr[0].Trim().ToUpper() == "SONG_SOURCE")
//						{
//							m_strSongSource = strArr[1];
//						}
//						else 
						if (strArr[0].Trim().ToUpper() == "MODIFY_RATE_INC")
						{
							try
							{
								m_dModifyRateInc = Convert.ToDouble(strArr[1]);
							}
							catch(Exception ex1)
							{
							}
						}
						else if (strArr[0].Trim().ToUpper() == "MODIFY_RATE_DEC")
						{
							try
							{
								m_dModifyRateDec = Convert.ToDouble(strArr[1]);
							}
							catch(Exception ex2)
							{
							}
						}
						else if (strArr[0].Trim().ToUpper() == "SONG_PATH")
						{
							// path �����
							string strTmp = strArr[1];
							string []strArrPath = checkNFixText(ref strTmp);
							for (int x = 0; x < strArrPath.Length; x++)
							{
								if (strArrPath[x] != "")
								{
									// ���� drive ��� m_strArrSongPath
									m_strArrSongPath.Add(strArrPath[x]);
								}
							}

							//m_strSongPath = strArr[1];
//							string []strArrTmp = strArr[1].Split(',');
//							int iend = 0;
//							if (strArrTmp.Length > _MAX_SONG_PATH)
//							{
//								iend = _MAX_SONG_PATH;
//							}
//							else
//							{
//								iend = strArrTmp.Length;
//							}

//							for (int x = 0; x < iend; x++)
//							{
//								m_strArrSongPath[x] = strArrTmp[x];
//							}							
						}
							// �ʴ� �ɳ�
						else if (strArr[0].Trim().ToUpper() == "ADS_PATH")
						{
							m_strAdsPath = strArr[1].Trim().ToUpper();
							if (!m_strAdsPath.EndsWith("\\"))
							{
								m_strAdsPath += "\\";
							}
						}
                        else if (strArr[0].Trim().ToUpper() == "LOGO_PATH")
                        {
                            string strFileLogo = strArr[1].Trim().ToUpper();
                            if (File.Exists(strFileLogo))
                            {
                                try
                                {
                                    Image img = new Bitmap(strFileLogo);
                                    this.BackgroundImage = img;
                                }
                                catch (Exception loadLogo)
                                {
                                }
                            }
                        }
                        else if (strArr[0].Trim().ToUpper() == "URLAUTH")
                        {
                            m_strURLServerAUTH = strArr[1].Trim().ToUpper();
                            if (!m_strURLServerAUTH.EndsWith("/"))
                            {
                                m_strURLServerAUTH += "/";
                            }
                        }
                        else if (strArr[0].Trim().ToUpper() == "SHOW_MESSAGE_BOX")
						{
							if (strArr[1].Trim().ToUpper() == "FALSE")
							{
								m_bShowMsgBox = false;
							}
						}
					}
					sr.Close();
				}
				catch(Exception ex)
				{
					//MessageBox.Show(ex.Message,"Error 101.. while loading config.cfg",MessageBoxButtons.OK,MessageBoxIcon.Error);
					MessageForm("Error 101 while loading config.cfg..\r\n[" + ex.Message + "]");
					throw new Exception("error ..in config.cfg");
				}
			}
//			else
//			{
//				MessageBox.Show("Error.. Can't open config.cfg","Error 102.. while loading config.cfg",MessageBoxButtons.OK,MessageBoxIcon.Error);
//				throw new Exception("error ..in config.cfg");
//			}

			// setup form ��ҧ�
			// form status
			m_status = new Status();
			m_status.Owner = this;
			//m_status.Size = new Size(175,75);
			//m_status.Location = new Point(this.Location.X+297,this.Location.Y);

			// form status pixel
			//m_statusPixel = new StatusPixel();
			//m_statusPixel.Owner = this;
			//m_statusPixel.Size = new Size(200,55);
			//m_statusPixel.Location = new Point(this.Location.X+297,this.Location.Y);


			// MessageBox.show
			m_messageForm = new MessageForm();
			m_messageForm.Owner = this;


			// ��ͧ�� Drive C:\\Nueng <-- �� cache ����..
			System.IO.Directory.CreateDirectory("C:\\Nueng");
			// ��Ǩ��Ҷ������� SONG_PATH			
			if (m_strArrSongPath.Count <= 0)
			{
				m_strArrSongPath.Add("C");
				//System.IO.Directory.CreateDirectory("C:\\Nueng");
				//MessageBox.Show("��辺 directory ���ŧ �֧�� C:\\Nueng �繤���ҵ�Ұҹ","����͹..",MessageBoxButtons.OK,MessageBoxIcon.Warning);
				MessageForm("����͹..\r\n��辺 Directory ���ŧ\r\nC:\\Nueng ���ҵ�Ұҹ");
			}
			else
			{
				// ����� Drive �������� ����Ǩ��� Drive �á�� C �������
				if (((string)m_strArrSongPath[0]).ToUpper() != "C")
				{
					m_strArrSongPath.Insert(0,"C");
				}
			}
			

			// form SongListControl
			m_songListControl = new SongListControl();
			m_songListControl.Owner = this;
			m_songListControl.Size = new Size(800, 300);
			m_songListControl.Location = new Point(this.Location.X,this.Location.Y);

			// form CopyUtil
			m_copyUtil = new CopyUtil();
			m_copyUtil.Owner = this;
			m_copyUtil.Size = new Size(800, 264);
			m_copyUtil.Location = new Point(this.Location.X,this.Location.Y);

			// form Stat
			m_statSong = new StatSong();
			m_statSong.Owner = this;
			m_statSong.Size = new Size(800, 264);
			m_statSong.Location = new Point(this.Location.X,this.Location.Y);

			// form Convert 2 book
			m_convert2Book = new Convert2Book();
			m_convert2Book.Owner = this;
			m_convert2Book.Size = new Size(800, 264);
			m_convert2Book.Location = new Point(this.Location.X,this.Location.Y);

			// form EditList
			m_editList = new EditList();
			m_editList.Owner = this;
			m_editList.Size = new Size(800, 264);
			m_editList.Location = new Point(this.Location.X,this.Location.Y);

			// form SearchSong
			m_searchSong = new SearchSong();
			m_searchSong.Owner = this;
			m_searchSong.Size = new Size(800, 300);
			m_searchSong.Location = new Point(this.Location.X,this.Location.Y);

			// form remote SearchSong
			m_RemoteSearch = new RemoteSong();
			m_RemoteSearch.Owner = this;
			m_RemoteSearch.Size = new Size(800, 300);
			m_RemoteSearch.Location = new Point(this.Location.X,this.Location.Y);
			
			// form credit
			m_credit = new Credit();
			m_credit.Owner = this;
			m_credit.Size = new Size(800, 264);
			m_credit.Location = new Point(this.Location.X,this.Location.Y);

			// form Help
			m_help = new Help();
			m_help.Owner = this;
			m_help.Size = new Size(800, 300);
			m_help.Location = new Point(this.Location.X,this.Location.Y);

			// form Mix
			m_mixForm = new MixForm();
			m_mixForm.Owner = this;
			m_help.Location = new Point(this.Location.X,this.Location.Y);

		}

		private string removeLastBackSlash(string str)
		{
			return str.EndsWith("\\") ? str.Substring(0,str.Length-1) : str;
		}

		public static void updateConfig(string strKeyword,string strValue)
		{
			try
			{
				if (File.Exists(MainForm.m_strCurrentDir + "\\config.cfg"))
				{
					// load �ѹ���
					ArrayList ar = new ArrayList();
					StreamReader sr = new StreamReader(MainForm.m_strCurrentDir + "\\config.cfg",System.Text.Encoding.GetEncoding(874));
					string strTmp = "";
					while ((strTmp = sr.ReadLine()) != null)
					{
						ar.Add(strTmp);
					}
					sr.Close();

					// ��¹����
					StreamWriter sw = new StreamWriter(MainForm.m_strCurrentDir + "\\config.cfg",false,System.Text.Encoding.GetEncoding(874));
					for (int x = 0; x < ar.Count; x++)
					{
						if (!((string)(ar[x])).StartsWith(strKeyword))
						{
							sw.WriteLine(((string)(ar[x])));
						}
						else
						{
							sw.WriteLine(strKeyword + "=" + strValue);
						}
					}
					sw.Close();
				}
				else
				{
					StreamWriter sw = new StreamWriter(MainForm.m_strCurrentDir + "\\config.cfg",false,System.Text.Encoding.GetEncoding(874));
					sw.WriteLine(strKeyword + "=" + strValue);
					sw.Close();
				}
			}
			catch(Exception ex)
			{
			}
		}

		// ��Ҥ��� m_hashSongIndex ���ŧ�� txt file
		public void saveHashSongIndex()
		{
			// �ŧ� list ��� backup ���¤��� list ��� �繤�������ش��������
			saveNSI("songIndexMirrorSE.nsi");


//			StreamWriter sw = new StreamWriter(MainForm.m_strCurrentDir + "\\songIndexMirrorSE.nsi",false,System.Text.Encoding.GetEncoding(874));
//			string strReturn = "";
//			System.Collections.IDictionaryEnumerator m_idic = m_hashSongIndex.GetEnumerator();
//			m_idic.Reset();
//			while (m_idic.MoveNext())
//			{
//				SongStructure ss = (SongStructure)m_idic.Value;
//				strReturn = "";
//				strReturn += ss.m_strNo + "\t"; //No
//				strReturn += ss.m_strSongName + "\t"; //SongName
//				strReturn += ss.m_strArtistName + "\t"; //ArtistName
//				strReturn += ss.m_strAlbumName + "\t"; //AlbumName
//				strReturn += ss.m_strFileName + "\t"; // filename ����.ext
//			
//				string strTmpTime = ss.m_strTimejump;
//				strTmpTime = strTmpTime == "" ? "0" : strTmpTime;
//				strReturn += strTmpTime + "\t"; //Time Jump	
//			
//				strTmpTime = ss.m_strTimeEnd;
//				strTmpTime = strTmpTime == "" ? "0" : strTmpTime;
//				strReturn += strTmpTime + "\t"; //Time end
//				strReturn += "\t"; // ect1
//				sw.WriteLine( strReturn );
//			}			
//			sw.Close();
		}

		public SongStructure songConvert(string strTmp)
		{
			SongStructure ss = new SongStructure();
			string []strArrTmp = strTmp.Split('\t');
			ss.m_strNo = strArrTmp[0].Trim();
			ss.m_strSongName = strArrTmp[1].Trim();
			ss.m_strArtistName = strArrTmp[2].Trim();
			ss.m_strAlbumName = strArrTmp[3].Trim();
			ss.m_strFileName = strArrTmp[4].Trim();
			ss.m_strTimejump = strArrTmp[5].Trim();
			ss.m_strTimeEnd = strArrTmp[6].Trim();
			try
			{
				ss.m_strEct1 = strArrTmp[7].Trim();
				ss.m_strEct2 = strArrTmp[8].Trim();
				ss.m_strEct3 = strArrTmp[9].Trim();
				ss.m_strEct4 = strArrTmp[10].Trim();
				ss.m_strEct5 = strArrTmp[11].Trim();
			}
			catch(Exception ex)
			{
			}
			return ss;
		}

		// ��Ҥ��� listview ���ŧ�� txt file
		public string getTxtFromListView(ListView listView1,int iIndex,string strGUID)
		{
			string strReturn = "";
			strReturn += listView1.Items[iIndex].SubItems[2].Text + "\t"; //No
			strReturn += listView1.Items[iIndex].SubItems[3].Text + "\t"; //SongName
			strReturn += listView1.Items[iIndex].SubItems[4].Text + "\t"; //ArtistName
			strReturn += listView1.Items[iIndex].SubItems[5].Text + "\t"; //AlbumName
			strReturn += strGUID + "\t"; // filename ����.ext
			
			string strTmpTime = listView1.Items[iIndex].SubItems[6].Text;
			strTmpTime = strTmpTime == "" ? "0" : strTmpTime;
			strReturn += strTmpTime + "\t"; //Time Jump	
			
			strTmpTime = listView1.Items[iIndex].SubItems[7].Text;
			strTmpTime = strTmpTime == "" ? "0" : strTmpTime;
			strReturn += strTmpTime + "\t"; //Time end
			try
			{
				strReturn += listView1.Items[iIndex].SubItems[8].Text + "\t"; // ect1
				strReturn += listView1.Items[iIndex].SubItems[9].Text + "\t"; // ect2
				strReturn += listView1.Items[iIndex].SubItems[10].Text + "\t"; // ect3
				strReturn += listView1.Items[iIndex].SubItems[11].Text + "\t"; // ect4
				strReturn += listView1.Items[iIndex].SubItems[12].Text + "\t"; // ect5
			}
			catch(Exception ex)
			{
			}
			return strReturn;
		}

		public string []checkNFixText(ref string str)
		{
			string strTmp = "";
			string []strArr = str.Replace(";",",").Split(','); // ����¹ ; �� , ���Ǥ�����
			for (int x = 0; x < strArr.Length; x++)
			{
				strArr[x] = removeLastBackSlash(strArr[x].Trim());
				strTmp += strArr[x] + ",";				
			}
			strTmp = strTmp.Substring(0,strTmp.Length-1);
			str = strTmp;
			return strArr;
		}

		// ��˹���ҡ�á����� (���ͧ��á�����)
		public void setStrKey(string strSet)
		{
			if (strSet == "")
			{				
				return;
			}

			m_strKey = "00000000000000000000" + strSet;
			m_strKey = m_strKey.Substring(m_strKey.Length - m_iSongSize,m_iSongSize);
			string strTmpStrKey = m_strKey; // ���������ŧ
			SongStructure ss = null; //string strTmpSongName = ""; // ���纪����ŧ���
			m_strKey = checkFileBeforePlay(m_strKey,ref ss); // �� path �������
			if (m_strKey == "__SONG_CODE_ERROR__")
			{
				// ������ŧ
				//MessageBox.Show( this, "�鹾�������ŧ�����ҧ���١��ͧ !!", "����͹..", MessageBoxButtons.OK, MessageBoxIcon.Stop );
				MessageForm("�鹾�������ŧ��\r\n���ҧ���١��ͧ !!");
			}
			else if (m_strKey == "")
			{
				// ������ŧ
				//MessageBox.Show( this, "��辺�ŧ���� " + strTmpStrKey, m_strProgramName + " ����͹..", MessageBoxButtons.OK, MessageBoxIcon.Stop );
				MessageForm("��辺�ŧ���� " + strTmpStrKey);
			}
			else
			{
				ss.m_strFilePlayer = m_strKey; // ������� m_strKey �� path ����ش�ҡ checkFileBeforePlay �Ҩ֧������������
				m_arrSongList.Add(ss);
				m_strKey = "";
				startSong();
			}
		}	

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
//			while(m_bRunProgram)
//			{
				try
				{
					Application.Run( new MainForm() );
				}
				catch (Exception ee)
				{
					StreamWriter file;
					string strFileError = "error.log";
					if (File.Exists(strFileError))
					{
						file = File.AppendText(strFileError);
					}
					else
					{
						file = File.CreateText(strFileError);
					}
					file.WriteLine(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " Error.." + ee.Message);
					file.Close();
					MessageBox.Show( ee.Message, m_strProgramName , MessageBoxButtons.OK, MessageBoxIcon.Stop );
				}
//			}
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				CloseInterfaces();
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.mainMenu1 = new System.Windows.Forms.MainMenu(this.components);
            this.menuTopFile = new System.Windows.Forms.MenuItem();
            this.menuFileOpenClip = new System.Windows.Forms.MenuItem();
            this.menuFileCloseClip = new System.Windows.Forms.MenuItem();
            this.menuFileSep1 = new System.Windows.Forms.MenuItem();
            this.menuFileExit = new System.Windows.Forms.MenuItem();
            this.menuTopControl = new System.Windows.Forms.MenuItem();
            this.menuControlPause = new System.Windows.Forms.MenuItem();
            this.menuControlStop = new System.Windows.Forms.MenuItem();
            this.menuControlMute = new System.Windows.Forms.MenuItem();
            this.menuControlSep1 = new System.Windows.Forms.MenuItem();
            this.menuControlStep = new System.Windows.Forms.MenuItem();
            this.menuControlSep2 = new System.Windows.Forms.MenuItem();
            this.menuControlHalf = new System.Windows.Forms.MenuItem();
            this.menuControlThreeQ = new System.Windows.Forms.MenuItem();
            this.menuControlNormal = new System.Windows.Forms.MenuItem();
            this.menuControlDouble = new System.Windows.Forms.MenuItem();
            this.menuControlSep3 = new System.Windows.Forms.MenuItem();
            this.menuControlFullScr = new System.Windows.Forms.MenuItem();
            this.menuTopRate = new System.Windows.Forms.MenuItem();
            this.menuRateIncr = new System.Windows.Forms.MenuItem();
            this.menuRateDecr = new System.Windows.Forms.MenuItem();
            this.menuRateSep1 = new System.Windows.Forms.MenuItem();
            this.menuRateNormal = new System.Windows.Forms.MenuItem();
            this.menuRateHalf = new System.Windows.Forms.MenuItem();
            this.menuRateDouble = new System.Windows.Forms.MenuItem();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.leftSpeaker = new System.Windows.Forms.MenuItem();
            this.bothSpeaker = new System.Windows.Forms.MenuItem();
            this.rightSpeaker = new System.Windows.Forms.MenuItem();
            this.menuItem5 = new System.Windows.Forms.MenuItem();
            this.incLeftVolume = new System.Windows.Forms.MenuItem();
            this.DecLeftVolume = new System.Windows.Forms.MenuItem();
            this.incRightVolume = new System.Windows.Forms.MenuItem();
            this.decRightVolume = new System.Windows.Forms.MenuItem();
            this.menuItem8 = new System.Windows.Forms.MenuItem();
            this.incVolume = new System.Windows.Forms.MenuItem();
            this.decVolume = new System.Windows.Forms.MenuItem();
            this.midVolume = new System.Windows.Forms.MenuItem();
            this.menuItem2 = new System.Windows.Forms.MenuItem();
            this.menuItem3 = new System.Windows.Forms.MenuItem();
            this.menuItem4 = new System.Windows.Forms.MenuItem();
            this.menuItem7 = new System.Windows.Forms.MenuItem();
            this.menuTopHelp = new System.Windows.Forms.MenuItem();
            this.menuHelpAbout = new System.Windows.Forms.MenuItem();
            this.menuItem6 = new System.Windows.Forms.MenuItem();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.UpdateSongTime = new System.Windows.Forms.Timer(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // menuTopFile
            // 
            this.menuTopFile.Index = -1;
            this.menuTopFile.Text = "";
            // 
            // menuFileOpenClip
            // 
            this.menuFileOpenClip.Index = -1;
            this.menuFileOpenClip.Text = "";
            // 
            // menuFileCloseClip
            // 
            this.menuFileCloseClip.Index = -1;
            this.menuFileCloseClip.Text = "";
            // 
            // menuFileSep1
            // 
            this.menuFileSep1.Index = -1;
            this.menuFileSep1.Text = "";
            // 
            // menuFileExit
            // 
            this.menuFileExit.Index = -1;
            this.menuFileExit.Text = "";
            // 
            // menuTopControl
            // 
            this.menuTopControl.Index = -1;
            this.menuTopControl.Text = "";
            // 
            // menuControlPause
            // 
            this.menuControlPause.Index = -1;
            this.menuControlPause.Text = "";
            // 
            // menuControlStop
            // 
            this.menuControlStop.Index = -1;
            this.menuControlStop.Text = "";
            // 
            // menuControlMute
            // 
            this.menuControlMute.Index = -1;
            this.menuControlMute.Text = "";
            // 
            // menuControlSep1
            // 
            this.menuControlSep1.Index = -1;
            this.menuControlSep1.Text = "";
            // 
            // menuControlStep
            // 
            this.menuControlStep.Index = -1;
            this.menuControlStep.Text = "";
            // 
            // menuControlSep2
            // 
            this.menuControlSep2.Index = -1;
            this.menuControlSep2.Text = "";
            // 
            // menuControlHalf
            // 
            this.menuControlHalf.Index = -1;
            this.menuControlHalf.Text = "";
            // 
            // menuControlThreeQ
            // 
            this.menuControlThreeQ.Index = -1;
            this.menuControlThreeQ.Text = "";
            // 
            // menuControlNormal
            // 
            this.menuControlNormal.Index = -1;
            this.menuControlNormal.Text = "";
            // 
            // menuControlDouble
            // 
            this.menuControlDouble.Index = -1;
            this.menuControlDouble.Text = "";
            // 
            // menuControlSep3
            // 
            this.menuControlSep3.Index = -1;
            this.menuControlSep3.Text = "";
            // 
            // menuControlFullScr
            // 
            this.menuControlFullScr.Index = -1;
            this.menuControlFullScr.Text = "";
            // 
            // menuTopRate
            // 
            this.menuTopRate.Index = -1;
            this.menuTopRate.Text = "";
            // 
            // menuRateIncr
            // 
            this.menuRateIncr.Index = -1;
            this.menuRateIncr.Text = "";
            // 
            // menuRateDecr
            // 
            this.menuRateDecr.Index = -1;
            this.menuRateDecr.Text = "";
            // 
            // menuRateSep1
            // 
            this.menuRateSep1.Index = -1;
            this.menuRateSep1.Text = "";
            // 
            // menuRateNormal
            // 
            this.menuRateNormal.Index = -1;
            this.menuRateNormal.Text = "";
            // 
            // menuRateHalf
            // 
            this.menuRateHalf.Index = -1;
            this.menuRateHalf.Text = "";
            // 
            // menuRateDouble
            // 
            this.menuRateDouble.Index = -1;
            this.menuRateDouble.Text = "";
            // 
            // menuItem1
            // 
            this.menuItem1.Index = -1;
            this.menuItem1.Text = "";
            // 
            // leftSpeaker
            // 
            this.leftSpeaker.Index = -1;
            this.leftSpeaker.Text = "";
            // 
            // bothSpeaker
            // 
            this.bothSpeaker.Index = -1;
            this.bothSpeaker.Text = "";
            // 
            // rightSpeaker
            // 
            this.rightSpeaker.Index = -1;
            this.rightSpeaker.Text = "";
            // 
            // menuItem5
            // 
            this.menuItem5.Index = -1;
            this.menuItem5.Text = "";
            // 
            // incLeftVolume
            // 
            this.incLeftVolume.Index = -1;
            this.incLeftVolume.Text = "";
            // 
            // DecLeftVolume
            // 
            this.DecLeftVolume.Index = -1;
            this.DecLeftVolume.Text = "";
            // 
            // incRightVolume
            // 
            this.incRightVolume.Index = -1;
            this.incRightVolume.Text = "";
            // 
            // decRightVolume
            // 
            this.decRightVolume.Index = -1;
            this.decRightVolume.Text = "";
            // 
            // menuItem8
            // 
            this.menuItem8.Index = -1;
            this.menuItem8.Text = "";
            // 
            // incVolume
            // 
            this.incVolume.Index = -1;
            this.incVolume.Text = "";
            // 
            // decVolume
            // 
            this.decVolume.Index = -1;
            this.decVolume.Text = "";
            // 
            // midVolume
            // 
            this.midVolume.Index = -1;
            this.midVolume.Text = "";
            // 
            // menuItem2
            // 
            this.menuItem2.Index = -1;
            this.menuItem2.Text = "";
            // 
            // menuItem3
            // 
            this.menuItem3.Index = -1;
            this.menuItem3.Text = "";
            // 
            // menuItem4
            // 
            this.menuItem4.Index = -1;
            this.menuItem4.Text = "";
            // 
            // menuItem7
            // 
            this.menuItem7.Index = -1;
            this.menuItem7.Text = "";
            // 
            // menuTopHelp
            // 
            this.menuTopHelp.Index = -1;
            this.menuTopHelp.Text = "";
            // 
            // menuHelpAbout
            // 
            this.menuHelpAbout.Index = -1;
            this.menuHelpAbout.Text = "";
            // 
            // menuItem6
            // 
            this.menuItem6.Index = -1;
            this.menuItem6.Text = "";
            // 
            // timer1
            // 
            this.timer1.Interval = 200;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(800, 20);
            this.label1.TabIndex = 0;
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label2.Font = new System.Drawing.Font("KodchiangUPC", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label2.ForeColor = System.Drawing.Color.Aqua;
            this.label2.Location = new System.Drawing.Point(0, 568);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(800, 32);
            this.label2.TabIndex = 2;
            this.label2.Text = "BenNueng@Gmail.com";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // timer2
            // 
            this.timer2.Interval = 5000;
            // 
            // UpdateSongTime
            // 
            this.UpdateSongTime.Enabled = true;
            this.UpdateSongTime.Interval = 500;
            this.UpdateSongTime.Tick += new System.EventHandler(this.UpdateSongTime_Tick);
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.label3.Location = new System.Drawing.Point(300, 475);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(200, 95);
            this.label3.TabIndex = 3;
            this.label3.Text = "00:00";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // MainForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(31, 73);
            this.BackColor = System.Drawing.Color.MidnightBlue;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "BenNueng@Gmail.com";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ResumeLayout(false);

		}
	#endregion

		#region DirectShow
		
		private void MainForm_Activated(object sender, System.EventArgs e)
		{
			if( firstActive )
				return;
			firstActive = true;

//			if( ! DsUtils.IsCorrectDirectXVersion() )
//			{
//				//MessageBox.Show( this, "DirectX 8.1 NOT installed!", m_strProgramName , MessageBoxButtons.OK, MessageBoxIcon.Stop );
//				MessageForm("DirectX 8.1 Not installed!");
//				this.Close(); return;
//			}

			
			// picture 
			// case1
			//pictureBox1.Location = new Point((this.Width-800) /2,(this.Height-600) /2);

            //// case2
            //pictureBox1.Location = label1.Location;
            //pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            //pictureBox1.Size = new Size(this.Width,this.Height);			
		}
		

		private void menuFileExit_Click( object sender, System.EventArgs e )
		{
			this.Close();
		}

		public void saveStat()
		{
			Stream st = null;
			try
			{
				st = File.Open(MainForm.m_strCurrentDir + "\\Stat.nst",FileMode.Create);
				IFormatter bf = new BinaryFormatter();
				bf.Serialize(st,m_hashStatSong);
			}
			catch(Exception ex)
			{
				//MessageBox.Show(ex.Message,"Nueng Karaoke Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
				string str = ex.Message;
			}
			finally
			{
				st.Close();
			}
		}

		private void MainForm_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			CloseInterfaces();
			saveStat();
			saveHashSongIndex();
			deleteASC();
		}

		private void MainForm_Resize(object sender, System.EventArgs e)
		{
			ResizeVideoWindow();	// also resize video preview
		}

		
		// ��Ǩ���ŧ����¹
		private void Register()
		{
			register regis = new register();
			if (!regis.checkRegistered()) throw new Exception("Error !!");
		}

		// method �������ŧ
		public void startSong()
		{
			// ���������ŧ� C:\\nueng �����Ẻ Network ���ӡ�� copy ��..
			// ��������ǡ�����ͧ ��蹨ҡ local �����

			// ��������ش�ش���� ��͹���ŧ����¡ thread � copy �ҡ ����ͧ��� �������������ͧ local����


			if (m_arrSongList.Count > 0 && !m_bNowPlaying)
			{
				SongStructure ss = (SongStructure)m_arrSongList[0];
				m_strKey = ss.m_strFilePlayer;
				label2.Text = ss.m_strSongName + " - " + ss.m_strArtistName;
				openClip(m_strKey,ss); // �����ŧ����� path
				m_strKey = "";
			}
		}

		public void openClip(string strFilename,SongStructure ssstrID)
		{
			// ʶԵ�...
			// ��ҫ����� + ����
			if (MainForm.m_hashStatSong.ContainsKey(ssstrID.m_strNo))
			{
				int x = Convert.ToInt32((string)MainForm.m_hashStatSong[ssstrID.m_strNo]) + 1;
				MainForm.m_hashStatSong[ssstrID.m_strNo] = x.ToString();
			}
			else
			{
				// ��������
				MainForm.m_hashStatSong[ssstrID.m_strNo] = string.Format("{0}",1);
			}

			// ��觻Դ�ŧ��ҷ����������͹
			menuFileCloseClip_Click( null, null );

			// �����Ң�й����ѧ���������ŧ
			m_bNowPlaying = true;
			timer2.Stop();
			
			// ��˹��ŧ����ͧ������
            clipFile = strFilename; // ����ŧ����ͧ������

            /////////////////////////////////////////////////////////////////////////////////////////////
            // ��Ǩ�ͺ server ��͹�����蹵ç���
            if (checkAuthentication())
            {
                if (!PlayClip(ssstrID))
                {
                    m_bNowPlaying = false;
                    timer2.Start();
                    menuFileCloseClip_Click(null, null);
                    afterComplete();
                }
                else
                {
                    // ����ŧ����� ���ʴ��������������������
                    playSongAuth(ssstrID.m_strNo);
                }
            }
            else
            {
                // ������Ѻ͹حҵ�
                afterComplete();
                MessageForm("�Թ������Ǩ���� .. \r\n ����Թ��͹��..(^^)");
            }
            /////////////////////////////////////////////////////////////////////////////////////////////

			UpdatePlaybackMenu();
			UpdateMainTitle();
			
			m_strKey = "";
		}

		private void menuFileOpenClip_Click(object sender, System.EventArgs e)
		{
//			OpenFileDialog af = new OpenFileDialog();
//			af.Title = "Open Media File...";
//			af.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory;
//			af.Filter = clipFileFilters;
//			if( af.ShowDialog() != DialogResult.OK )
//				return;
//
//			menuFileCloseClip_Click( null, null );
//
//			clipFile = af.FileName;
//			if( ! PlayClip() )
//				menuFileCloseClip_Click( null, null );
//
//			UpdatePlaybackMenu();
//			UpdateMainTitle();
		}

		public void closeClip()
		{
			menuFileCloseClip_Click(null,null);
		}

		private void menuFileCloseClip_Click(object sender, System.EventArgs e)
		{
			clipFile = null;
			clipType = ClipType.None;
			CloseInterfaces();

			UpdatePlaybackMenu();
			UpdateMainTitle();
			InitPlayerWindow();
			this.Refresh();
		}

		/// <summary> start all the interfaces, graphs and preview window. </summary>
		bool PlayClip(SongStructure ssstrID)
		{
			try 
			{
				CloseInterfaces();
				//com.DTREnable = false;

				if( ! GetInterfaces() )
					return false;
			
				CheckClipType();
				if( clipType == ClipType.None )
					return false;

				int hr = mediaEvt.SetNotifyWindow( this.Handle, WM_GRAPHNOTIFY, IntPtr.Zero );
		
				if( (clipType == ClipType.AudioVideo) || (clipType == ClipType.VideoOnly) )
				{
					videoWin.put_Owner( this.Handle );
					videoWin.put_WindowStyle( WS_CHILD | WS_CLIPSIBLINGS | WS_CLIPCHILDREN );

					InitVideoWindow( 1, 1 );
					CheckSizeMenu( menuControlNormal );
					GetFrameStepInterface();
				}
				else
					InitPlayerWindow();

				hr = mediaCtrl.Run();
				if( hr >= 0 )
				{
					playState = PlayState.Running;
					
					// reconfig here !!
					// ���§���¢��
					if (m_iSpeaker == 1) bothSpeakerClip();
					else if (m_iSpeaker == 2) rightSpeakerClip();
					else leftSpeakerClip();

					// ��Ѻ��������
					SetRate( m_dRate ); 
					
					// ���ⴴ��������
					setTimeJump( ssstrID.m_strTimejump );

					// ��˹�������ش
					setTimeEnd( ssstrID.m_strTimeEnd );

					// ��˹������ѧ�ŧ
					basicAudio.put_Volume( savedVolume );

					// �Ҿ�����
					//fullScreen = true;
					//ToggleFullScreen();
				}
				return hr >= 0;
			}
			catch( Exception ee )
			{
				//MessageBox.Show( this, "Could not start clip\r\n" + ee.Message, m_strProgramName, MessageBoxButtons.OK, MessageBoxIcon.Stop );
				MessageForm("Could not start clip\r\n" + ee.Message);
				return false;
			}
		}


		/// <summary> create the used COM components and get the interfaces. </summary>
		bool GetInterfaces()
		{
			Type comtype = null;
			object comobj = null;
			try 
			{
				comtype = Type.GetTypeFromCLSID( Clsid.FilterGraph );
				if( comtype == null )
					throw new NotSupportedException( "DirectX (8.1 or higher) not installed?" );
				comobj = Activator.CreateInstance( comtype );
				graphBuilder = (IGraphBuilder) comobj; comobj = null;
		
				int hr = graphBuilder.RenderFile( clipFile, null );
				if( hr < 0 )
					Marshal.ThrowExceptionForHR( hr );

				mediaCtrl	= (IMediaControl)  graphBuilder;
				mediaEvt	= (IMediaEventEx)  graphBuilder;
				mediaSeek	= (IMediaSeeking)  graphBuilder;
				mediaPos	= (IMediaPosition) graphBuilder;

				videoWin	= graphBuilder as IVideoWindow;
				basicVideo	= graphBuilder as IBasicVideo2;
				basicAudio	= graphBuilder as IBasicAudio;
				return true;
			}
			catch( Exception ee )
			{
				MessageBox.Show( this, "Could not get interfaces\r\n" + ee.Message, m_strProgramName, MessageBoxButtons.OK, MessageBoxIcon.Stop );
				return false;
			}
			finally
			{
				if( comobj != null )
					Marshal.ReleaseComObject( comobj ); comobj = null;
			}
		}



		/// <summary> try to get the step interfaces. </summary>
		bool GetFrameStepInterface()
		{
			videoStep = graphBuilder as IVideoFrameStep;
			if( videoStep == null )
				return false;

			// Check if this decoder can step
			int hr = videoStep.CanStep( 0, null );
			if( hr != 0 )
			{
				videoStep = null;
				return false;
			}
			return true;
		}


		/// <summary> try to detect clip type (video/audio) [not reliable]. </summary>
		void CheckClipType()
		{
			if( basicAudio == null )
				clipType = ClipType.None;
			else
				clipType = ClipType.AudioOnly;

			if( (videoWin == null) || (basicVideo == null) )
				return;

			int visible;
			int hr = videoWin.get_Visible( out visible );
			if( hr < 0 )
				return;
			else
			{
				if( basicAudio == null )
					clipType = ClipType.VideoOnly;
				else
					clipType = ClipType.AudioVideo;
			}
		}


		/// <summary> configure video preview window. </summary>
		bool InitVideoWindow( int multiplier, int divider )
		{
			if( basicVideo == null )
				return false;

			int height, width;//, hr;

			// Read the default video size
			//hr = basicVideo.GetVideoSize( out width, out height );
			//if( (hr != 0) || (width < 16) || (height < 16) )
			//	return true;

			// Account for requests of normal, half, or double size
			//width  = width  * multiplier / divider;
			//height = height * multiplier / divider;
			width = 800;
			height = 600;
	
			this.ClientSize = new Size( width, height );
			ResizeVideoWindow();
			return true;
		}

		/// <summary> configure window for audio playback. </summary>
		bool InitPlayerWindow()
		{
			this.ClientSize = new Size( 800, 600 );
			return true;
		}


		/// <summary> resize preview video window to fill client area. </summary>
		void ResizeVideoWindow()
		{
			if( videoWin == null )
				return;
			Rectangle rc = this.ClientRectangle;
			int hr = videoWin.SetWindowPosition( 0, 0, rc.Right, rc.Bottom );
		}


		/// <summary> enable menu items to match current state. </summary>
		void UpdatePlaybackMenu()
		{
			menuFileCloseClip.Enabled = clipFile != null;

			bool enable = playState != PlayState.Init;
			menuControlPause.Enabled	= enable;
			menuControlStop.Enabled		= enable;
			menuControlMute.Enabled		= enable;
			menuControlStep.Enabled		= videoStep != null;

			menuRateIncr.Enabled		= enable;
			menuRateDecr.Enabled		= enable;
			menuRateNormal.Enabled		= enable;
			menuRateHalf.Enabled		= enable;
			menuRateDouble.Enabled		= enable;

			enable = enable && (clipType != ClipType.AudioOnly);
			menuControlHalf.Enabled		= enable;
			menuControlThreeQ.Enabled	= enable;
			menuControlNormal.Enabled	= enable;
			menuControlDouble.Enabled	= enable;
			menuControlFullScr.Enabled	= enable;
		}


		/// <summary> do cleanup and release DirectShow. </summary>
		void CloseInterfaces()
		{
			int hr;
			try 
			{
				if( mediaCtrl != null )
				{
					hr = mediaCtrl.StopWhenReady();
					mediaCtrl = null;
				}

				playState = PlayState.Stopped;

				if( mediaEvt != null )
				{
					hr = mediaEvt.SetNotifyWindow( IntPtr.Zero, WM_GRAPHNOTIFY, IntPtr.Zero );
					mediaEvt = null;
				}

				if( videoWin != null )
				{
					hr = videoWin.put_Visible( DsHlp.OAFALSE );
					hr = videoWin.put_Owner( IntPtr.Zero );
					videoWin = null;
				}

				mediaSeek	= null;
				mediaPos	= null;
				basicVideo	= null;
				videoStep	= null;
				basicAudio	= null;

				if( graphBuilder != null )
					Marshal.ReleaseComObject( graphBuilder ); graphBuilder = null;

				playState = PlayState.Init;
			}
			catch( Exception )
			{}
		}


		/// <summary> override window fn to handle graph events. </summary>
		protected override void WndProc( ref Message m )
		{
			if( m.Msg == WM_GRAPHNOTIFY )
			{
				if( mediaEvt != null )
					OnGraphNotify();
				return;
			}
			base.WndProc( ref m );
		}

		/// <summary> graph event (WM_GRAPHNOTIFY) handler. </summary>
		void OnGraphNotify()
		{
			int p1, p2, hr = 0;
			DsEvCode code;
			do
			{
				hr = mediaEvt.GetEvent( out code, out p1, out p2, 0 );
				if( hr < 0 )
					break;
				hr = mediaEvt.FreeEventParams( code, p1, p2 );
				if( code == DsEvCode.Complete )
				{
					//OnClipCompleted();
					hr = 1;
					afterComplete();
				}
			}
			while( hr == 0 );
		}

		private void afterComplete()
		{
			CloseInterfaces();
			// ������ŧ�� ��� ����繡������¡���á
			if (m_arrSongList.Count > 0 && !m_bPlayByInsertSong)
			{
				m_arrSongList.RemoveAt(0);
			}
			else
			{
				m_bPlayByInsertSong = false; // �ŧ������蹵�����ͧ��
			}

			m_songListControl.refreshList(); // ����Դ��÷ӧҹ�������� refresh list
			label2.Text = m_strTitle;
			timer1.Enabled = true; // ���¡ timer1 �ҷӧҹ ����˹�ǧ������������¹�ŧ
			timer2.Start(); // ���¡�ɳ� �ҷӧҹ
			m_bAdsPlay = false; // �ɳ���蹨�����
		}

		/// <summary> graph event if clip has finished </summary>
		/*void OnClipCompleted()
		{
			if( (mediaCtrl == null) || (mediaSeek == null) )
				return;

			DsOptInt64 pos = new DsOptInt64( 0 );
			int hr = mediaSeek.SetPositions( pos, SeekingFlags.AbsolutePositioning, null, SeekingFlags.NoPositioning );
			if( hr == 0 )
				return;
			hr = mediaCtrl.Stop();
			if( hr < 0 )
				return;
			hr = mediaCtrl.Run();
		}*/

		/// <summary> menu clicked to pause the playback. </summary>
		private void menuControlPause_Click(object sender, System.EventArgs e)
		{
			if( mediaCtrl == null )
				return;

			if( (playState == PlayState.Paused) || (playState == PlayState.Stopped) )
			{
				if( mediaCtrl.Run() == 0 )
					playState = PlayState.Running;
			}
			else if( playState == PlayState.Running )
			{
				if( mediaCtrl.Pause() == 0 )
					playState = PlayState.Paused;
			}
			UpdateMainTitle();
		}

		public void pausePlayClip()
		{
			menuControlPause_Click(null,null);
		}

		/// <summary> menu clicked to stop the playback. </summary>
		private void menuControlStop_Click(object sender, System.EventArgs e)
		{
			if( (mediaCtrl == null) || (mediaSeek == null) )
				return;

			if( (playState != PlayState.Paused) && (playState != PlayState.Running) )
				return;

			int hr = mediaCtrl.Stop();
			playState = PlayState.Stopped;

			DsLong pos = new DsLong( 0 );
			hr = mediaSeek.SetPositions( pos, AMSeekingSeekingFlags.AbsolutePositioning, null, AMSeekingSeekingFlags.NoPositioning );
			hr = mediaCtrl.Pause();
			UpdateMainTitle();
		}

		public void stop()
		{
			menuControlStop_Click(null,null);
		}

		/// <summary> menu clicked to mute audio. </summary>
		private void menuControlMute_Click(object sender, System.EventArgs e)
		{
			int hr, currentVolume;

			if( (graphBuilder == null) || (basicAudio == null) )
				return;
			hr = basicAudio.get_Volume( out currentVolume );
			if( hr != 0 )
				return;

			if( currentVolume != -10000 )   // midi may use -9640 ???
			{
				savedVolume = currentVolume;
				currentVolume = -10000;
				menuControlMute.Checked = true;
			}
			else
			{
				currentVolume = savedVolume;
				menuControlMute.Checked = false;
			}

			hr = basicAudio.put_Volume( currentVolume );
			hr += 1;
		}

		public void muteClip()
		{
			menuControlMute_Click(null,null);
		}

		/// <summary> menu clicked to step one frame. </summary>
		private void menuControlStep_Click(object sender, System.EventArgs e)
		{
			int hr;
			if( (videoStep == null) || (mediaCtrl == null) )
				return;
			if( playState != PlayState.Paused )
				hr = mediaCtrl.Pause();
			playState = PlayState.Paused;
			hr = videoStep.Step( 1, null );
		}

		public void stepClip()
		{
			menuControlStep_Click(null,null);
		}

		/// <summary> step n-numbers of frames in stream. </summary>
		private void StepFrames( int frames )
		{
			int hr;
			if( (videoStep == null) || (mediaCtrl == null) )
				return;
			hr = videoStep.CanStep( frames, null );
			if( hr != 0 )
				return;
			if( playState != PlayState.Paused )
				hr = mediaCtrl.Pause();
			playState = PlayState.Paused;
			hr = videoStep.Step( frames, null );
		}

		private void menuControlHalf_Click(object sender, System.EventArgs e)
		{
			InitVideoWindow( 1, 2 );
			CheckSizeMenu( menuControlHalf );
		}

		public void halfSize()
		{
			menuControlHalf_Click(null,null);
		}

		private void menuControlThreeQ_Click(object sender, System.EventArgs e)
		{
			InitVideoWindow( 3, 4 );
			CheckSizeMenu( menuControlThreeQ );
		}

		public void threeQSize()
		{
			menuControlThreeQ_Click(null,null);
		}

		private void menuControlNormal_Click(object sender, System.EventArgs e)
		{
			InitVideoWindow( 1, 1 );
			CheckSizeMenu( menuControlNormal );
		}

		public void normalSize()
		{
			menuControlNormal_Click(null,null);
		}

		private void menuControlDouble_Click(object sender, System.EventArgs e)
		{
			InitVideoWindow( 2, 1 );
			CheckSizeMenu( menuControlDouble );
		}

		public void doubleSize()
		{
			menuControlDouble_Click(null,null);
		}

		void CheckSizeMenu( MenuItem checkItem )
		{
			menuControlHalf.Checked		= checkItem == menuControlHalf;
			menuControlThreeQ.Checked	= checkItem == menuControlThreeQ;
			menuControlNormal.Checked	= checkItem == menuControlNormal;
			menuControlDouble.Checked	= checkItem == menuControlDouble;
		}


		private void menuControlFullScr_Click(object sender, System.EventArgs e)
		{
			ToggleFullScreen();
		}

		public void fullScreenClip()
		{
			menuControlFullScr_Click(null,null);
		}

		private void menuRateIncr_Click(object sender, System.EventArgs e)
		{
			ModifyRate( m_dModifyRateInc );
		}

		public void increaseRate()
		{
			menuRateIncr_Click(null,null);
		}

		private void menuRateDecr_Click(object sender, System.EventArgs e)
		{
			ModifyRate( m_dModifyRateDec );
		}

		public void decreaseRate()
		{
			menuRateDecr_Click(null,null);
		}

		private void menuRateNormal_Click(object sender, System.EventArgs e)
		{
			SetRate( 1.0 );
		}

		public void normalRate()
		{
			menuRateNormal_Click(null,null);
		}

		private void menuRateHalf_Click(object sender, System.EventArgs e)
		{
			SetRate( 0.5 );
		}

		public void halfRate()
		{
			menuRateHalf_Click(null,null);
		}

		private void menuRateDouble_Click(object sender, System.EventArgs e)
		{
			SetRate( 2.0 );
		}

		public void doubleRate()
		{
			menuRateDouble_Click(null,null);
		}

		void ModifyRate( double rateAdjust )
		{
			if( (mediaPos == null) || (rateAdjust == 0.0) )
				return;

			double rate;
			int hr = mediaPos.get_Rate( out rate );
			if( hr < 0 )
				return;
			rate += rateAdjust;
			m_dRate = rate;
			hr = mediaPos.put_Rate( rate );

			// rate
			rate -= 1.0d;
			setStatusMsg("�������� " + rate.ToString("0.00"));
		}

		void SetRate( double newRate )
		{
			if( mediaPos == null )
				return;

			int hr = mediaPos.put_Rate( newRate );
			m_dRate = newRate;
		}

		public void midVolumeClip()
		{
			midVolume_Click(null,null);
		}

		private void midVolume_Click(object sender, System.EventArgs e)
		{
			int hr, currentVolume;

			if( (graphBuilder == null) || (basicAudio == null) )
				return;
			hr = basicAudio.get_Volume( out currentVolume );
			if( hr != 0 )
				return;

			savedVolume = 0;

			hr = basicAudio.put_Volume( savedVolume );
		}

		public void decVolumeClip()
		{
			decVolume_Click(null,null);
		}

		private void decVolume_Click(object sender, System.EventArgs e)
		{
			int hr, currentVolume;

			if( (graphBuilder == null) || (basicAudio == null) )
				return;
			hr = basicAudio.get_Volume( out currentVolume );
			if( hr != 0 )
				return;

			if( currentVolume > -10000 )   // midi may use -9640 ???
			{
				currentVolume -= 100;
				savedVolume = currentVolume;
			}

			hr = basicAudio.put_Volume( currentVolume );

			setSoundLevel();
		}

		private void setSoundLevel()
		{			
			// ���§
			if (savedVolume > 500)
			{
				setStatusMsg("���§13");
			}
			else if (savedVolume < -3500)
			{
				setStatusMsg("���§0");
			}
			else 
			{
				// �ӹǹ
				int tmpsaved = savedVolume > 500 ? 500: savedVolume;
				tmpsaved = tmpsaved < -3500 ? -3500: tmpsaved;
				tmpsaved = (tmpsaved + 3500) / 269; // + 3500 ���ͨС����� + , ��� 269 �������дѺ
				setStatusMsg("���§"+tmpsaved.ToString());
			}
		}

        private void hideTextStatus()
        {
            m_status.setTxtStatus("");
            //m_status.Close();
            System.Diagnostics.Debug.Print("Mainform::hide");
        }

		private void setStatusMsg(string str)
		{
            //m_strKey = "";
			m_status.setTxtStatus(str);

			/*//m_statusPixel.setTxtStatus(str);

			if (!m_bStatusShowDialog)
			{
				m_bStatusShowDialog = true;
                m_status.ShowDialog();
                System.Diagnostics.Debug.Print("Mainform::show");
				//m_statusPixel.setVisible(true);
			}*/
		}

		public void incVolumeClip()
		{
			incVolume_Click(null,null);
		}

		private void incVolume_Click(object sender, System.EventArgs e)
		{
			int hr, currentVolume;

			if( (graphBuilder == null) || (basicAudio == null) )
				return;
			hr = basicAudio.get_Volume( out currentVolume );
			if( hr != 0 )
				return;

			if( currentVolume < 10000 )   // midi may use -9640 ???
			{
				currentVolume += 100;
				savedVolume = currentVolume;
			}

			hr = basicAudio.put_Volume( currentVolume );

			setSoundLevel();
		}

		/// <summary> update caption text of this form. </summary>
		void UpdateMainTitle()
		{
			if( clipFile == null )
			{
				this.Text = m_strProgramName;
				return;
			}
	
			string txt = Path.GetFileName( clipFile ) + " : " + clipType.ToString();
			if( playState == PlayState.Paused )
				txt = txt + " -Paused-";
			this.Text = txt;
		}

		private void setTimeEnd(string strTimeEnd)
		{
			if (strTimeEnd == "0" || strTimeEnd == "" || strTimeEnd == null)
			{
				m_strTimeEnd = "0";
				return;
			}
			m_strTimeEnd = strTimeEnd;
			mediaPos.put_StopTime(Convert.ToDouble(strTimeEnd));
		}

		private void setTimeJump(string strTimeJump)
		{
			if (strTimeJump == "0")
			{
				return;
			}
			m_strTimeJump = strTimeJump;
			mediaPos.put_CurrentPosition(Convert.ToDouble(strTimeJump));
		}

		private void setTimeEnd()
		{
			if (m_strTimeEnd == "0")
			{
				return;
			}
			mediaPos.put_StopTime(Convert.ToDouble(m_strTimeEnd));
		}

		private void setTimeJump()
		{
			if (m_strTimeJump == "0")
			{
				return;
			}
			mediaPos.put_CurrentPosition(Convert.ToDouble(m_strTimeJump));
		}

		public string getExtends(string str)
		{
			if (str.Length > 4)
			{
				return str.Substring(str.Length - 4,4);
			}
			return "";
		}

		public static string reverseString(string str)
		{
			string strReturn = "";
			for (int x = str.Length; x > 0; x--)
			{
				strReturn += str[x-1].ToString();
			}
			return strReturn;
		}

		/// <summary>
		/// </summary>
		/// <param name="strKey">�����ŧ</param>
		/// <param name="refss">�ٻẺ�ç���ҧ��������</param>
		/// <returns>�� path �ŧ���� �����</returns>
        public static string checkFileBeforePlay(string strKey, ref SongStructure refss)
        {
            if (m_hashSongIndex.ContainsKey(strKey))
            {
                SongStructure ss = (SongStructure)m_hashSongIndex[strKey];

                // ��������ŧ�Դ��Ҵ
                MyUtil myUtil = new MyUtil();
                string strTmpp = reverseString(MainForm.m_strMAC) + myUtil.decodeTxt("2AECAE656AA9ECBA7A3BFC");
                strTmpp = FormsAuthentication.HashPasswordForStoringInConfigFile(strTmpp, "MD5");

                // ����� file c:\windows\system32\kknetregister.log ������ ����ͧ��Ǩ����ҷ��仢ͧ�ŧ
                if (File.Exists(System.Environment.SystemDirectory + "\\kknetregister.log"))
                {
                    // �����������Ţ macaddress ���ç�Ѻ���� lan ��� error
                    StreamReader sr = new StreamReader(System.Environment.SystemDirectory + "\\kknetregister.log");
                    string strTmpRead = sr.ReadLine();
                    sr.Close();
                    // ��� MacAddress + "KKNetRegiser" �� "�������" ���Ǵ���ҵç�Ѻ�����������
                    if (strTmpRead == null || (strTmpRead.Trim() != myUtil.encodeTxt(reverseString(MainForm.m_strMAC) + "KKNetRegister")))
                    {
                        return "__SONG_CODE_ERROR__";
                    }
                }
                else if (ss.m_strEct1 == null || ss.m_strEct1 == "" || ss.m_strEct1 != strTmpp)
                {
                    return "__SONG_CODE_ERROR__";
                }

                //				// ��� version ���
                //				if (ss.m_strEct2 == null 
                //					|| ss.m_strEct2 == "" 
                //					|| ss.m_strEct2 != MainForm.m_strMAC)
                //					return "__SONG_CODE_ERROR__";

                // ������� dealer �Դ��Ҵ

                // ǹ�ٻ��� ����͡�ҡ hdd �ç���
                int x = 0;
                bool cmdExit = false;
                while (!cmdExit && x < MainForm.m_strArrSongPath.Count)
                {
                    string strTmpFile = ((string)(MainForm.m_strArrSongPath[x])) + ":\\Nueng\\" + ss.m_strFileName;
                    if (File.Exists(strTmpFile))
                    {
                        cmdExit = true;
                        refss = ss;
                        return strTmpFile;
                    }
                    else
                    {
                        x++;
                    }
                }
            }
            return "";
        }

		/// <summary> full-screen toggle. </summary>
		void ToggleFullScreen()
		{
			if( (clipType != ClipType.VideoOnly) && (clipType != ClipType.AudioVideo) )
				return;
			if( videoWin == null )
				return;

			int mode;
			int hr = videoWin.get_FullScreenMode( out mode );
			if( mode == DsHlp.OAFALSE )
			{
				hr = videoWin.get_MessageDrain( out drainWin );
				hr = videoWin.put_MessageDrain( this.Handle );
				mode = DsHlp.OATRUE;
				hr = videoWin.put_FullScreenMode( mode );
				if( hr >= 0 )
					fullScreen = true;
			}
			else
			{
				mode = DsHlp.OAFALSE;
				hr = videoWin.put_FullScreenMode( mode );
				if( hr >= 0 )
					fullScreen = false;
				hr = videoWin.put_MessageDrain( drainWin );
				this.BringToFront();
				this.Refresh();
			}
		}

		private void menuHelpAbout_Click( object sender, System.EventArgs e )
		{
			//MessageBox.Show( this, "Presented by:\r\nNETMaster", "ProCom Karaoke", MessageBoxButtons.OK, MessageBoxIcon.Information );
		}


		/// <summary> file name of clip. </summary>
		private	string					clipFile;

		/// <summary> flag to detect first Form appearance </summary>
		private bool					firstActive;

		/// <summary> type of clip, video / audio. </summary>
		private	ClipType				clipType;
		private	PlayState				playState;
		private	bool					fullScreen;

		private IGraphBuilder			graphBuilder;

		/// <summary> control interface. </summary>
		private IMediaControl			mediaCtrl;

		/// <summary> graph event interface. </summary>
		private IMediaEventEx			mediaEvt;

		/// <summary> seek interface for positioning in stream. </summary>
		private IMediaSeeking			mediaSeek;
		/// <summary> seek interface to set position in stream. </summary>
		private IMediaPosition			mediaPos;

		/// <summary> video preview window interface. </summary>
		private IVideoWindow			videoWin;
		/// <summary> interface to get information and control video. </summary>
		private IBasicVideo2			basicVideo;
		/// <summary> interface to single-step video. </summary>
		private IVideoFrameStep			videoStep;

		/// <summary> audio interface used to control volume. </summary>
		private IBasicAudio				basicAudio;
		private int						savedVolume = 0;
		private IntPtr					drainWin;


		private const int WM_GRAPHNOTIFY	= 0x00008001;	// message from graph

		private const int WS_CHILD			= 0x40000000;	// attributes for video window
		private const int WS_CLIPCHILDREN	= 0x02000000;
		private const int WS_CLIPSIBLINGS	= 0x04000000;

		private const string clipFileFilters =
			"Video Files (avi qt mov mpg mpeg m1v)|*.avi;*.qt;*.mov;*.mpg;*.mpeg;*.m1v|" +
			"Audio files (wav mpa mp2 mp3 au aif aiff snd)|*.wav;*.mpa;*.mp2;*.mp3;*.au;*.aif;*.aiff;*.snd|" +
			"MIDI Files (mid midi rmi)|*.mid;*.midi;*.rmi|" +
			"Image Files (jpg bmp gif tga)|*.jpg;*.bmp;*.gif;*.tga|" +
			"All Files (*.*)|*.*";

		public void leftSpeakerClip()
		{
			leftSpeaker_Click(null,null);
		}

		private void leftSpeaker_Click(object sender, System.EventArgs e)
		{
			//int hr, currentBalance;
			
			// ����ա������¹�����¡����⾧��ҧ㴢�ҧ˹����������§Ẻ �ѵ��ѵ�
			if (menuControlMute.Checked) // true = mute
			{
				menuControlMute_Click( null, null );
			}			

			if( (graphBuilder == null) || (basicAudio == null) )
				return;

			// ��ͧ�����ç���
            try
            {
                IBaseFilter ff;
                this.graphBuilder.FindFilterByName("MPEG Audio Decoder", out ff);
                IMpegAudioDecoder filter = (IMpegAudioDecoder)ff;
                filter.put_DualMode(MPEGAudioDual.Left);

                setStatusMsg("Mono /L");
            }
            catch (Exception ex)
            {
                setStatusMsg("Audio normal");
                showAudioTime();
            }

//			hr = basicAudio.get_Balance( out currentBalance );
//			if( hr != 0 )
//				return;
//
//			currentBalance = -10000;
//
//			hr = basicAudio.put_Balance( currentBalance );
		}

		public void rightSpeakerClip()
		{
			rightSpeaker_Click(null,null);
		}

		private void rightSpeaker_Click(object sender, System.EventArgs e)
		{
			//int hr, currentBalance;
			
			// ����ա������¹�����¡����⾧��ҧ㴢�ҧ˹����������§Ẻ �ѵ��ѵ�
			if (menuControlMute.Checked) // true = mute
			{
				menuControlMute_Click( null, null );
			}


            if ((graphBuilder == null) || (basicAudio == null) )
				return;

			// ��ͧ�����ç���
            try
            {
                IBaseFilter ff;
                this.graphBuilder.FindFilterByName("MPEG Audio Decoder", out ff);
                IMpegAudioDecoder filter = (IMpegAudioDecoder)ff;
                filter.put_DualMode(MPEGAudioDual.Right);

                setStatusMsg("Mono /R");
            }
            catch (Exception ex)
            {
                setStatusMsg("Audio normal");
                showAudioTime();
            }

//			hr = basicAudio.get_Balance( out currentBalance );
//			if( hr != 0 )
//				return;
//
//			currentBalance = 10000;
//
//			hr = basicAudio.put_Balance( currentBalance );
		}

		public void bothSpeakerClip()
		{
			bothSpeaker_Click(null,null);
		}

		private void bothSpeaker_Click(object sender, System.EventArgs e)
		{
			//int hr, currentBalance;

			// ����ա������¹�����¡����⾧��ҧ㴢�ҧ˹����������§Ẻ �ѵ��ѵ�
			if (menuControlMute.Checked) // true = mute
			{
				menuControlMute_Click( null, null );
			}

            if ((graphBuilder == null) || (basicAudio == null))
				return;
			
			// ��ͧ�����ç���
            try
            {
                IBaseFilter ff;
                this.graphBuilder.FindFilterByName("MPEG Audio Decoder", out ff);
                IMpegAudioDecoder filter = (IMpegAudioDecoder)ff;
                filter.put_DualMode(MPEGAudioDual.Merge);

                setStatusMsg("Stereo");
            }
            catch (Exception ex)
            {
                setStatusMsg("Audio normal");
                showAudioTime();
            }

//			hr = basicAudio.get_Balance( out currentBalance );
//			if( hr != 0 )
//				return;
//
//			currentBalance = 0;
//
//			hr = basicAudio.put_Balance( currentBalance );
		}

        // ����� audio ����ʴ�����
        private void showAudioTime()
        {
            if ((graphBuilder == null) || (basicAudio == null))
            {
                if (label3.Text != "00:00")
                {
                    label3.Text = "00:00";
                }
                // check location and change position
                int x = (label2.Width - label3.Size.Width) / 2;
                int y = label2.Location.Y - label3.Size.Height;
                label3.Location = new Point(x, y);
                return;
            }

            double dStopTime;
            double dPreTime;
            double dTimeElap;
            mediaPos.get_StopTime(out dStopTime);
            mediaPos.get_CurrentPosition(out dPreTime);
            dTimeElap = dStopTime - dPreTime;                     
            

            int itmp = (int)(dTimeElap / 60);
            string timeupdate = itmp < 10 ? "0": "";
            timeupdate += itmp.ToString() + ":";

            dTimeElap = dStopTime - dPreTime;
            itmp = (int)(dTimeElap % 60);
            timeupdate += itmp < 10 ? "0" : "";
            timeupdate += itmp.ToString();

            label3.Text = timeupdate;

            // check location and change position
            int x1 = (label2.Width - label3.Size.Width) / 2;
            int y1 = label2.Location.Y - label3.Size.Height;
            label3.Location = new Point(x1, y1);
        }

		public double getCurrentPosition()
		{
			double dCurrentPosition;
			if (mediaPos.get_CurrentPosition(out dCurrentPosition) == 0)
			{
				return dCurrentPosition;
			}
			return 0.0;
		}

		private void menuItem3_Click(object sender, System.EventArgs e)
		{
			double dCurrentPosition;
			if (mediaPos.get_CurrentPosition(out dCurrentPosition) == 0)
			{
				MessageBox.Show("Current time = " + dCurrentPosition.ToString());
			}
		}

		public double getStopTime()
		{
			double dStopTime;
			if (mediaPos.get_StopTime(out dStopTime) == 0)
			{
				return dStopTime;
			}
			return 0.0;
		}

		private void menuItem4_Click(object sender, System.EventArgs e)
		{
			double dStopTime;
			if (mediaPos.get_StopTime(out dStopTime) == 0)
			{
				MessageBox.Show("Stop time = " + dStopTime.ToString());
			}
		}

		private void menuItem6_Click(object sender, System.EventArgs e)
		{
			MessageBox.Show( this, "Nueng : " + m_strTitle, m_strProgramName, MessageBoxButtons.OK, MessageBoxIcon.Information );
		}
		#endregion

		private void menuItem7_Click(object sender, System.EventArgs e)
		{
			double dStopTime;
			if (mediaPos.get_StopTime(out dStopTime) == 0)
			{
				//MessageBox.Show("Stop time = " + dStopTime.ToString());
				mediaPos.put_CurrentPosition(dStopTime - 10.0);
			}
		}

		private void timer1_Tick(object sender, System.EventArgs e)
		{
			timer1.Enabled = false;
			m_bNowPlaying = false;
			
			bool cmdExit = false;
			while(!cmdExit)
			{
				if (m_arrSongList.Count > 0)
				{
					SongStructure ss = (SongStructure)m_arrSongList[0];
					if (File.Exists(ss.m_strFilePlayer))
					{
						m_strKey = ss.m_strFilePlayer;
						setSongTxt(ss.m_strSongName + " - " + ss.m_strArtistName);
						openClip(m_strKey,ss); // �����ŧ����� path
						m_strKey = "";
						cmdExit = true; // �͡��ͺ���� // �������ըеԴ�ٻ !!
					}
					else
					{
						// �����������
						m_arrSongList.RemoveAt(0);
						m_songListControl.refreshList(); // ����Դ��÷ӧҹ�������� refresh list
					}
				}
				else
				{
					cmdExit = true; // �������ըеԴ�ٻ !!
				}
			}
		}

        // set txt label2
		public void setSongTxt(string str)
		{
			label2.Text = str;
		}

		

		// set �����š�͹��� processkey
		private void processKeyStatus(char ch)
		{
			if (m_strKey.Length < m_iSongSize)
			{
				m_strKey += ch.ToString();

                setStatusMsg(m_strKey);
			}
		}

		
		public void onKeyUp(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			//if( playState == PlayState.Init )
			//return;

			switch( e.KeyCode )
			{
#if __SOR_AOR__
                case Keys.Insert:
#endif
                case Keys.NumPad0:
                case Keys.D0: { processKeyStatus('0'); break; }
#if __SOR_AOR__
                case Keys.End:
#endif
                case Keys.NumPad1:
                case Keys.D1: { processKeyStatus('1'); break; }
#if __SOR_AOR__
                case Keys.Down:
#endif
                case Keys.NumPad2:
                case Keys.D2: { processKeyStatus('2'); break; }
#if __SOR_AOR__
                case Keys.PageDown:
#endif
                case Keys.NumPad3:
                case Keys.D3: { processKeyStatus('3'); break; }
#if __SOR_AOR__
                case Keys.Left:
#endif
                case Keys.NumPad4:
                case Keys.D4: { processKeyStatus('4'); break; }
                case Keys.Clear:
                case Keys.NumPad5:
                case Keys.D5: { processKeyStatus('5'); break; }
#if __SOR_AOR__
                case Keys.Right:
#endif
                case Keys.NumPad6:
                case Keys.D6: { processKeyStatus('6'); break; }
#if __SOR_AOR__
                case Keys.Home:
#endif
                case Keys.NumPad7:
                case Keys.D7: { processKeyStatus('7'); break; }
#if __SOR_AOR__
                case Keys.Up:
#endif
                case Keys.NumPad8:
                case Keys.D8: { processKeyStatus('8'); break; }
#if __SOR_AOR__
                case Keys.PageUp:
#endif
                case Keys.NumPad9:
                case Keys.D9: { processKeyStatus('9'); break; }

                // ��Ѻ speed �繻���
#if __SOR_AOR__
                case Keys.S:
#else
				case Keys.Decimal:
#endif
				{
					SetRate( 1.0d );
					this.setStatusMsg("�������ǻ���");
					break;
				}
#if __SOR_AOR__
                case Keys.Subtract:
#endif
				case Keys.Back:
				{ 
					if (m_strKey.Length > 1)
					{
						m_strKey = m_strKey.Substring(0,m_strKey.Length-1);

                        setStatusMsg(m_strKey);
					}
					else
					{
						m_strKey = "";

                        hideTextStatus();
						//m_statusPixel.setVisible(false);
					}
					break;
				}

				case Keys.Enter:
				{
					//
					if (m_strKey == "") break;

                    hideTextStatus();
					//m_statusPixel.setVisible(false);
					
					m_strKey = "00000000000000000000" + m_strKey;
					m_strKey = m_strKey.Substring(m_strKey.Length - m_iSongSize,m_iSongSize);
					string strTmpStrKey = m_strKey; // ���������ŧ
					SongStructure ss = null; //string strTmpSongName = ""; // ���纪����ŧ���
					m_strKey = checkFileBeforePlay(m_strKey,ref ss); // �� path �������
					if (m_strKey == "__SONG_CODE_ERROR__")
					{
						// ������ŧ
						//MessageBox.Show( this, "�鹾�������ŧ�����ҧ���١��ͧ !!", "����͹..", MessageBoxButtons.OK, MessageBoxIcon.Stop );
						MessageForm("�鹾�������ŧ��\r\n���ҧ���١��ͧ !!");
					}
					else if (m_strKey == "")
					{
						// ������ŧ
						//MessageBox.Show( this, "��辺�ŧ���� " + strTmpStrKey, m_strProgramName + " ����͹..", MessageBoxButtons.OK, MessageBoxIcon.Stop );
						MessageForm("��辺�ŧ���� " + strTmpStrKey);
					}
					else
					{
						ss.m_strFilePlayer = m_strKey; // ������� m_strKey �� path ����ش�ҡ checkFileBeforePlay �Ҩ֧������������
						m_arrSongList.Add(ss);
						startSong();
					}
					m_strKey = "";
					break;
				}				
//				case Keys.F6:
//				{					
//					LeftUP lu = new LeftUP();
//					lu.Size = new Size(276, 124);
//					lu.Location = new Point(this.Location.X,this.Location.Y);					
//					lu.Show();
//					break;
//				}
//				case Keys.F7:
//				{					
//					RightUP ru = new RightUP();
//					ru.Size = new Size(276, 124);
//					ru.Location = new Point(this.Size.Width - ru.Size.Width,this.Location.Y);					
//					ru.Show();
//					break;
//				}
				case Keys.F1:
				{				
					m_help.ShowDialog();
					break;
				}
				case Keys.F2:
				{				
					m_songListControl.ShowDialog();
					break;
				}
//				case Keys.F4:
//				{
//					System.Windows.Forms.Cursor.Show();
//					m_mixForm.ShowDialog();
//					System.Windows.Forms.Cursor.Hide();
//					break;
//				}
				case Keys.F5:
				{
					System.Windows.Forms.Cursor.Show();
					m_copyUtil.ShowDialog();
					System.Windows.Forms.Cursor.Hide();
					break;
				}
				case Keys.F7:
				{
					System.Windows.Forms.Cursor.Show();
					m_statSong.ShowDialog();
					System.Windows.Forms.Cursor.Hide();
					break;
				}
				case Keys.F8:
				{
					System.Windows.Forms.Cursor.Show();
					m_convert2Book.ShowDialog();
					System.Windows.Forms.Cursor.Hide();
					break;
				}
				case Keys.F9:
				{
					System.Windows.Forms.Cursor.Show();
					m_editList.ShowDialog();
					System.Windows.Forms.Cursor.Hide();
					break;
				}
				case Keys.F6:
				{				
					m_RemoteSearch.ShowDialog();
					break;
				}
				case Keys.F3:
				{				
					m_searchSong.ShowDialog();
					break;
				}
				case Keys.F10:
				{				
					m_credit.ShowDialog();
					break;
				}
				case Keys.Space:
				{	
					menuControlPause_Click( null, null ); 
					break;
				}				
				case Keys.F11:
				{	
					ToggleFullScreen(); 
					break;
                }
#if __SOR_AOR__
#else
				case Keys.Add:
				{
					incVolume_Click(null,null);
					break;
				}
				case Keys.Subtract:
				{
					decVolume_Click(null,null);
					break;
				}
#endif
                case Keys.F12: // exit program
				{
					if (e.Control)
					{
						this.Close();
						//this.Dispose(true);
					}		
					break;
				}
#if __SOR_AOR__
                // ��������������������
                case Keys.Divide:
                {
                    // �觢�����仢�͹حҵ�
                    //m_client.sendMessage2Server("[CHKCREDIT]");
                    showTimesRemain();
                    break;
                }
#else
				case Keys.Divide:
				{	
					menuControlMute_Click( null, null ); 
					if (menuControlMute.Checked)
					{
						setStatusMsg("Mute");
					}
					else
					{
						setStatusMsg("UnMute");
					}
					break; 
				}
#endif
#if __SOR_AOR__
                case Keys.Add:
#else
				case Keys.Multiply:
#endif
				{
					//m_iSpeaker // 1 = both, 2 = right, 3 = left
					if (++m_iSpeaker > 3) m_iSpeaker = 1;
					if (m_iSpeaker == 1)
					{
						bothSpeakerClip();
					}
					else if (m_iSpeaker == 2)
					{
						rightSpeakerClip();
					}
					else
					{
						leftSpeakerClip();
					}					
					break;
				}
#if __SOR_AOR__
#else
				case Keys.Home:
				{
					stop();
					pausePlayClip();
					setTimeJump();
					setTimeEnd();
					break;
				}
#endif

#if __SOR_AOR__
                case Keys.Multiply:
#endif
				case Keys.Delete:
				{
					menuFileCloseClip_Click(null,null);
					afterComplete();
					break;
                }
#if __SOR_AOR__
#else

				case Keys.End:
				{
					stop();
					break;
				}
#endif

//				case Keys.PageDown:
//				{	
//					menuRateDecr_Click( null, null ); 
//					break;
//				}
//				case Keys.PageUp:
//				{	
//					menuRateIncr_Click( null, null ); 
//					break; 
//				}

//				case Keys.N:
//				{	menuRateNormal_Click( null, null ); break; }
//				case Keys.Q:
//				{
//					// Shutdown System
//					WindowsController.ExitWindows(RestartOptions.ShutDown, true);
//					break;
//				}			
			}
		}

        private bool checkAuthentication()
        {
            HTTPConnect http = new HTTPConnect();
            string strPostData = System.Environment.MachineName + ",moc_remain,--";
            string strText = http.webPageGET(m_strURLServerAUTH + "interface/auth_computer/" + strPostData);
            if (strText.IndexOf("FAIL") < 0)
            {
                return true;
            }
            return false;
        }

        private void playSongAuth(string strID)
        {
            HTTPConnect http = new HTTPConnect();
            string strPostData = System.Environment.MachineName + ",play_song," + strID;
            //string strText = http.getStream(m_strURLServerAUTH + "interface/auth_computer", strPostData);
            string strText = http.webPageGET(m_strURLServerAUTH + "interface/auth_computer/" + strPostData);
            strText = strText.Replace("FAIL", "Time Over");
            setStatusMsg(strText);
        }

        private void showTimesRemain()
        {
            HTTPConnect http = new HTTPConnect();
            string strPostData = System.Environment.MachineName + ",moc_remain,--";
            string strText = http.webPageGET(m_strURLServerAUTH + "interface/auth_computer/" + strPostData);
            strText = strText.Replace("FAIL", "Time Over");
            setStatusMsg(strText);
        }

		// openclip Ẻ���� ��������� �ɳ�
		private void openAdsClip(string strFilename)
		{			
			// ��觻Դ�ŧ��ҷ����������͹
			menuFileCloseClip_Click( null, null );

			// ��˹��ŧ����ͧ������
			clipFile = strFilename; // ����ŧ����ͧ������
			if( ! PlayAdsClip() )
			{				
				m_bNowPlaying = false;
				menuFileCloseClip_Click( null, null );				
			}			
		}

		// playAdsClip copy �Ҩҡ playClip ���ǵѴ�ҧ���ҧ�͡
		private bool PlayAdsClip()
		{
			try 
			{
				CloseInterfaces();
				//com.DTREnable = false;

				if( ! GetInterfaces() )
					return false;
			
				CheckClipType();
				if( clipType == ClipType.None )
					return false;

				int hr = mediaEvt.SetNotifyWindow( this.Handle, WM_GRAPHNOTIFY, IntPtr.Zero );
		
				if( (clipType == ClipType.AudioVideo) || (clipType == ClipType.VideoOnly) )
				{
					videoWin.put_Owner( this.Handle );
					videoWin.put_WindowStyle( WS_CHILD | WS_CLIPSIBLINGS | WS_CLIPCHILDREN );

					InitVideoWindow( 1, 1 );
					CheckSizeMenu( menuControlNormal );
					GetFrameStepInterface();
				}
				else
					InitPlayerWindow();

				hr = mediaCtrl.Run();
				if( hr >= 0 )
				{
					playState = PlayState.Running;
					
					// reconfig here !!
					// ���§���¢��
					bothSpeakerClip();
					
					// ��˹������ѧ�ŧ
					basicAudio.put_Volume( savedVolume );

					// �Ҿ�����
					//fullScreen = false;
					//ToggleFullScreen();
				}
				return hr >= 0;
			}
			catch( Exception ee )
			{
				//MessageBox.Show( this, "Could not start clip\r\n" + ee.Message, m_strProgramName, MessageBoxButtons.OK, MessageBoxIcon.Stop );
				return false;
			}
		}

		private void timer2_Tick(object sender, System.EventArgs e)
		{
			timer2.Stop();
			if (m_strAdsPath == "" || m_strAdsPath == null || m_bNowPlaying || m_bAdsPlay)
			{
				return;
			}			
			
			// copy �Ҩҡ�����
			bool cmdExit = false;
			while (!cmdExit && m_iAds < 51) // �������� 0 - 51
			{
				// m_strAdsPath.endWith '\' always
				string strFilename = m_strAdsPath + "CustomAds" + m_iAds.ToString("00");			
				if (File.Exists(strFilename + ".mpg"))
				{
					openAdsClip(strFilename + ".mpg");
					cmdExit = true; // exit loop
					m_bAdsPlay = true;
				}
				else if (File.Exists(strFilename + ".avi"))
				{
					openAdsClip(strFilename + ".avi");
					cmdExit = true; // exit loop
					m_bAdsPlay = true;
				}
				else if (File.Exists(strFilename + ".wmv"))
				{
					openAdsClip(strFilename + ".wmv");
					cmdExit = true; // exit loop
					m_bAdsPlay = true;
				}
				m_iAds++; // �ǡ����ɳ�
				//iCount++;
			}

			// �ǡ��������
			if (m_iAds > 50)
			{
				m_iAds = 0; // ���������
				timer2.Start();
			}
		}

		public void addMp3(SongStructure ss)
		{
			m_mixForm.addSong(ss);
		}

        // �ʴ��������ŧ
        private void UpdateSongTime_Tick(object sender, EventArgs e)
        {
            showAudioTime();
        }

//
//		private void modeChange()
//		{
//			if (++m_iSpeaker > 3) m_iSpeaker = 1;
//			if (m_iSpeaker == 1)
//			{
//				bothSpeakerClip();
//				setStatusMsg("Stereo");
//			}
//			else if (m_iSpeaker == 2)
//			{
//				rightSpeakerClip();
//				setStatusMsg("Mono /R");
//			}
//			else
//			{
//				leftSpeakerClip();
//				setStatusMsg("Mono /L");
//			}
//		}
//
//		private void backKey()
//		{
//			if (m_strKey.Length > 1)
//			{
//				m_strKey = m_strKey.Substring(0,m_strKey.Length-1);
//				m_status.setTxtStatus(m_strKey);
//				if (!m_bStatusShowDialog)
//				{
//					m_status.ShowDialog();
//				}
//			}
//			else
//			{
//				m_strKey = "";
//				m_status.Close();
//			}
//		}

//		private void onDataReceived(object sender, AxInfrared.__IRConnect_DataReceivedEvent e)
//		{
//			string []strData = e.theData.Split(' ');
//			strData[3] = strData[3].Replace("\r","").Replace("\n","");
//			if (strData[1] == "00" && strData[3] == "NuengRemote")
//			{
//				switch (strData[2])
//				{					
//                    case "power": { WindowsController.ExitWindows(RestartOptions.ShutDown,true); break;}
//					case "next": { SendKeys.Send("{DEL}"); break;}
//					case "volup": { incVolume_Click(null,null); break;}
//					case "voldown": { decVolume_Click(null,null); break;}
//					case "numpad0": { SendKeys.Send("0"); break;}
//					case "numpad1": { SendKeys.Send("1"); break;}
//					case "numpad2": { SendKeys.Send("2"); break;}
//					case "numpad3": { SendKeys.Send("3"); break;}
//					case "numpad4": { SendKeys.Send("4"); break;}
//					case "numpad5": { SendKeys.Send("5"); break;}
//					case "numpad6": { SendKeys.Send("6"); break;}
//					case "numpad7": { SendKeys.Send("7"); break;}
//					case "numpad8": { SendKeys.Send("8"); break;}
//					case "numpad9": { SendKeys.Send("9"); break;}
//
//					case "speedup": { SendKeys.Send("{PGUP}"); break;}
//					case "speeddown": { SendKeys.Send("{PGDN}"); break;}
//					case "stop": { SendKeys.Send("{END}"); break;}
//
//					case "mode": { SendKeys.Send("{MULTIPLY}"); break;}
//					case "back": { SendKeys.Send("{BACKSPACE}"); break;}
//					case "home": { SendKeys.Send("{HOME}"); break;}									 
//					case "mute": { SendKeys.Send("{DIVIDE}"); break;}
//										
//					case "queue": { SendKeys.Send("{F2}"); break;}
//					case "search": { SendKeys.Send("{F5}"); break;}
//					
//					case "up": { SendKeys.Send("{UP}"); break;}
//					case "down": { SendKeys.Send("{DOWN}"); break;}
//					case "left": { decVolume_Click(null,null); break;}
//					case "right": { incVolume_Click(null,null); break;}
//					case "enter": { SendKeys.Send("{ENTER}"); break;}
//				}
//			}
//		}
	}

	internal enum PlayState
	{
		Init, Stopped, Paused, Running
	}

	internal enum ClipType
	{
		None, AudioVideo, VideoOnly, AudioOnly
	}

} // namespace PlayWndNET
