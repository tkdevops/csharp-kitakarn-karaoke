using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Threading;

namespace MyMP3_X1
{
	/// <summary>
	/// Summary description for SearchSong.
	/// </summary>
	public class SearchSong : System.Windows.Forms.Form
	{
		System.Collections.IDictionaryEnumerator m_idic = null;
		ArrayList m_ArrSongIndex = new ArrayList();
		//string m_strLastTxtInList = ""; // �纤������ش�ͧ txt

        delegate void AddListViewCallback(string[] strTextData, object ss, double dcurrent);

		Thread m_threadList;
		double m_dcurrent = 0.0d;

		private int m_iListViewSelected = -1;
		private System.Windows.Forms.ListView listView1;
		private System.Windows.Forms.ColumnHeader columnHeader2;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.Timer timer1;
		private System.Windows.Forms.ProgressBar progressBar1;
		private System.Windows.Forms.TextBox textBox2;
		private System.Windows.Forms.TextBox textBox3;
		private System.ComponentModel.IContainer components;

		public SearchSong()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			this.TransparencyKey = System.Drawing.Color.FromArgb(((System.Byte)(76)), ((System.Byte)(75)), ((System.Byte)(54)));

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.listView1 = new System.Windows.Forms.ListView();
			this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.timer1 = new System.Windows.Forms.Timer(this.components);
			this.progressBar1 = new System.Windows.Forms.ProgressBar();
			this.textBox2 = new System.Windows.Forms.TextBox();
			this.textBox3 = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// listView1
			// 
			this.listView1.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(76)), ((System.Byte)(75)), ((System.Byte)(54)));
			this.listView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
																						this.columnHeader2});
			this.listView1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(222)));
			this.listView1.ForeColor = System.Drawing.Color.Aqua;
			this.listView1.FullRowSelect = true;
			this.listView1.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
			this.listView1.HideSelection = false;
			this.listView1.Location = new System.Drawing.Point(48, 48);
			this.listView1.MultiSelect = false;
			this.listView1.Name = "listView1";
			this.listView1.Size = new System.Drawing.Size(716, 228);
			this.listView1.TabIndex = 10;
			this.listView1.TabStop = false;
			this.listView1.View = System.Windows.Forms.View.Details;
			this.listView1.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
			// 
			// columnHeader2
			// 
			this.columnHeader2.Text = "�ŧ";
			this.columnHeader2.Width = 650;
			// 
			// textBox1
			// 
			this.textBox1.BackColor = System.Drawing.Color.DarkBlue;
			this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.textBox1.ForeColor = System.Drawing.Color.LightCyan;
			this.textBox1.Location = new System.Drawing.Point(128, 8);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(396, 29);
			this.textBox1.TabIndex = 5;
			this.textBox1.Text = "";
			this.textBox1.TextChanged += new System.EventHandler(this.onTextChanged);
			// 
			// timer1
			// 
			this.timer1.Interval = 5;
			this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
			// 
			// progressBar1
			// 
			this.progressBar1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.progressBar1.Location = new System.Drawing.Point(0, 288);
			this.progressBar1.Name = "progressBar1";
			this.progressBar1.Size = new System.Drawing.Size(800, 12);
			this.progressBar1.Step = 1;
			this.progressBar1.TabIndex = 42;
			this.progressBar1.Visible = false;
			// 
			// textBox2
			// 
			this.textBox2.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(76)), ((System.Byte)(75)), ((System.Byte)(54)));
			this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold);
			this.textBox2.ForeColor = System.Drawing.Color.LightCyan;
			this.textBox2.Location = new System.Drawing.Point(48, 8);
			this.textBox2.Name = "textBox2";
			this.textBox2.ReadOnly = true;
			this.textBox2.Size = new System.Drawing.Size(76, 31);
			this.textBox2.TabIndex = 44;
			this.textBox2.TabStop = false;
			this.textBox2.Text = "����";
			// 
			// textBox3
			// 
			this.textBox3.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(76)), ((System.Byte)(75)), ((System.Byte)(54)));
			this.textBox3.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.textBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold);
			this.textBox3.ForeColor = System.Drawing.Color.Orange;
			this.textBox3.Location = new System.Drawing.Point(536, 8);
			this.textBox3.Name = "textBox3";
			this.textBox3.ReadOnly = true;
			this.textBox3.Size = new System.Drawing.Size(192, 31);
			this.textBox3.TabIndex = 45;
			this.textBox3.TabStop = false;
			this.textBox3.Text = "(��س����ѡ����)";
			this.textBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.textBox3.Visible = false;
			// 
			// SearchSong
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(76)), ((System.Byte)(75)), ((System.Byte)(54)));
			this.ClientSize = new System.Drawing.Size(800, 300);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.textBox3,
																		  this.textBox2,
																		  this.progressBar1,
																		  this.textBox1,
																		  this.listView1});
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.KeyPreview = true;
			this.Name = "SearchSong";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.Text = "Search Song";
			this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.onKeyDown);
			this.Closing += new System.ComponentModel.CancelEventHandler(this.onClosing);
			this.Load += new System.EventHandler(this.SearchSong_Load);
			this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.onKeyUp);
			this.Activated += new System.EventHandler(this.onActivate);
			this.ResumeLayout(false);

		}
		#endregion

		private void onTextChanged(object sender, System.EventArgs e)
		{
			// ��������
			//			listView1.Items.Clear();			
			//			m_iListViewSelected = -1;
			//			m_ArrSongIndex.Clear();

			if (textBox1.Text.Trim() == "")
			{
				//((MainForm)(this.Owner)).setTxtStatus("");
				//m_strLastTxtInList = ""; // clear ������
				return;
			}

			// ��駤��
			//			m_idic = MainForm.m_hashSongIndex.GetEnumerator();
			//			m_idic.Reset();


			// ���������
			//timer1.Enabled = true;

			
			lock(listView1)
			{
				listView1.Items.Clear();
				Monitor.Pulse(listView1);
			}
			m_iListViewSelected = -1;
			m_ArrSongIndex.Clear();

			m_idic = MainForm.m_hashSongIndex.GetEnumerator();
			m_idic.Reset();

			try
			{
				if (m_threadList != null)
				{
					if (m_threadList.IsAlive)
					{
						m_threadList.Abort();
						m_threadList = null;
					}
				}

				m_dcurrent = new Random(999999).NextDouble(); // �觤�������Ǩ�ͺ� thread
				m_threadList = new Thread(new ThreadStart(addToList));
				m_threadList.Priority = ThreadPriority.BelowNormal;
				m_threadList.Start();

			}
			catch(Exception ex)
			{
				this.Close();
			}
		}
		
		private void addToList()
		{	

			System.Collections.IDictionaryEnumerator idic = m_idic;
			double dcurrent = m_dcurrent;
			// ��駤��
			try
			{				
				bool bMoveNext = false;
				while (bMoveNext = idic.MoveNext())
				{
					SongStructure ss = (SongStructure)m_idic.Value;
					string strSongName = ss.m_strSongName;
					string strNo = ss.m_strNo;
					string strArtistName = ss.m_strArtistName;
					string strAlbumName = ss.m_strAlbumName;

						
					if ((strSongName != null && strSongName.Trim().ToUpper().StartsWith(textBox1.Text.Trim().ToUpper()))
						|| (strNo != null && strNo.Trim().ToUpper().StartsWith(textBox1.Text.Trim().ToUpper()))
						|| (strArtistName != null && strArtistName.Trim().ToUpper().StartsWith(textBox1.Text.Trim().ToUpper()))
						|| (strAlbumName != null && strAlbumName.Trim().ToUpper().StartsWith(textBox1.Text.Trim().ToUpper()))
						)
					{
                        string[] strTmp = new string[] { ss.m_strNo + " : " + ss.m_strSongName + " : " + ss.m_strArtistName + " : " + ss.m_strAlbumName };
                        addToListView(strTmp, ss, dcurrent);
					}	
				}				
			}
			catch(System.NullReferenceException exnull)
			{
				//MessageBox.Show("����� (^^!)");
				//this.Close();
			}
		}

        // thread in .net 2
        private void addToListView(string[] strTextData, object ss, double dcurrent)
        {
            if (listView1.InvokeRequired)
            {
                AddListViewCallback d = new AddListViewCallback(addToListView);
                this.Invoke(d, new object[] { strTextData, ss, dcurrent });
            }
            else
            {
                //Processing the data
                if (listView1 != null && m_dcurrent == dcurrent)
                {
                    lock (listView1)
                    {
                        listView1.Items.Add(new ListViewItem(strTextData));
                        Monitor.Pulse(listView1);
                    }
                    m_ArrSongIndex.Add(ss);
                }
                else
                {
                    //break;
                }
            }
        }

		private void timer1_Tick(object sender, System.EventArgs e)
		{
			timer1.Stop();
			bool bMoveNext = false;
			while (bMoveNext = m_idic.MoveNext())
			{
				SongStructure ss = (SongStructure)m_idic.Value;
				string strSongName = ss.m_strSongName;
				string strNo = ss.m_strNo;
				string strArtistName = ss.m_strArtistName;
				string strAlbumName = ss.m_strAlbumName;

				if ((strSongName != null && strSongName.Trim().ToUpper().StartsWith(textBox1.Text.Trim().ToUpper()))
					|| (strNo != null && strNo.Trim().ToUpper().StartsWith(textBox1.Text.Trim().ToUpper()))
					|| (strArtistName != null && strArtistName.Trim().ToUpper().StartsWith(textBox1.Text.Trim().ToUpper()))
					|| (strAlbumName != null && strAlbumName.Trim().ToUpper().StartsWith(textBox1.Text.Trim().ToUpper()))
					)
				{
					listView1.Items.Add(new ListViewItem(new string[]{ss.m_strNo + " : " + ss.m_strSongName + " : " + ss.m_strArtistName + " : " + ss.m_strAlbumName}));
					m_ArrSongIndex.Add(ss);
				
					//					string strTmp = listView1.Items[0].SubItems[0].Text;
					//					if (m_strLastTxtInList != strTmp)
					//					{
					//						//((MainForm)(this.Owner)).setTxtStatus(listView1.Items[0].SubItems[0].Text);
					//						m_strLastTxtInList = strTmp;
					//					}
				}
				
			}

			if (!bMoveNext)
			{
				timer1.Enabled = false;
				//				if (listView1.Items.Count <= 0)
				//				{
				//					//((MainForm)(this.Owner)).setTxtStatus("");
				//					m_strLastTxtInList = ""; // clear ������
				//				}
				return;
			}

			timer1.Start();
		}

		private void moveListViewDown()
		{
			if (listView1.Items.Count <= 0)
			{
				return;
			}
			else if (listView1.Items.Count-1 > m_iListViewSelected)
			{
				listView1.Items[++m_iListViewSelected].Selected = true;
				Thread.Sleep(100);
			}
		}

		private void moveListViewUp()
		{
			if (listView1.Items.Count <= 0)
			{
				return;
			}
			else if (m_iListViewSelected > 0)
			{
				listView1.Items[--m_iListViewSelected].Selected = true;
				Thread.Sleep(100);
			}
		}

		public void onKeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			switch (e.KeyData)
			{
				case Keys.Divide:
				case Keys.Multiply:
				case Keys.Add:
				case Keys.Subtract:
				{
					//textBox3.Visible = true;
					((MainForm)(this.Owner)).onKeyUp(null,e);
					break;
				}
				case Keys.PageDown:
				case Keys.PageUp:
				{
					break;
				}
				case Keys.Up:
				{
					if (listView1.Items.Count <= 0)
					{
						return;
					}
					listView1.Focus();
					moveListViewUp();
					break;
				}
				case Keys.Down:
				{
					if (listView1.Items.Count <= 0)
					{
						return;
					}
					listView1.Focus();
					moveListViewDown();
					break;
				}
				case Keys.Insert:
				{
					// ��� insert � textbox ������ͧ������
					if (textBox1.Focused)
					{
						return;
					}

					if (m_iListViewSelected > -1)
					{
						SongStructure ss = (SongStructure)m_ArrSongIndex[m_iListViewSelected];
						string strTmp = MainForm.checkFileBeforePlay(ss.m_strNo,ref ss); // ��� 0 �ú 6 ���
						if (strTmp == "__SONG_CODE_ERROR__")
						{
							// ������ŧ
							//MessageBox.Show( this, "�鹾�������ŧ�����ҧ���١��ͧ !!", "����͹..", MessageBoxButtons.OK, MessageBoxIcon.Stop );
							((MainForm)(this.Owner)).MessageForm("�鹾�������ŧ��\r\n���ҧ���١��ͧ !!");
						}
						else if (strTmp != "")
						{
							ss.m_strFilePlayer = strTmp; // ������� strTmp �� path ����ش�ҡ checkFileBeforePlay �Ҩ֧������������

							// �ٻẺ�����蹷ѹ��
							// ������ŧ
							if (MainForm.m_arrSongList.Count > 0)
							{
								// �á��ӴѺ��� 1 ����¡��ԡ�ŧ�Ѩ�غѹ
								MainForm.m_arrSongList.Insert(1,ss);
								((MainForm)(this.Owner)).onKeyUp(null,new System.Windows.Forms.KeyEventArgs(Keys.Delete));
							}
							else
							{
								MainForm.m_arrSongList.Add(ss);
								((MainForm)(this.Owner)).onKeyUp(null,new System.Windows.Forms.KeyEventArgs(Keys.Delete));
							}
						}
						else
						{
							// ������ŧ
							//MessageBox.Show("���������ŧ ��سҵ�Ǩ�ͺ�ա����","����͹",MessageBoxButtons.OK,MessageBoxIcon.Information);
							((MainForm)(this.Owner)).MessageForm("���������ŧ\r\n��سҵ�Ǩ�ͺ !!");
						}
					}
					break;
				}
				case Keys.Enter:
				{					
					// ��� enter � textbox ������ͧ������
					if (textBox1.Focused)
					{
						return;
					}

					if (m_iListViewSelected > -1)
					{
						SongStructure ss = (SongStructure)m_ArrSongIndex[m_iListViewSelected];
						string strTmp = MainForm.checkFileBeforePlay(ss.m_strNo,ref ss); // ��� 0 �ú 6 ���
						if (strTmp == "__SONG_CODE_ERROR__")
						{
							// ������ŧ
							//MessageBox.Show( this, "�鹾�������ŧ�����ҧ���١��ͧ !!", "����͹..", MessageBoxButtons.OK, MessageBoxIcon.Stop );
							((MainForm)(this.Owner)).MessageForm("�鹾�������ŧ��\r\n���ҧ���١��ͧ !!");
						}
						else if (strTmp != "")
						{
							progressBar1.Value = 0;
							progressBar1.Maximum = 25;
							progressBar1.Visible = true;
							for (int x = 0; x < progressBar1.Maximum; x++)
							{
								progressBar1.Increment(1);
								progressBar1.Refresh();
								System.Threading.Thread.Sleep(5);
							}
							progressBar1.Visible = false;
	
							ss.m_strFilePlayer = strTmp; // ������� strTmp �� path ����ش�ҡ checkFileBeforePlay �Ҩ֧������������
							
							// ����				
							MainForm.m_arrSongList.Add(ss);
							((MainForm)(this.Owner)).startSong();
							
						}
						else
						{
							// ������ŧ
							//MessageBox.Show("���������ŧ ��سҵ�Ǩ�ͺ�ա����","����͹",MessageBoxButtons.OK,MessageBoxIcon.Information);
							((MainForm)(this.Owner)).MessageForm("���������ŧ\r\n��سҵ�Ǩ�ͺ !!");
						}
					}
					break;
				}				
				case Keys.Escape:
				case Keys.F1:
				case Keys.F2:
				//case Keys.F3:// ��� key ����͡
				case Keys.F5:
				case Keys.F6: 
				case Keys.F7:
				case Keys.F8:
				case Keys.F9:
				case Keys.F10:
				{
					this.Close();
					break;
				}
				default:
				{					
					if (e.Control && e.Shift)
					{
						if (m_iListViewSelected > -1)
						{
							SongStructure ss = (SongStructure)m_ArrSongIndex[m_iListViewSelected];
							string strTmp = MainForm.checkFileBeforePlay(ss.m_strNo,ref ss); // ��� 0 �ú 6 ���
							if (strTmp == "__SONG_CODE_ERROR__")
							{
								// ������ŧ
								//MessageBox.Show( this, "�鹾�������ŧ�����ҧ���١��ͧ !!", "����͹..", MessageBoxButtons.OK, MessageBoxIcon.Stop );
								((MainForm)(this.Owner)).MessageForm("�鹾�������ŧ��\r\n���ҧ���١��ͧ !!");
							}
							else if (strTmp != "")
							{
								progressBar1.Value = 0;
								progressBar1.Maximum = 25;
								progressBar1.Visible = true;
								for (int x = 0; x < progressBar1.Maximum; x++)
								{
									progressBar1.Increment(1);
									progressBar1.Refresh();
									System.Threading.Thread.Sleep(5);
								}
								progressBar1.Visible = false;
	
								ss.m_strFilePlayer = strTmp; // ������� strTmp �� path ����ش�ҡ checkFileBeforePlay �Ҩ֧������������
								if ((ss.m_strFilePlayer.ToUpper().EndsWith(".MP3") || ss.m_strFilePlayer.ToUpper().EndsWith(".OGG")))
								{
									// �� Shift + enter Mp3 , ogg
									((MainForm)(this.Owner)).addMp3(ss);
								}
							}
							else
							{
								// ������ŧ
								//MessageBox.Show("���������ŧ ��سҵ�Ǩ�ͺ�ա����","����͹",MessageBoxButtons.OK,MessageBoxIcon.Information);
								((MainForm)(this.Owner)).MessageForm("���������ŧ\r\n��سҵ�Ǩ�ͺ !!");
							}
						}
					}
					else if (!e.Control)
					{
						textBox1.Focus();
					}
					break;
				}
			}
		}

		private void listView1_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// ��Ҥ��� listview ��纵�����˹觵�ҧ� �ͧ control
			m_iListViewSelected = -1;			

			if (listView1.SelectedIndices.Count > 0)
			{			
				m_iListViewSelected = listView1.SelectedIndices[0];
				//label1.Text = listView1.Items[m_iListViewSelected].SubItems[0].Text;
				//((MainForm)(this.Owner)).setTxtStatus(listView1.Items[m_iListViewSelected].SubItems[0].Text);
			}
			else
			{
				return;
			}
		}

		private void onKeyUp(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			switch(e.KeyData)
			{
				case Keys.F3:
				{
					this.Close();
					break;
				}
			}		
		}

		private void onClosing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (m_threadList != null)
			{
				if (m_threadList.IsAlive)
				{
					m_threadList.Abort();
				}
			}

			timer1.Stop();
			//((MainForm)(this.Owner)).setTxtVisible(false);
		}

		public void setTextBox(string str)
		{
			textBox1.Text = str;
		}

		private void SearchSong_Load(object sender, System.EventArgs e)
		{
			//textBox1.Text = "";
			textBox1.Select(textBox1.Text.Length,textBox1.Text.Length);
		}

		private void onActivate(object sender, System.EventArgs e)
		{
			textBox3.Visible = false;
		}

		//		private void onFocus(object sender, System.EventArgs e)
		//		{
		//			//show
		//			//((MainForm)(this.Owner)).setTxtChoose(true);
		//		}
		//
		//		private void onLostFocus(object sender, System.EventArgs e)
		//		{
		//			//hide
		//			//((MainForm)(this.Owner)).setTxtChoose(false);
		//			//((MainForm)(this.Owner)).setTxtStatus(listView1.Items[m_iListViewSelected].SubItems[0].Text);
		//		}

	}
}
