using System;

namespace MyMP3_X1
{
	/// <summary>
	/// Summary description for SongStructure.
	/// </summary>
	[Serializable]
	public class SongStructure
	{
		public string m_strNo = "";
		public string m_strSongName = "";
		public string m_strArtistName = "";
		public string m_strAlbumName = "";
		public string m_strFileName = "";
		public string m_strTimejump = "";
		public string m_strTimeEnd = "";
		public string m_strFilePlayer = "";
		public string m_strEct1 = "";
		public string m_strEct2 = "";
		public string m_strEct3 = "";
		public string m_strEct4 = "";
		public string m_strEct5 = "";

		public SongStructure()
		{
			
		}
	}
}
