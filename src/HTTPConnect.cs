using System;
using System.Collections.Generic;
using System.Net;
using System.IO;
using System.Text;

namespace MyMP3_X1
{
    class HTTPConnect
    {
        public HTTPConnect()
        {
        }

        public string getStream(string strURL,string strPostData)
        {
            //string param1 = System.Environment.MachineName;

            //string postData = string.Format("computerName={0}", param1);

            ASCIIEncoding encoding = new ASCIIEncoding();            

            byte[] buffer = encoding.GetBytes(strPostData);

            // Prepare web request...
            HttpWebRequest myRequest = (HttpWebRequest)WebRequest.Create(strURL);

            // We use POST ( we can also use GET )
            myRequest.Method = "POST";

            // Set the content type to a FORM
            myRequest.ContentType = "application/x-www-form-urlencoded";

            // Get length of content
            myRequest.ContentLength = buffer.Length;

            // Get request stream
            Stream newStream = myRequest.GetRequestStream();

            // Send the data.
            newStream.Write(buffer, 0, buffer.Length);

            // Close stream
            newStream.Close();



            // Assign the response object of 'HttpWebRequest' to a 'HttpWebResponse' variable.
            HttpWebResponse myHttpWebResponse = (HttpWebResponse)myRequest.GetResponse();

            // Display the contents of the page to the console.
            Stream streamResponse = myHttpWebResponse.GetResponseStream();

            // Get stream object
            StreamReader streamRead = new StreamReader(streamResponse);

            Char[] readBuffer = new Char[256];

            // Read from buffer
            int count = streamRead.Read(readBuffer, 0, 256);

            String resultData = new String(readBuffer, 0, count);
            // ���� ���� 256 byte �������
            //while (count > 0) 
            //{
            //    // get string
            //    String resultData = new String( readBuffer, 0, count);

            //    // Write the data 
            //    Console.WriteLine( resultData );

            //    // Read from buffer
            //    count = streamRead.Read( readBuffer, 0, 256);
            //}

            // Release the response object resources.
            streamRead.Close();
            streamResponse.Close();

            // Close response
            myHttpWebResponse.Close();

            return resultData;
        }

        public string webPageGET(String uri)
        {
            const int bufSizeMax = 65536; // max read buffer size conserves memory
            const int bufSizeMin = 8192;  // min size prevents numerous small reads
            StringBuilder sb;

            // A WebException is thrown if HTTP request fails
            try
            {

                // Create an HttpWebRequest using WebRequest.Create (see .NET docs)!
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);

                // Execute the request and obtain the response stream
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream responseStream = response.GetResponseStream();

                // Content-Length header is not trustable, but makes a good hint.
                // Responses longer than int size will throw an exception here!
                int length = (int)response.ContentLength;

                // Use Content-Length if between bufSizeMax and bufSizeMin
                int bufSize = bufSizeMin;
                if (length > bufSize)
                    bufSize = length > bufSizeMax ? bufSizeMax : length;

                // Allocate buffer and StringBuilder for reading response
                byte[] buf = new byte[bufSize];
                sb = new StringBuilder(bufSize);

                // Read response stream until end
                while ((length = responseStream.Read(buf, 0, buf.Length)) != 0)
                    sb.Append(Encoding.UTF8.GetString(buf, 0, length));

            }
            catch (Exception ex)
            {
                sb = new StringBuilder(ex.Message);
            }
            return sb.ToString();
        }
    }
}
