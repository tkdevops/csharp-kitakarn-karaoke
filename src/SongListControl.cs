using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.IO;
using System.Management;

namespace MyMP3_X1
{
	/// <summary>
	/// Summary description for CopyUtil.
	/// </summary>
	public class SongListControl : System.Windows.Forms.Form
	{
		bool m_bUpper = false; // �� true ��� select �ѹ�á�ش
		bool m_bLower = false;
		private System.Windows.Forms.ListView listView1;
		private System.Windows.Forms.ColumnHeader columnHeader2;
		private System.Windows.Forms.ProgressBar progressBar1;
		private System.Windows.Forms.TextBox textBox1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		//public SongListControl(string strVersion,string strSongFormat)
		public SongListControl()
		{
			InitializeComponent();
			this.TransparencyKey = System.Drawing.Color.FromArgb(((System.Byte)(76)), ((System.Byte)(75)), ((System.Byte)(54)));
		}



		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.listView1 = new System.Windows.Forms.ListView();
			this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
			this.progressBar1 = new System.Windows.Forms.ProgressBar();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// listView1
			// 
			this.listView1.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(76)), ((System.Byte)(75)), ((System.Byte)(54)));
			this.listView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
																						this.columnHeader2});
			this.listView1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(222)));
			this.listView1.ForeColor = System.Drawing.Color.Aqua;
			this.listView1.FullRowSelect = true;
			this.listView1.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
			this.listView1.Location = new System.Drawing.Point(48, 48);
			this.listView1.MultiSelect = false;
			this.listView1.Name = "listView1";
			this.listView1.Size = new System.Drawing.Size(716, 228);
			this.listView1.TabIndex = 41;
			this.listView1.View = System.Windows.Forms.View.Details;
			this.listView1.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
			// 
			// columnHeader2
			// 
			this.columnHeader2.Text = "�ŧ";
			this.columnHeader2.Width = 650;
			// 
			// progressBar1
			// 
			this.progressBar1.Location = new System.Drawing.Point(64, 282);
			this.progressBar1.Name = "progressBar1";
			this.progressBar1.Size = new System.Drawing.Size(676, 12);
			this.progressBar1.Step = 1;
			this.progressBar1.TabIndex = 42;
			this.progressBar1.Visible = false;
			// 
			// textBox1
			// 
			this.textBox1.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(76)), ((System.Byte)(75)), ((System.Byte)(54)));
			this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold);
			this.textBox1.ForeColor = System.Drawing.Color.LightCyan;
			this.textBox1.Location = new System.Drawing.Point(48, 8);
			this.textBox1.Name = "textBox1";
			this.textBox1.ReadOnly = true;
			this.textBox1.Size = new System.Drawing.Size(144, 31);
			this.textBox1.TabIndex = 43;
			this.textBox1.TabStop = false;
			this.textBox1.Text = "��¡���ŧ";
			// 
			// SongListControl
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(76)), ((System.Byte)(75)), ((System.Byte)(54)));
			this.CausesValidation = false;
			this.ClientSize = new System.Drawing.Size(800, 300);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.textBox1,
																		  this.progressBar1,
																		  this.listView1});
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.KeyPreview = true;
			this.Name = "SongListControl";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.Text = "List Control";
			this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.onKeyDown);
			this.Load += new System.EventHandler(this.SongListControl_Load);
			this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.onKeyUp);
			this.ResumeLayout(false);

		}
		#endregion

		
		public void refreshList()
		{
			try
			{
				System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;
				// progress bar
				progressBar1.Value = 0;
				progressBar1.Maximum = MainForm.m_arrSongList.Count;
				progressBar1.Visible = true;

				// �����ء Label ����ҧ
				listView1.Items.Clear();					

				// ����
				for (int x = 0; x < MainForm.m_arrSongList.Count; x++)
				{
					SongStructure ss = (SongStructure)MainForm.m_arrSongList[x];
					listView1.Items.Add(new ListViewItem(new string[]{ss.m_strNo + " : " + ss.m_strSongName + " : " + ss.m_strArtistName + " : " + ss.m_strAlbumName}));

					// refresh progress bar
					progressBar1.Increment(1);
					progressBar1.Refresh();
				}
			}
			catch(Exception ex)
			{
			}
			finally
			{
				progressBar1.Visible = false;
				System.Windows.Forms.Cursor.Current = Cursors.Default;
			}
		}

		private void SongListControl_Load(object sender, System.EventArgs e)
		{
			refreshList();
		}
			
		private void onKeyUp(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			switch (e.KeyData)
			{
				
				case Keys.Delete:
				{
//					if (m_iListIndex == 0 && m_iLabelIndex == 0 && playing)
//					{
//						//close that's song
//					}
					if (listView1.Items.Count < 1)
					{
						break;
					}

					if (listView1.SelectedIndices.Count > 0)
					{
						MainForm.m_arrSongList.RemoveAt(listView1.SelectedIndices[0]);
						listView1.Items.RemoveAt(listView1.SelectedIndices[0]);
					}
					else
					{
						MessageBox.Show("��س����͡�ŧ����ͧ���ź !!","�Դ��Ҵ !!",MessageBoxButtons.OK,MessageBoxIcon.Information);
					}
                    break;
				}
				case Keys.Insert:
				{
					// ������ŧ� list ��������кص��˹� insert �������͹
					if (listView1.Items.Count > 0 && listView1.SelectedIndices.Count == 0)
					{
						MessageBox.Show("��س����͡���˹��ŧ����ͧ����á !!","�Դ��Ҵ !!",MessageBoxButtons.OK,MessageBoxIcon.Information);
						break;
					}

					// �����ŧ
					AskForm askForm = new AskForm("�����ŧ...",9999999999);
					askForm.ShowDialog();
					if (askForm.getOkButton())
					{
						SongStructure ss = null; //string strTmpSongName = ""; // ���纪����ŧ���
						string strTmp = MainForm.checkFileBeforePlay(askForm.getValue().ToString("000000"),ref ss); // ��� 0 �ú 6 ���
						if (strTmp == "__SONG_CODE_ERROR__")
						{
							// ������ŧ
							MessageBox.Show( this, "�鹾�������ŧ�����ҧ���١��ͧ !!", "����͹..", MessageBoxButtons.OK, MessageBoxIcon.Stop );
						}
						else if (strTmp != "")
						{
							// ������ŧ
							// �á�ç������
							if (listView1.SelectedIndices.Count > 0)
							{
								ss.m_strFilePlayer = strTmp; // ������� strTmp �� path ����ش�ҡ checkFileBeforePlay �Ҩ֧������������
								MainForm.m_arrSongList.Insert(listView1.SelectedIndices[0],ss);
								listView1.Items.Insert(listView1.SelectedIndices[0],new ListViewItem(new string[]{ss.m_strNo + " : " + ss.m_strSongName + " : " + ss.m_strArtistName + " : " + ss.m_strAlbumName}));
							}
							// �á�ѹ�á���
							else
							{
								// insert at 0
								ss.m_strFilePlayer = strTmp; // ������� strTmp �� path ����ش�ҡ checkFileBeforePlay �Ҩ֧������������
								MainForm.m_arrSongList.Add(ss);
								listView1.Items.Add(new ListViewItem(new string[]{ss.m_strSongName + " : " + ss.m_strArtistName + " : " + ss.m_strAlbumName}));
							}
						}
						else
						{
							MessageBox.Show("��辺�ŧ " + askForm.getValue().ToString("000000") + " ����˹�","�Դ��Ҵ !!",MessageBoxButtons.OK,MessageBoxIcon.Information);
						}
					}
					break;
				}

				case Keys.Divide:
				case Keys.Multiply:
				case Keys.Add:
				case Keys.Subtract:
				{
					((MainForm)(this.Owner)).onKeyUp(null,e);
					break;
				}
				case Keys.Up | Keys.Shift:
				{
					if (listView1.Items.Count < 2)
					{
						break;
					}
					else if (!m_bUpper && listView1.SelectedIndices.Count > 0) // ������������ش�٧�ش
					{
						try
						{
							// ����Ѻ���ç���
							string strTmp = listView1.Items[listView1.SelectedIndices[0]].SubItems[0].Text;
							listView1.Items[listView1.SelectedIndices[0]].SubItems[0].Text = listView1.Items[listView1.SelectedIndices[0] + 1].SubItems[0].Text;
							listView1.Items[listView1.SelectedIndices[0] + 1].SubItems[0].Text =  strTmp;

							SongStructure songStructure = (SongStructure)MainForm.m_arrSongList[listView1.SelectedIndices[0]];
							MainForm.m_arrSongList[listView1.SelectedIndices[0]] = (SongStructure)MainForm.m_arrSongList[listView1.SelectedIndices[0] + 1];
							MainForm.m_arrSongList[listView1.SelectedIndices[0] + 1] = songStructure;

							if (listView1.SelectedIndices[0] == 0)
							{
								m_bUpper = true;
							}
						}
						catch(Exception ex)
						{
						}
					}
					break;
				}
//				case Keys.Up:
//				{
//					moveUp();
//					break;
//				}
				case Keys.Down | Keys.Shift:
				{
					if (listView1.Items.Count < 2)
					{
						break;
					}
					else if (!m_bLower) // ������������ش����ش
					{
						try
						{
							string strTmp = listView1.Items[listView1.SelectedIndices[0]].SubItems[0].Text;
							listView1.Items[listView1.SelectedIndices[0]].SubItems[0].Text = listView1.Items[listView1.SelectedIndices[0] - 1].SubItems[0].Text;
							listView1.Items[listView1.SelectedIndices[0] - 1].SubItems[0].Text =  strTmp;

							SongStructure songStructure = (SongStructure)MainForm.m_arrSongList[listView1.SelectedIndices[0]];
							MainForm.m_arrSongList[listView1.SelectedIndices[0]] = (SongStructure)MainForm.m_arrSongList[listView1.SelectedIndices[0] - 1];
							MainForm.m_arrSongList[listView1.SelectedIndices[0] - 1] = songStructure;

							if (listView1.SelectedIndices[0] == listView1.Items.Count -1)
							{
								m_bLower = true;
							}
						}
						catch(Exception ex)
						{
						}
					}
					break;
				}
				case Keys.F2:
				{
					this.Close();
					break;
				}
			}		
		}

		private void onKeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			switch (e.KeyData)
			{
				// ���������ѭ�ҵ͹��� form ���� enter ����� (enter � dialog ���� form ���ӧҹ����)
				case Keys.Enter:
				{
					if (listView1.SelectedIndices.Count > 0)
					{
						((MainForm)(this.Owner)).m_bPlayByInsertSong = true; // �������繡������¡���á
						SongStructure ss = (SongStructure)MainForm.m_arrSongList[listView1.SelectedIndices[0]];					
						((MainForm)(this.Owner)).setSongTxt(ss.m_strSongName + " - " + ss.m_strArtistName);
						((MainForm)(this.Owner)).openClip(ss.m_strFilePlayer,ss);						
					}
					break;
				}
				case Keys.Escape:
				case Keys.F1:
				//case Keys.F2:// ��� key ����͡
				case Keys.F3:
				case Keys.F5:
				case Keys.F6: 
				case Keys.F7:
				case Keys.F8:
				case Keys.F9:
				case Keys.F10:
				{
					this.Close();
					break;
				}				
			}
		}

		private void listView1_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (listView1.SelectedIndices.Count > 0)
			{			
				// ��Ǩ��������躹�ش�������
				if (listView1.SelectedIndices[0] != 0)
				{
					m_bUpper = false;
				}

				//��Ǩ�����������ҧ�ش�������
				if (listView1.SelectedIndices[0] != listView1.Items.Count -1)
				{
					m_bLower = false;
				}
			}
		}
	}
}