/// - �� bug ��ͧ�ѹ��� clone 
/// - ����¹���ʼ�Ե�ѳ���� N2

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace MyMP3_X1
{
	/// <summary>
	/// Summary description for register.
	/// </summary>
	public class register : System.Windows.Forms.Form
	{
		////////////////////////////////////////////////////////////////////////
		// config �ç���
		// ���� key ����� Register {F71CFFB4-D687-4197-91D6-04257E02E1EF}
		const string __REGKEY__ = "E1AFF476F3B3B4B63A7D38BD3DFF824183820487858844C98C4C4CCE8E0F8C52D38F9591D285";
		// ���ʻ������Թ��� 㹷������ Nueng Karaoke (N2) �ͧ���
		// ���ʻ������Թ��� 㹷������ KITAKARN KARAOKE (KK)
		const string __PRODUCT_TYPE__ = "KK"; 
		// ���� REgister �������������ʹ {1D62B9FB-9315-4D4A-AC0A-EE109F35F3D0}
		const string __REGKEY_KILLER__ = "E17430B5B7B477B5B77D7BFE7F7F82413E438187830449868C87888E4F8ECC1292CF15515785";
		////////////////////////////////////////////////////////////////////////

		private MyUtil myUtil;
		private bool m_bRegistered = false;

		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox textBox2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox textBox3;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public register()
		{
			InitializeComponent();

			myUtil = new MyUtil();

			MainForm.m_strMAC = myUtil.getMACAddress();
			if (MainForm.m_strMAC == "")
			{
				MessageBox.Show("��سҵԴ��駡������͢��� ��͹��ҹ�����","��辺���� LAN",MessageBoxButtons.OK,MessageBoxIcon.Error);
				return;
			}

			// ����¹���˹觡����Ŵ��� Registry �ͧ Nueng Karaoke �ç���
			myUtil.setDefaultRegKey(__REGKEY__);
			
			// �纤�� machine code �ҵ�Ǩ��Ҷ١�Ѻ����ͧ㹷�����������
			// "MCID" = 6CF06F31
			string strTmp = myUtil.getRegDecode(myUtil.decodeTxt("6CF06F31"));
			if (strTmp != "" && myUtil.checkCodeSum(strTmp)) // ������ʶ١���Ǩ�ͺ ������ʷ���� �ç�Ѻ��� register ��������
			{
				string strRegister = myUtil.genRegisterCodeFromMachineID(strTmp);
				string strRegister2 = myUtil.getRegDecode(myUtil.decodeTxt("AB67E8F2E82B6C"));
				string strRegister3 = myUtil.genRegisterCodeFromMachineID(this.makeMachineIDFromNetworkCard());
				if (strRegister == strRegister2)
				{
					// ������ʶ١���ҹ
					m_bRegistered = true;

					// ��� card lan ���١��ͧ���������
					if (strRegister3 != strRegister2)
					{
						// ��ʹẺ�����������
						//save register

//						// ������¡��ԡ ���ͷ�������ͧ�ѧ����Ҩ�ⴹ�ӫ��
//						// TSCD = TalaySoft Count Down = 2AECF131
//						string strValue = myUtil.getRegDecode(myUtil.decodeTxt("2AECF131"));
//						if (strValue == "" || strValue == null)
//						{
//							strValue = "18825";
//						}
//						else
//						{
//							strValue = Convert.ToString(Convert.ToInt32(strValue) - 753);
//						}

						// �ѹ�֡��ҡ�Ѻ ����ź��������� �����Դ���駵��仨������������
//						if (Convert.ToInt32(strValue) <= 0)
//						{
//							// clear ������ register
//							// MCID
//							myUtil.saveRegEncode(myUtil.decodeTxt("6CF06F31"),"F0F0F000F077F9B9B7BBFD0CFE7FBD7EBE80C000070008880BC70C0E0D000D9090909000");
//							// RegCode
//							myUtil.saveRegEncode(myUtil.decodeTxt("AB67E8F2E82B6C"),"F0F0F000F077F9B9B7BBFD0CFE7FBD7EBE80C000070608880BC70C0E0D000D9090909000");
//							// error code 619: please contact webmaster@talaysoft.com
//							MessageBox.Show(myUtil.decodeTxt("2325686AE76B3DEEECAD2C72F32F45F176B8767AF7377CBA473B813F837E0103C643D60A0889DADBDC"),myUtil.decodeTxt("6EA4A5E7A73CF5EB2E6F41BD7F7E45C3C4C5898A8B"));
//						}
//						myUtil.saveRegEncode(myUtil.decodeTxt("2AECF131"),strValue);			
//						myUtil.saveRegEncode(myUtil.decodeTxt("2AECF131"),"0");

						m_bRegistered = false; // ������������
						myUtil.deleteSubKeyTree(myUtil.decodeTxt(__REGKEY__));						
					}
				}
			}
			
			if (!m_bRegistered)
			{			
				this.Text = myUtil.decodeTxt(this.Text);
				label1.Text = myUtil.decodeTxt(label1.Text);
				label2.Text = myUtil.decodeTxt(label2.Text);
				label3.Text = myUtil.decodeTxt(label3.Text);
				this.ShowDialog();
			}
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.button2 = new System.Windows.Forms.Button();
			this.label2 = new System.Windows.Forms.Label();
			this.textBox2 = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.textBox3 = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(104, 12);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(408, 20);
			this.textBox1.TabIndex = 0;
			this.textBox1.Text = "";
			this.textBox1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.onEnter);
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 12);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(92, 20);
			this.label1.TabIndex = 1;
			this.label1.Text = "2BA4E62966EC283E7537";
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(436, 100);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(76, 24);
			this.button2.TabIndex = 3;
			this.button2.Text = "cancel";
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(8, 40);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(92, 20);
			this.label2.TabIndex = 4;
			this.label2.Text = "6C68E92869A96C3EF7ED3071";
			// 
			// textBox2
			// 
			this.textBox2.Location = new System.Drawing.Point(104, 40);
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new System.Drawing.Size(408, 20);
			this.textBox2.TabIndex = 5;
			this.textBox2.Text = "";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(8, 68);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(92, 20);
			this.label3.TabIndex = 7;
			this.label3.Text = "AB67E868E7276CAA3FF8EE3172";
			// 
			// textBox3
			// 
			this.textBox3.Location = new System.Drawing.Point(104, 68);
			this.textBox3.Name = "textBox3";
			this.textBox3.Size = new System.Drawing.Size(408, 20);
			this.textBox3.TabIndex = 6;
			this.textBox3.Text = "";
			this.textBox3.KeyUp += new System.Windows.Forms.KeyEventHandler(this.onRegister);
			// 
			// register
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(538, 142);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.label3,
																		  this.textBox3,
																		  this.textBox2,
																		  this.label2,
																		  this.button2,
																		  this.label1,
																		  this.textBox1});
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Name = "register";
			this.ShowInTaskbar = false;
			this.Text = "AB67E868E7276CAA";
			this.Load += new System.EventHandler(this.register_Load);
			this.ResumeLayout(false);

		}
		#endregion

		private void register_Load(object sender, System.EventArgs e)
		{
			
		}

		public bool checkRegistered()
		{
			return m_bRegistered;
		}

		private void onEnter(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			if (Keys.Enter == e.KeyCode)
			{
				if (!myUtil.checkProductID(textBox1.Text,__PRODUCT_TYPE__))
				{
					// ProductID is invalid !!!spacebar to try again
					MessageBox.Show(myUtil.decodeTxt("2BA4E62966EC287436406FEE4372B2B1773577394B8C8D8EFB3C810282C485C25743065A46C8875E8F0F9190D0"));
					return;
				}
				else
				{
					textBox2.Text = "";
					textBox2.Text = makeMachineIDFromNetworkCard();
				}
			}
		}

		private string makeMachineIDFromNetworkCard()
		{
			string strMACAddress = myUtil.getMACAddress();
			if (strMACAddress == "")
			{
				MessageBox.Show("��سҵԴ��駡������͢��� ��͹��ҹ�����","��辺���� LAN",MessageBoxButtons.OK,MessageBoxIcon.Error);
				return "";
			}
			byte[] bTmp = System.Text.ASCIIEncoding.ASCII.GetBytes(strMACAddress);
			string strReturn = myUtil.makeCodeSum(myUtil.bin2hex(myUtil.encode(bTmp)));	
			return strReturn;
		}

		private void button2_Click(object sender, System.EventArgs e)
		{
			// Please register first !!!
			MessageBox.Show(myUtil.decodeTxt("2B25686AE76B3DAA6EEF6FEE2E73B146B676B5F6364C8D8E8F"));
			this.Dispose(true);
		}

		private void onRegister(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Enter)
			{
//				//www.talaysoft.com
//				if (textBox3.Text == myUtil.decodeTxt("25232426B5F9FAE9EAEBBE3573317570FBF5B835C8FCFA7B"))
//				{
//					m_bRegistered = true;
//					this.Dispose(true);
//				}
//				else 

//				if (myUtil.checkPIDwithID(textBox3.Text))
				if (myUtil.checkCodeSum(textBox2.Text) &&
					(textBox3.Text == myUtil.genRegisterCodeFromMachineID(textBox2.Text)))
				{
					//save register
					// MCID
					myUtil.saveRegEncode(myUtil.decodeTxt("6CF06F31"),textBox2.Text);
					// RegCode
					myUtil.saveRegEncode(myUtil.decodeTxt("AB67E8F2E82B6C"),textBox3.Text);
					m_bRegistered = true;
					this.Dispose(true);
				}
				else
				{
					// register code is invalid !!!\r\nspacebar to try again
					MessageBox.Show(myUtil.decodeTxt("A367E868E7276CAA3FF0EE31724473F24776B6B57B397B3D4F90919244C046C30344890A8ACC8DCA5F4B0E624ED08F6697179998D8"));
				}
			}
		}
	}
}
