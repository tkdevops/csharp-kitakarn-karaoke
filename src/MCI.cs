using System;
using System.Text;
using System.Runtime.InteropServices;

namespace MyMP3_X1
{

	public delegate void PositionEventHandler(object sender, PositionChangedEventArgs e);

	/// <summary>
	/// Summary description for MCI.
	/// </summary>
	public class MCI
	{
		[DllImport("winmm.dll")]
		private static extern int mciSendString(string strCommand,StringBuilder strReturn,int iReturnLength, IntPtr hwndCallback);

		public event PositionEventHandler PositionChanged;
		private StringBuilder sBuffer = new StringBuilder(128);		
		private System.Windows.Forms.Timer timer1;		

		//public int m_iDefaultMCINO = 1; // �����Ţ default ����ͧ���
		int Nb = 0; // tmp default MCI NO

		public MCI(int idefaultMCI)
		{
			//
			// TODO: Add constructor logic here
			//
			Nb = idefaultMCI;
			timer1 = new System.Windows.Forms.Timer();
			timer1.Enabled = false;
			timer1.Interval = 100;
			timer1.Tick += new System.EventHandler(this.timer1_Tick);
		}

		private void timer1_Tick(object sender, System.EventArgs e)
		{
			PositionChangedEventArgs pArgs = new PositionChangedEventArgs(this.OGG_Position);

			if(PositionChanged != null)
				PositionChanged(this,pArgs);
		}


		//public int OGG_Load(int Nb,string file)
		public int OGG_Load(string file)
		{
			int i = mciSendString("open \""+ file +"\" alias OGG"+ Nb.ToString(),null ,0, IntPtr.Zero);
			if (i == 0)
			{
				return 1;
			}

			return 0;
		}

		//public int OGG_Play(int Nb)
		public int OGG_Play()
		{			
			int i = mciSendString("play OGG" + Nb.ToString() + " from 0" ,null ,0, IntPtr.Zero);
			timer1.Enabled = true;
			return i;
		}

		//public int OGG_Pause(int Nb)
		public int OGG_Pause()
		{			
			int i = mciSendString("pause OGG" + Nb.ToString(),null ,0, IntPtr.Zero);
			return i;
		}

		//public int OGG_Resume(int Nb) 
		public int OGG_Resume() 
		{			
			int i = mciSendString("resume OGG" + Nb.ToString(),null ,0, IntPtr.Zero);
			return i;
		}

		//public int OGG_Stop(int Nb)
		public int OGG_Stop()
		{			
			int i = mciSendString("stop OGG" + Nb.ToString(),null ,0, IntPtr.Zero);
			timer1.Enabled = false;
			return i;
		}
		
		//public int OGG_Free(int Nb) 
		public int OGG_Free() 
		{			
			int i = mciSendString("close OGG" + Nb.ToString(),null ,0, IntPtr.Zero); 
			timer1.Enabled = false;
			return i;
		}

		//public int OGG_SetVolume(int Nb,int volume) 
		public int OGG_SetVolume(int volume) 
		{			
			int i = mciSendString("SetAudio OGG" + Nb.ToString()+" volume to " + volume.ToString(),null ,0, IntPtr.Zero);
			return i;
		}

		//public int OGG_GetVolume(int Nb) 
		public int OGG_GetVolume() 
		{
			int i = mciSendString("status OGG" + Nb.ToString()+" volume",sBuffer, sBuffer.Capacity, IntPtr.Zero);
			return int.Parse(sBuffer.ToString());
		}


		// Property
		public int OGG_Volume
		{
			get
			{
				if(OGG_IsPlaying() == 1)
				{
					return OGG_GetVolume();
				}
				else
				{
					return 0;
				}
			}
			set
			{
				if(value <= 1000)
				{
					OGG_SetVolume(value);
				}
			}
		}		
		
		//public int OGG_PlayPart(int Nb,int Start,int endPos) 
		public int OGG_PlayPart(int Start,int endPos) 
		{			
			int i = mciSendString("play OGG" + Nb.ToString() + " from " + Start.ToString()+" to " + endPos.ToString(),null ,0, IntPtr.Zero);
			return i;
		}

		//public int OGG_PlayPart(int Nb,int Start,int endPos) 
		public int OGG_PlayPart(int Start) 
		{			
			int i = mciSendString("play OGG" + Nb.ToString() + " from " + Start.ToString(),null ,0, IntPtr.Zero);
			return i;
		}
		
		//public int OGG_SetSpeed(int Nb,int Tempo) 
		public int OGG_SetSpeed(int Tempo) 
		{			
			int i = mciSendString("set OGG"+ Nb.ToString()+" Speed "+ Tempo.ToString(),null ,0, IntPtr.Zero);
			return i;
		}

		//public int OGG_GetSpeed(int Nb) 
		public int OGG_GetSpeed() 
		{
			int i = mciSendString("status OGG" + Nb.ToString()+" Speed",sBuffer, sBuffer.Capacity, IntPtr.Zero);
			return int.Parse(sBuffer.ToString());
		}

		public int OGG_Speed
		{
			get
			{
				return OGG_GetSpeed();
			}
			set
			{
				OGG_SetSpeed(value);
			}
		}
		
		//public int OGG_GetLength(int Nb) 
		public int OGG_GetLength() 
		{			
			byte []btmp = new Byte[256];
			System.Text.Encoding.GetEncoding(874).GetString(btmp);
			string a = System.Text.Encoding.GetEncoding(874).GetString(btmp);
			int i = mciSendString("status OGG" + Nb.ToString()+" length",sBuffer, sBuffer.Capacity, IntPtr.Zero);
			return int.Parse(sBuffer.ToString());
		}

		//public int OGG_GetPosition(int Nb) 
		public int OGG_GetPosition() 
		{
			int i = mciSendString("status OGG" + Nb.ToString()+" position",sBuffer, sBuffer.Capacity, IntPtr.Zero);
			return int.Parse(sBuffer.ToString());
		}

		public int OGG_Position
		{
			set
			{
				OGG_PlayPart(value);
			}

			get
			{
				return OGG_GetPosition();
			}
		}

		//public int OGG_IsPlaying(int Nb)
		public int OGG_IsPlaying() 
		{
			int i = mciSendString("status OGG" + Nb.ToString() + " mode",sBuffer, sBuffer.Capacity, IntPtr.Zero);

			if (sBuffer.ToString(0,sBuffer.Length) == "playing")
			{
				return 1;
			}
			return 0;
		}

		//public int OGG_IsPaused(int Nb) 
		public int OGG_IsPaused() 
		{
			int i = mciSendString("status OGG" + Nb.ToString() + " mode",sBuffer, sBuffer.Capacity, IntPtr.Zero);
			if (sBuffer.ToString(0,sBuffer.Length) == "paused")
			{
				return 1;
			}
			return 0;
		}
		
		//public int OGG_IsReady(int Nb) 
		public int OGG_IsReady() 
		{
			int i = mciSendString("status OGG" + Nb.ToString()+" mode",sBuffer, sBuffer.Capacity, IntPtr.Zero);
		
			if (sBuffer.ToString(0,sBuffer.Length) == "not ready")
			{
				return 0;
			}
			return 1;
		}

		//public int OGG_Seek(int Nb,int pos)
		public int OGG_Seek(int pos)
		{
			
			int ok = 0;
			//if (OGG_IsPlaying(Nb) == 1)
			if (OGG_IsPlaying() == 1)
			{
				ok = 1;
				//OGG_Stop(Nb);
				OGG_Stop();
			}

			int i = mciSendString("Seek OGG" + Nb.ToString()+" to "+pos.ToString(),null ,0, IntPtr.Zero);

			if (ok == 1)
			{
				mciSendString("play OGG" + Nb.ToString(),null ,0, IntPtr.Zero);
			}
			return i;
		}
	}

	public class PositionChangedEventArgs : EventArgs
	{
		private int _position;

		public PositionChangedEventArgs(int num)
		{
			this._position = num;
		}

		public int newPosition
		{
			get
			{
				return _position;
			}
		}
	}
}
