using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace MyMP3_X1
{
	/// <summary>
	/// Summary description for MixForm.
	/// </summary>
	public class MixForm : System.Windows.Forms.Form
	{
		ArrayList m_arrSongList = new ArrayList(); // ���ӴѺ�ŧ
		int m_iOldVolume = 1000;
		int m_iCountAllSong = 0; // ���ӴѺ�ŧ������Դ�����

		private MyMP3_X1.MCIControl mciControl1;
		private MyMP3_X1.MCIControl mciControl2;
		private System.Windows.Forms.TrackBar trackBar1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.PictureBox pictureBox2;
		private System.Windows.Forms.RadioButton radioButton2;
		private System.Windows.Forms.RadioButton radioButton1;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.RadioButton radioButton4;
		private System.Windows.Forms.RadioButton radioButton3;
		private System.Windows.Forms.PictureBox pictureBox4;
		private System.Windows.Forms.PictureBox pictureBox3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.NumericUpDown numericUpDown1;
		private System.Windows.Forms.RadioButton radioButton5;
		private System.Windows.Forms.PictureBox pictureBox5;
		private System.Windows.Forms.ListView listView1;
		private System.Windows.Forms.ColumnHeader columnHeader1;
		private System.Windows.Forms.ColumnHeader columnHeader2;
		private System.Windows.Forms.ColumnHeader columnHeader3;
		private System.Windows.Forms.ColumnHeader columnHeader4;
		private System.Windows.Forms.ColumnHeader columnHeader5;
		private System.Windows.Forms.ColumnHeader columnHeader6;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Button button4;
		private System.Windows.Forms.Button button5;
		private System.Windows.Forms.Button button6;
		private System.Windows.Forms.Button button7;
		private System.Windows.Forms.Button button9;
		private System.Windows.Forms.Button button10;
		private System.Windows.Forms.CheckBox checkBox1;
		private System.Windows.Forms.Button button8;
		private System.Windows.Forms.RadioButton radioButton6;
		private System.Windows.Forms.RadioButton radioButton7;
		private System.Windows.Forms.PictureBox pictureBox6;
		private System.Windows.Forms.PictureBox pictureBox7;
		private System.Windows.Forms.RadioButton radioButton8;
		private System.Windows.Forms.PictureBox pictureBox8;
		private System.Windows.Forms.CheckBox checkBox2;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public MixForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			mciControl1.initMCI(1);
			mciControl2.initMCI(2);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(MixForm));
			this.mciControl1 = new MyMP3_X1.MCIControl();
			this.mciControl2 = new MyMP3_X1.MCIControl();
			this.trackBar1 = new System.Windows.Forms.TrackBar();
			this.label1 = new System.Windows.Forms.Label();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.radioButton6 = new System.Windows.Forms.RadioButton();
			this.radioButton7 = new System.Windows.Forms.RadioButton();
			this.pictureBox6 = new System.Windows.Forms.PictureBox();
			this.pictureBox7 = new System.Windows.Forms.PictureBox();
			this.radioButton8 = new System.Windows.Forms.RadioButton();
			this.pictureBox8 = new System.Windows.Forms.PictureBox();
			this.radioButton5 = new System.Windows.Forms.RadioButton();
			this.pictureBox5 = new System.Windows.Forms.PictureBox();
			this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
			this.label2 = new System.Windows.Forms.Label();
			this.radioButton4 = new System.Windows.Forms.RadioButton();
			this.radioButton3 = new System.Windows.Forms.RadioButton();
			this.pictureBox4 = new System.Windows.Forms.PictureBox();
			this.pictureBox3 = new System.Windows.Forms.PictureBox();
			this.radioButton1 = new System.Windows.Forms.RadioButton();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.radioButton2 = new System.Windows.Forms.RadioButton();
			this.pictureBox2 = new System.Windows.Forms.PictureBox();
			this.listView1 = new System.Windows.Forms.ListView();
			this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader4 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader5 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader6 = new System.Windows.Forms.ColumnHeader();
			this.button2 = new System.Windows.Forms.Button();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.checkBox2 = new System.Windows.Forms.CheckBox();
			this.button3 = new System.Windows.Forms.Button();
			this.button1 = new System.Windows.Forms.Button();
			this.label3 = new System.Windows.Forms.Label();
			this.button4 = new System.Windows.Forms.Button();
			this.button5 = new System.Windows.Forms.Button();
			this.button6 = new System.Windows.Forms.Button();
			this.button7 = new System.Windows.Forms.Button();
			this.button9 = new System.Windows.Forms.Button();
			this.button10 = new System.Windows.Forms.Button();
			this.checkBox1 = new System.Windows.Forms.CheckBox();
			this.button8 = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
			this.groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
			this.groupBox2.SuspendLayout();
			this.SuspendLayout();
			// 
			// mciControl1
			// 
			this.mciControl1.BackColor = System.Drawing.Color.DarkBlue;
			this.mciControl1.Location = new System.Drawing.Point(8, 8);
			this.mciControl1.Name = "mciControl1";
			this.mciControl1.Size = new System.Drawing.Size(368, 208);
			this.mciControl1.TabIndex = 5;
			// 
			// mciControl2
			// 
			this.mciControl2.BackColor = System.Drawing.Color.DarkBlue;
			this.mciControl2.Location = new System.Drawing.Point(432, 8);
			this.mciControl2.Name = "mciControl2";
			this.mciControl2.Size = new System.Drawing.Size(368, 208);
			this.mciControl2.TabIndex = 15;
			// 
			// trackBar1
			// 
			this.trackBar1.LargeChange = 50;
			this.trackBar1.Location = new System.Drawing.Point(376, 32);
			this.trackBar1.Maximum = 1200;
			this.trackBar1.Name = "trackBar1";
			this.trackBar1.Orientation = System.Windows.Forms.Orientation.Vertical;
			this.trackBar1.Size = new System.Drawing.Size(45, 184);
			this.trackBar1.SmallChange = 50;
			this.trackBar1.TabIndex = 10;
			this.trackBar1.TickFrequency = 120;
			this.trackBar1.TickStyle = System.Windows.Forms.TickStyle.Both;
			this.trackBar1.Value = 1200;
			this.trackBar1.ValueChanged += new System.EventHandler(this.onMasterVolume);
			// 
			// label1
			// 
			this.label1.ForeColor = System.Drawing.Color.Red;
			this.label1.Location = new System.Drawing.Point(376, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(48, 24);
			this.label1.TabIndex = 6;
			this.label1.Text = "���§";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.AddRange(new System.Windows.Forms.Control[] {
																					this.radioButton6,
																					this.radioButton7,
																					this.pictureBox6,
																					this.pictureBox7,
																					this.radioButton8,
																					this.pictureBox8,
																					this.radioButton5,
																					this.pictureBox5,
																					this.numericUpDown1,
																					this.label2,
																					this.radioButton4,
																					this.radioButton3,
																					this.pictureBox4,
																					this.pictureBox3,
																					this.radioButton1,
																					this.pictureBox1,
																					this.radioButton2,
																					this.pictureBox2});
			this.groupBox1.ForeColor = System.Drawing.Color.White;
			this.groupBox1.Location = new System.Drawing.Point(16, 208);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(768, 128);
			this.groupBox1.TabIndex = 20;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "�ٻẺ Mixer";
			// 
			// radioButton6
			// 
			this.radioButton6.Location = new System.Drawing.Point(536, 80);
			this.radioButton6.Name = "radioButton6";
			this.radioButton6.Size = new System.Drawing.Size(64, 24);
			this.radioButton6.TabIndex = 60;
			this.radioButton6.Text = "FadeIn";
			this.radioButton6.Click += new System.EventHandler(this.onClick);
			// 
			// radioButton7
			// 
			this.radioButton7.Location = new System.Drawing.Point(368, 72);
			this.radioButton7.Name = "radioButton7";
			this.radioButton7.Size = new System.Drawing.Size(64, 40);
			this.radioButton7.TabIndex = 55;
			this.radioButton7.Text = "FadeIn&& Out";
			this.radioButton7.Click += new System.EventHandler(this.onClick);
			// 
			// pictureBox6
			// 
			this.pictureBox6.BackgroundImage = ((System.Drawing.Bitmap)(resources.GetObject("pictureBox6.BackgroundImage")));
			this.pictureBox6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.pictureBox6.Location = new System.Drawing.Point(600, 72);
			this.pictureBox6.Name = "pictureBox6";
			this.pictureBox6.Size = new System.Drawing.Size(92, 46);
			this.pictureBox6.TabIndex = 23;
			this.pictureBox6.TabStop = false;
			// 
			// pictureBox7
			// 
			this.pictureBox7.BackgroundImage = ((System.Drawing.Bitmap)(resources.GetObject("pictureBox7.BackgroundImage")));
			this.pictureBox7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.pictureBox7.Location = new System.Drawing.Point(432, 72);
			this.pictureBox7.Name = "pictureBox7";
			this.pictureBox7.Size = new System.Drawing.Size(92, 46);
			this.pictureBox7.TabIndex = 22;
			this.pictureBox7.TabStop = false;
			// 
			// radioButton8
			// 
			this.radioButton8.Location = new System.Drawing.Point(192, 80);
			this.radioButton8.Name = "radioButton8";
			this.radioButton8.Size = new System.Drawing.Size(72, 24);
			this.radioButton8.TabIndex = 50;
			this.radioButton8.Text = "FadeOut";
			this.radioButton8.Click += new System.EventHandler(this.onClick);
			// 
			// pictureBox8
			// 
			this.pictureBox8.BackgroundImage = ((System.Drawing.Bitmap)(resources.GetObject("pictureBox8.BackgroundImage")));
			this.pictureBox8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.pictureBox8.Location = new System.Drawing.Point(264, 72);
			this.pictureBox8.Name = "pictureBox8";
			this.pictureBox8.Size = new System.Drawing.Size(92, 46);
			this.pictureBox8.TabIndex = 21;
			this.pictureBox8.TabStop = false;
			// 
			// radioButton5
			// 
			this.radioButton5.Location = new System.Drawing.Point(24, 80);
			this.radioButton5.Name = "radioButton5";
			this.radioButton5.Size = new System.Drawing.Size(64, 24);
			this.radioButton5.TabIndex = 45;
			this.radioButton5.Text = "Mix";
			this.radioButton5.Click += new System.EventHandler(this.onClick);
			// 
			// pictureBox5
			// 
			this.pictureBox5.BackgroundImage = ((System.Drawing.Bitmap)(resources.GetObject("pictureBox5.BackgroundImage")));
			this.pictureBox5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.pictureBox5.Location = new System.Drawing.Point(88, 72);
			this.pictureBox5.Name = "pictureBox5";
			this.pictureBox5.Size = new System.Drawing.Size(92, 46);
			this.pictureBox5.TabIndex = 19;
			this.pictureBox5.TabStop = false;
			// 
			// numericUpDown1
			// 
			this.numericUpDown1.Location = new System.Drawing.Point(704, 48);
			this.numericUpDown1.Minimum = new System.Decimal(new int[] {
																		   3,
																		   0,
																		   0,
																		   0});
			this.numericUpDown1.Name = "numericUpDown1";
			this.numericUpDown1.Size = new System.Drawing.Size(48, 20);
			this.numericUpDown1.TabIndex = 65;
			this.numericUpDown1.Value = new System.Decimal(new int[] {
																		 5,
																		 0,
																		 0,
																		 0});
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(712, 24);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(32, 16);
			this.label2.TabIndex = 16;
			this.label2.Text = "�Թҷ�";
			// 
			// radioButton4
			// 
			this.radioButton4.Location = new System.Drawing.Point(536, 16);
			this.radioButton4.Name = "radioButton4";
			this.radioButton4.Size = new System.Drawing.Size(64, 40);
			this.radioButton4.TabIndex = 40;
			this.radioButton4.Text = "FadeIn&& Mix";
			this.radioButton4.Click += new System.EventHandler(this.onClick);
			// 
			// radioButton3
			// 
			this.radioButton3.Location = new System.Drawing.Point(368, 24);
			this.radioButton3.Name = "radioButton3";
			this.radioButton3.Size = new System.Drawing.Size(56, 24);
			this.radioButton3.TabIndex = 35;
			this.radioButton3.Text = "Cross";
			this.radioButton3.Click += new System.EventHandler(this.onClick);
			// 
			// pictureBox4
			// 
			this.pictureBox4.BackgroundImage = ((System.Drawing.Bitmap)(resources.GetObject("pictureBox4.BackgroundImage")));
			this.pictureBox4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.pictureBox4.Location = new System.Drawing.Point(600, 16);
			this.pictureBox4.Name = "pictureBox4";
			this.pictureBox4.Size = new System.Drawing.Size(92, 46);
			this.pictureBox4.TabIndex = 13;
			this.pictureBox4.TabStop = false;
			// 
			// pictureBox3
			// 
			this.pictureBox3.BackgroundImage = ((System.Drawing.Bitmap)(resources.GetObject("pictureBox3.BackgroundImage")));
			this.pictureBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.pictureBox3.Location = new System.Drawing.Point(432, 16);
			this.pictureBox3.Name = "pictureBox3";
			this.pictureBox3.Size = new System.Drawing.Size(92, 46);
			this.pictureBox3.TabIndex = 12;
			this.pictureBox3.TabStop = false;
			// 
			// radioButton1
			// 
			this.radioButton1.Checked = true;
			this.radioButton1.Location = new System.Drawing.Point(24, 24);
			this.radioButton1.Name = "radioButton1";
			this.radioButton1.Size = new System.Drawing.Size(64, 24);
			this.radioButton1.TabIndex = 25;
			this.radioButton1.TabStop = true;
			this.radioButton1.Text = "Normal";
			this.radioButton1.Click += new System.EventHandler(this.onClick);
			// 
			// pictureBox1
			// 
			this.pictureBox1.BackgroundImage = ((System.Drawing.Bitmap)(resources.GetObject("pictureBox1.BackgroundImage")));
			this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.pictureBox1.Location = new System.Drawing.Point(88, 16);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(92, 46);
			this.pictureBox1.TabIndex = 10;
			this.pictureBox1.TabStop = false;
			// 
			// radioButton2
			// 
			this.radioButton2.Location = new System.Drawing.Point(192, 16);
			this.radioButton2.Name = "radioButton2";
			this.radioButton2.Size = new System.Drawing.Size(72, 40);
			this.radioButton2.TabIndex = 30;
			this.radioButton2.Text = "FadeOut&& Mix";
			this.radioButton2.Click += new System.EventHandler(this.onClick);
			// 
			// pictureBox2
			// 
			this.pictureBox2.BackgroundImage = ((System.Drawing.Bitmap)(resources.GetObject("pictureBox2.BackgroundImage")));
			this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.pictureBox2.Location = new System.Drawing.Point(264, 16);
			this.pictureBox2.Name = "pictureBox2";
			this.pictureBox2.Size = new System.Drawing.Size(92, 46);
			this.pictureBox2.TabIndex = 11;
			this.pictureBox2.TabStop = false;
			// 
			// listView1
			// 
			this.listView1.BackColor = System.Drawing.Color.DarkBlue;
			this.listView1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.listView1.CheckBoxes = true;
			this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
																						this.columnHeader1,
																						this.columnHeader2,
																						this.columnHeader3,
																						this.columnHeader4,
																						this.columnHeader5,
																						this.columnHeader6});
			this.listView1.ForeColor = System.Drawing.Color.White;
			this.listView1.FullRowSelect = true;
			this.listView1.HideSelection = false;
			this.listView1.Location = new System.Drawing.Point(16, 24);
			this.listView1.MultiSelect = false;
			this.listView1.Name = "listView1";
			this.listView1.Size = new System.Drawing.Size(592, 152);
			this.listView1.TabIndex = 70;
			this.listView1.View = System.Windows.Forms.View.Details;
			// 
			// columnHeader1
			// 
			this.columnHeader1.Text = "�ӴѺ";
			// 
			// columnHeader2
			// 
			this.columnHeader2.Text = "����";
			// 
			// columnHeader3
			// 
			this.columnHeader3.Text = "�ŧ";
			this.columnHeader3.Width = 120;
			// 
			// columnHeader4
			// 
			this.columnHeader4.Text = "��ŻԹ";
			this.columnHeader4.Width = 120;
			// 
			// columnHeader5
			// 
			this.columnHeader5.Text = "��ź��";
			this.columnHeader5.Width = 120;
			// 
			// columnHeader6
			// 
			this.columnHeader6.Text = "����";
			this.columnHeader6.Width = 80;
			// 
			// button2
			// 
			this.button2.ForeColor = System.Drawing.Color.White;
			this.button2.Location = new System.Drawing.Point(632, 136);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(112, 32);
			this.button2.TabIndex = 74;
			this.button2.Text = "ź�ŧ���¡��";
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.AddRange(new System.Windows.Forms.Control[] {
																					this.checkBox2,
																					this.listView1,
																					this.button2});
			this.groupBox2.ForeColor = System.Drawing.Color.White;
			this.groupBox2.Location = new System.Drawing.Point(16, 344);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(768, 184);
			this.groupBox2.TabIndex = 10;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "�ӴѺ�ŧ";
			// 
			// checkBox2
			// 
			this.checkBox2.Location = new System.Drawing.Point(632, 104);
			this.checkBox2.Name = "checkBox2";
			this.checkBox2.TabIndex = 72;
			this.checkBox2.Text = "���͡������";
			this.checkBox2.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
			// 
			// button3
			// 
			this.button3.ForeColor = System.Drawing.Color.Orange;
			this.button3.Location = new System.Drawing.Point(16, 536);
			this.button3.Name = "button3";
			this.button3.TabIndex = 75;
			this.button3.Text = "Effect 1";
			this.button3.Click += new System.EventHandler(this.onEffectClick);
			// 
			// button1
			// 
			this.button1.ForeColor = System.Drawing.Color.Orange;
			this.button1.Location = new System.Drawing.Point(96, 536);
			this.button1.Name = "button1";
			this.button1.TabIndex = 80;
			this.button1.Text = "Effect 2";
			this.button1.Click += new System.EventHandler(this.onEffectClick);
			// 
			// label3
			// 
			this.label3.ForeColor = System.Drawing.Color.White;
			this.label3.Location = new System.Drawing.Point(504, 0);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(288, 16);
			this.label3.TabIndex = 13;
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// button4
			// 
			this.button4.ForeColor = System.Drawing.Color.Orange;
			this.button4.Location = new System.Drawing.Point(176, 536);
			this.button4.Name = "button4";
			this.button4.TabIndex = 85;
			this.button4.Text = "Effect 3";
			this.button4.Click += new System.EventHandler(this.onEffectClick);
			// 
			// button5
			// 
			this.button5.ForeColor = System.Drawing.Color.Orange;
			this.button5.Location = new System.Drawing.Point(416, 536);
			this.button5.Name = "button5";
			this.button5.TabIndex = 100;
			this.button5.Text = "Effect 6";
			this.button5.Click += new System.EventHandler(this.onEffectClick);
			// 
			// button6
			// 
			this.button6.ForeColor = System.Drawing.Color.Orange;
			this.button6.Location = new System.Drawing.Point(336, 536);
			this.button6.Name = "button6";
			this.button6.TabIndex = 95;
			this.button6.Text = "Effect 5";
			this.button6.Click += new System.EventHandler(this.onEffectClick);
			// 
			// button7
			// 
			this.button7.ForeColor = System.Drawing.Color.Orange;
			this.button7.Location = new System.Drawing.Point(256, 536);
			this.button7.Name = "button7";
			this.button7.TabIndex = 90;
			this.button7.Text = "Effect 4";
			this.button7.Click += new System.EventHandler(this.onEffectClick);
			// 
			// button9
			// 
			this.button9.ForeColor = System.Drawing.Color.Orange;
			this.button9.Location = new System.Drawing.Point(576, 536);
			this.button9.Name = "button9";
			this.button9.TabIndex = 110;
			this.button9.Text = "Effect 8";
			this.button9.Click += new System.EventHandler(this.onEffectClick);
			// 
			// button10
			// 
			this.button10.ForeColor = System.Drawing.Color.Orange;
			this.button10.Location = new System.Drawing.Point(496, 536);
			this.button10.Name = "button10";
			this.button10.TabIndex = 105;
			this.button10.Text = "Effect 7";
			this.button10.Click += new System.EventHandler(this.onEffectClick);
			// 
			// checkBox1
			// 
			this.checkBox1.ForeColor = System.Drawing.Color.White;
			this.checkBox1.Location = new System.Drawing.Point(744, 536);
			this.checkBox1.Name = "checkBox1";
			this.checkBox1.Size = new System.Drawing.Size(48, 16);
			this.checkBox1.TabIndex = 120;
			this.checkBox1.Text = "Edit";
			// 
			// button8
			// 
			this.button8.ForeColor = System.Drawing.Color.Orange;
			this.button8.Location = new System.Drawing.Point(656, 536);
			this.button8.Name = "button8";
			this.button8.TabIndex = 115;
			this.button8.Text = "Effect 9";
			this.button8.Click += new System.EventHandler(this.onEffectClick);
			// 
			// MixForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.DarkBlue;
			this.ClientSize = new System.Drawing.Size(800, 568);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.button8,
																		  this.checkBox1,
																		  this.button9,
																		  this.button10,
																		  this.button5,
																		  this.button6,
																		  this.button7,
																		  this.button4,
																		  this.label3,
																		  this.button1,
																		  this.button3,
																		  this.groupBox2,
																		  this.groupBox1,
																		  this.label1,
																		  this.trackBar1,
																		  this.mciControl2,
																		  this.mciControl1});
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.KeyPreview = true;
			this.Name = "MixForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.onKeyDown);
			this.Load += new System.EventHandler(this.MixForm_Load);
			this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.onKeyUp);
			((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
			this.groupBox1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
			this.groupBox2.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void startSong()
		{
//			if (m_arrSongList.Count > 0 && !m_bNowPlaying)
//			{
//				SongStructure ss = (SongStructure)m_arrSongList[0];
//				m_strKey = ss.m_strFilePlayer;
//				label2.Text = ss.m_strSongName + " - " + ss.m_strArtistName;
//				openClip(m_strKey,ss); // �����ŧ����� path
//				m_strKey = "";
//			}
		}

		public void addSong(SongStructure ss)
		{
			m_arrSongList.Add(ss);
			m_iCountAllSong++;
			listView1.Items.Add(new ListViewItem(new string[]{m_iCountAllSong.ToString(),ss.m_strNo,ss.m_strSongName
																 ,ss.m_strArtistName,ss.m_strAlbumName,""}));
			startSong();
		}

		private void onKeyUp(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			switch(e.KeyCode)
			{
				case Keys.F4:
				{
					this.Hide();
					break;
				}
			}
		}

		private void onKeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			switch(e.KeyCode)
			{
				case Keys.Escape:
				case Keys.F1:
				case Keys.F2:
				case Keys.F3:
				//case Keys.F4:// ��� key ����͡
				case Keys.F5:
				case Keys.F6: 
				case Keys.F7:
				case Keys.F8:
				case Keys.F9:
				case Keys.F10:
				{
					this.Hide();
					break;
				}
			}		
		}

		private void onMasterVolume(object sender, System.EventArgs e)
		{
			int diff = Math.Abs(this.trackBar1.Value - m_iOldVolume);
			if (trackBar1.Value > m_iOldVolume)
			{
				// inc				
				mciControl1.incVolume(diff);
				mciControl2.incVolume(diff);
			}
			else
			{
				// dec
				mciControl1.decVolume(diff);
				mciControl2.decVolume(diff);
			}
			m_iOldVolume = trackBar1.Value;
		}


		private void MixForm_Load(object sender, System.EventArgs e)
		{
			label3.Text = MainForm.m_strProgramName + " " + MainForm.m_strVersion;

		}

		private void onEffectClick(object sender, System.EventArgs e)
		{
//			switch (sender)
//			{
//				case button1:
//				{
//					break;
//				}
//			}
//			
		}

		private void checkBox2_CheckedChanged(object sender, System.EventArgs e)
		{
			bool bcheck = false;
			if (checkBox2.Checked)
			{
				bcheck = true;
			}
			
			for (int x = 0; x < listView1.Items.Count; x ++)
			{
				listView1.Items[x].Checked = bcheck;
			}
		}

		private void onClick(object sender, System.EventArgs e)
		{
//			switch (sender)
//			{
//				case radioButton1:
//				{
//					// Normal
//					break;
//				}
//				case radioButton2:
//				{
//					// FadeOut & Mix
//					break;
//				}
//				case radioButton3:
//				{
//					// Cross
//					break;
//				}
//				case radioButton4:
//				{
//					// FadeIn & Mix
//					break;
//				}
//				case radioButton5:
//				{
//					// Mix
//					break;
//				}
//				case radioButton8:
//				{
//					// FadeOut
//					break;
//				}
//				case radioButton7:
//				{
//					// FadeIn & Out
//					break;
//				}
//				case radioButton6:
//				{
//					// FadeIn
//					break;
//				}
//			}
		}
	}
}
