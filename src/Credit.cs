using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace MyMP3_X1
{
	/// <summary>
	/// Summary description for Credit.
	/// </summary>
	public class Credit : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox textBox1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public Credit()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
            this.TransparencyKey = System.Drawing.Color.FromArgb(((System.Byte)(76)), ((System.Byte)(75)), ((System.Byte)(54)));
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.BackColor = System.Drawing.Color.LightCyan;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.ForeColor = System.Drawing.Color.Blue;
			this.label1.Location = new System.Drawing.Point(48, 48);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(716, 208);
			this.label1.TabIndex = 41;
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// textBox1
			// 
			this.textBox1.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(76)), ((System.Byte)(75)), ((System.Byte)(54)));
			this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold);
			this.textBox1.ForeColor = System.Drawing.Color.LightCyan;
			this.textBox1.Location = new System.Drawing.Point(48, 8);
			this.textBox1.Name = "textBox1";
			this.textBox1.ReadOnly = true;
			this.textBox1.Size = new System.Drawing.Size(144, 31);
			this.textBox1.TabIndex = 44;
			this.textBox1.TabStop = false;
			this.textBox1.Text = "�͢ͺ�س";
			// 
			// Credit
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(76)), ((System.Byte)(75)), ((System.Byte)(54)));
			this.ClientSize = new System.Drawing.Size(800, 264);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.textBox1,
																		  this.label1});
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "Credit";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.Text = "Credit";
			this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.onKeyDown);
			this.Load += new System.EventHandler(this.Credit_Load);
			this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.onKeyUp);
			this.ResumeLayout(false);

		}
		#endregion

		private void Credit_Load(object sender, System.EventArgs e)
		{
			label1.Text = "�Ҩ�� ����Ѻ�Ӫ���� ����´�� ��������� ��зع��Ѿ��,\r\n"
				+"�س��Ԫҵ� ������͹ ����Ѻ���й��������µ�ҧ�,\r\n"
				+"����� ����¾ٴ��� ����ͧ��ǵ�ҧ� ����ʹ�����ͧ����� ���ѹ���� �����,\r\n"
				+"����ͧ,��������,����͡,�� ����Ѻ������ǧ��,\r\n"
				+"���͹�ʹ��..��,��� ����繷���к�»ѭ��㹷ء����ͧ,\r\n"
				+"�س��� ����Ѻ background �ٻ����ʹ��� ����������⡨��֧�ء�ѹ���,\r\n"
				+"\r\n"
				+"Special Thank to Mr.Dracore (Canada) for vcdGear special version\r\n"
				+"�������ա���·�ҹ����������ö�С��������� ..\r\n"
				+"����ա���駤�����ҡ�Թ� ����Ѻ�����..\r\n"
				+"\"�͢ͺ��Фس�ҡ��Ѻ (^^)\"";
		}

		private void onKeyUp(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			switch (e.KeyData)
			{
				case Keys.F10:
				case Keys.Escape:
				{
					this.Close();
					break;
				}
			}	
		}

		private void onKeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			if (e.KeyData == Keys.Escape ||
				e.KeyData == Keys.F1 ||
				e.KeyData == Keys.F2 ||				
				e.KeyData == Keys.F3 ||				

				e.KeyData == Keys.F5 ||
				e.KeyData == Keys.F6 ||
				e.KeyData == Keys.F7 ||
				e.KeyData == Keys.F8 || 
				e.KeyData == Keys.F9)
				//e.KeyData == Keys.F10) // menu ������˹�Դ ��һ�������͡
			{
				this.Close();
			}
		}
	}
}
