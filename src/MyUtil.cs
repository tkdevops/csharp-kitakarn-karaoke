
// 29 �.�. 2548
// ����� win2k ��ä��� MAC Address �����աẺ˹�� !!
//#define __WIN2K__

/*
 * 17 �.�. 2546
 * -. ����¹ cpuID �� MACAddress ���� cpuID ����� !!!
 * 
 *  22 �.�. 2546
 * -. ���� checkCodeSum �Ѻ makeCodeSum ���Ǩ�ͺ��������١��Ҿ����Դ�������ŧ����¹���
 * 
 * 14 �.�. 2546 
 * -. ����¹���˹� register ������ classroot
 * -. ���ҧ��� register �� GUID {B78AA36E-24AE-4162-8005-7B3D024D480A} 
 *		���������ʴ��� E1B0F4347374F9B9767DBD3D7B7B824183C3C587454849898C0BC90E4A50D1514E5353569385
 * -. ����¹��������� �ʹ������ >> 6 ��� << 2
 */

using System;
using System.Management;
using Microsoft.Win32;

namespace MyMP3_X1
{
	public class MyUtil
	{
		// GUID �������� �������¹��������� ClassRoot
		// {B78AA36E-24AE-4162-8005-7B3D024D480A}
		// ���ʤ�� : E1B0F4347374F9B9767DBD3D7B7B824183C3C587454849898C0BC90E4A50D1514E5353569385
		string __DEFAULT_REGKEY__ = "E1B0F4347374F9B9767DBD3D7B7B824183C3C587454849898C0BC90E4A50D1514E5353569385";	
		char []m_cHexTable = new char[]{'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};


		////////////////////////////////////////////////////////////////////////
		/// ��ǹ config �����ͷӡ������͹�Ե ...�ҹ� ������¹ ������¹��������������������͹˹�ҹ��
		/// ��ⴹ crack ������� �������¹ ����ѡ�÷����������ҷ�������������� ��ͧ�����¹��������
		/// encodeTxt ����������Ͷʹ�����������...
		int __SHIFT1__ = 6;
		int __SHIFT2__ = 2;
		////////////////////////////////////////////////////////////////////////

						
		public MyUtil()
		{
		}

		// ��Ѻ����¹�ٻẺ����������
		public void setShiftValue(int x1,int x2)
		{
			__SHIFT1__ = x1;
			__SHIFT2__ = x2;
		}

		// ����¹���� RegKey
		public void setDefaultRegKey(string strRegKey)
		{
			__DEFAULT_REGKEY__ = strRegKey;
		}


#if (__WIN2K__)
		// Ẻ���ǡѺ������ AutoRegister
		public string getMACAddress()
		{
			ManagementClass mc = new ManagementClass("Win32_NetworkAdapterConfiguration");
			ManagementObjectCollection moc = mc.GetInstances();
			string MACAddress = String.Empty;
			foreach(ManagementObject mo in moc)
			{
				if(MACAddress == String.Empty)  // only return MAC Address from first card
				{
					if((bool)mo["IPEnabled"] == true)
					{
						MACAddress = mo["MacAddress"].ToString() ;
					}
				}
				mo.Dispose();
			}
			MACAddress = MACAddress.Replace(":","");

			// MACAddress �� 12 ��ѡ ���� Parity �ա 4 �� 16
			int tmpMACAddr = 0;
			for (int x = 0; x < MACAddress.Length; x++)
			{
				tmpMACAddr += binTable(MACAddress[x]);
			}
			MACAddress += tmpMACAddr.ToString("0000");
			
			return MACAddress;
		}
#else
        
		/// <summary>
		/// Returns MAC Address from first Network Card in Computer
		/// </summary>
		/// <returns>[string] MAC Address</returns>
		public string getMACAddress()
		{
			ManagementClass mc = new ManagementClass("Win32_NetworkAdapter");
			ManagementObjectCollection moc = mc.GetInstances();
			string MACAddress = String.Empty;
			foreach(ManagementObject mo in moc)
			{
				if(MACAddress == String.Empty)  // only return MAC Address from first card
				{
					try
					{
						if(((string)mo["NetConnectionID"]).Trim().ToUpper().StartsWith("LOCAL AREA CONNECTION") )
						{
							MACAddress = mo["MacAddress"].ToString() ;
							mo.Dispose();
						}
					}
					catch(Exception zbc)
					{
					}
				}				
			}

			if (MACAddress == "")
			{
				return "";
			}

			MACAddress = MACAddress.Replace(":","");
						

			// MACAddress �� 12 ��ѡ ���� Parity �ա 4 �� 16
			int tmpMACAddr = 0;
			for (int x = 0; x < MACAddress.Length; x++)
			{
				tmpMACAddr += binTable(MACAddress[x]);
			}
			MACAddress += tmpMACAddr.ToString("0000");
			
			return MACAddress;
		}
#endif

		//		// Get Processor ID
		//		public string getProcessorID()
		//		{
		//			ManagementObjectSearcher query1 = new ManagementObjectSearcher("SELECT ProcessorId FROM Win32_processor");
		//			ManagementObjectCollection queryCollection1 = query1.Get();
		//			foreach( ManagementObject mo in queryCollection1 ) 
		//			{
		//				return mo["ProcessorId"].ToString();
		//			} 
		//			return "";
		//		}

		// EnCode
		public byte[] encode(byte[] bConvert)
		{
			for (int x = 0; x < bConvert.Length; x++)
			{
				int iTmp = bConvert[x];
				int iTmp2 = iTmp;

				iTmp2 = (iTmp & 0x07) << __SHIFT1__;
				iTmp = (iTmp ^ 0xFF) >> __SHIFT2__;
				iTmp += iTmp2;

				bConvert[x] = (byte)(iTmp + x);
			}
			return bConvert;
		}

		// DeCode
		public byte[] decode(byte[] bInput)
		{
			for (int x = 0; x < bInput.Length; x++)
			{
				int iTmp = bInput[x];
				iTmp = iTmp - x;
				int iTmp2 = (iTmp & 0xE0) >> __SHIFT1__;

				iTmp = (iTmp ^ 0xFF) << __SHIFT2__;
				iTmp += iTmp2;

				bInput[x] = (byte)(iTmp);
			}
			return bInput;
		}

		// Check sPecial ID with string ID
//		public bool checkPIDwithID(string strID)
//		{
//			if ((strID == "") || (strID.Length < 32)) return false;
//
////			// ����Թ��Ҷ١���ŧ����������ͧ
////			byte[] bTmp = System.Text.ASCIIEncoding.ASCII.GetBytes(myUtil.getMACAddress());
////			string strM = myUtil.makeCodeSum(myUtil.bin2hex(myUtil.encode(bTmp)));
//
//			if ((strID == "") || (strID.Length < 32)) return false;
//			string strCPUID = getMACAddress();
//			if (strCPUID == "")
//			{
//				return false;
//			}
//			byte []bCPUID = System.Text.Encoding.GetEncoding(874).GetBytes(strCPUID);
//			byte []bBinID = hex2bin(System.Text.ASCIIEncoding.ASCII.GetBytes(strID.ToUpper()));
//
//			bCPUID = encode(bCPUID);
//			string strXor = System.Text.Encoding.GetEncoding(874).GetString(xorByte(bCPUID,bBinID));
//			if (strXor == strCPUID) return true;
//			return false;
//		}

		public string genRegisterCodeFromMachineID(string strMCID)
		{
			byte [] bXor = hex2bin(System.Text.ASCIIEncoding.ASCII.GetBytes(strMCID));
			byte [] bCPU = decode(hex2bin(System.Text.ASCIIEncoding.ASCII.GetBytes(strMCID)));
			string strReturn = "";
			strReturn = bin2hex(xorByte(bXor,bCPU));
			return strReturn;
		}

		public byte[] xorByte(byte[] bConvert1,byte[] bConvert2)
		{
			for (int x = 0; x < bConvert1.Length; x++)
			{
				int iTmp = bConvert1[x];
				int iTmp2 = bConvert2[x];
				
				bConvert1[x] = (byte)(iTmp ^ iTmp2);
			}
			return bConvert1;
		}

		public string bin2hex(byte[] bConvert1)
		{
			char[] bConvert2 = new char[bConvert1.Length*2];

			for (int x = 0; x < bConvert1.Length; x++)
			{
				int iTmp = bConvert1[x];
				bConvert2[x*2] = m_cHexTable[iTmp >> 4];
				bConvert2[x*2+1] = m_cHexTable[(iTmp & 0x0F)];
			}
			string strReturn = new string(bConvert2);			
			return strReturn;
		}

		private int binTable(char i)
		{
			int iTmpValue = 0;
			switch (i)
			{
				case '0':{ iTmpValue = 0; break;}
				case '1':{ iTmpValue = 1; break;}
				case '2':{ iTmpValue = 2; break;}
				case '3':{ iTmpValue = 3; break;}
				case '4':{ iTmpValue = 4; break;}
				case '5':{ iTmpValue = 5; break;}
				case '6':{ iTmpValue = 6; break;}
				case '7':{ iTmpValue = 7; break;}
				case '8':{ iTmpValue = 8; break;}
				case '9':{ iTmpValue = 9; break;}
				case 'A':{ iTmpValue = 10; break;}
				case 'B':{ iTmpValue = 11; break;}
				case 'C':{ iTmpValue = 12; break;}
				case 'D':{ iTmpValue = 13; break;}
				case 'E':{ iTmpValue = 14; break;}
				case 'F':{ iTmpValue = 15; break;}
				default:{iTmpValue = 15; break;}
			}
			return iTmpValue;
		}

		public byte[] hex2bin(byte[] bHex)
		{
			byte[] bConvert = new byte[bHex.Length/2];

			for (int x = 0; x < bConvert.Length; x++)
			{
				int iTmp = binTable((char)bHex[x*2]) << 4;
				int iTmp2 = binTable((char)bHex[x*2+1]);
				bConvert[x] = (byte)(iTmp + iTmp2);
			}
			return bConvert;
		}


		private string genHEX(int length)
		{
			string strReturn = System.Guid.NewGuid().ToString().ToUpper().Replace("{","").Replace("}","").Replace("-","").Substring(0,length);
			return strReturn;
		}

		//		// Generate HEX string
		//		private string genHEX(int length, int iType) 
		//		{
		//			System.Threading.Thread.Sleep(500);
		//			Random AppRandom = new Random((int)DateTime.Now.Ticks);
		//			
		//			string charlist;
		//			switch (iType)
		//			{
		//				case 0: { charlist = ABC; break; }
		//				case 1: { charlist = abc; break; }
		//				case 2: { charlist = n123; break; }
		//				case 3: { charlist = Mix; break; }
		//				case 4: { charlist = MIX123; break; }
		//				case 5: { charlist = mix123; break; }
		//				case 6: { charlist = Mix123; break; }
		//				case 7: { charlist = HEX; break; }
		//				default:{ charlist = Mix123; break; }
		//			}
		//			
		//			System.Text.StringBuilder sb = new System.Text.StringBuilder();
		//			for (int i=0;i<length;i++) 
		//			{
		//				int r = AppRandom.Next(0, charlist.Length);
		//				sb.Append(charlist.Substring(r, 1));
		//			}
		//			
		//			string strID = sb.ToString();
		//            
		//			// part of encode
		//			return strID;
		//		}


		private char checkChar(int iValue)
		{
			return m_cHexTable[iValue % 16];
		}

		// generate Product ID
		public string genProductID(string strProductType)
		{
			//System.Threading.Thread.Sleep(1000);
			//char[] cHex = genHEX(12,7).ToCharArray();
			char[] cHex = genHEX(12).ToCharArray();
			if (strProductType.Length < 2)
			{
				strProductType += "00";
			}
			cHex[4] = strProductType[0];
			cHex[5] = strProductType[1];

			char[] cEnd = new char[4]; // ���� parity
			cEnd[0] = checkChar(binTable(cHex[0]) + binTable(cHex[1]) + binTable(cHex[2]) + binTable(cHex[3]));
			cEnd[1] = checkChar(binTable(cHex[4]) + binTable(cHex[5]) + binTable(cHex[6]) + binTable(cHex[7]));
			cEnd[2] = checkChar(binTable(cHex[8]) + binTable(cHex[9]) + binTable(cHex[10]) + binTable(cHex[11]));
			int x1 = binTable(cHex[0]) + binTable(cHex[4]) + binTable(cHex[8]);
			int x2 = binTable(cHex[1]) + binTable(cHex[5]) + binTable(cHex[9]);
			int x3 = binTable(cHex[2]) + binTable(cHex[6]) + binTable(cHex[10]);
			int x4 = binTable(cHex[3]) + binTable(cHex[7]) + binTable(cHex[11]);
			cEnd[3] = checkChar(x1+x2+x3+x4);
			string strReturn = new string(cHex) + new string(cEnd);
			strReturn = strReturn.Insert(4," - ");
			strReturn = strReturn.Insert(11," - ");
			strReturn = strReturn.Insert(18," - ");
			return strReturn;			
		}

		// Check Product ID
		public bool checkProductID(string strID,string strProductType)
		{
			if ((strID.Length < 16) || strID == "") return false;
			strID = strID.Trim();
			strID = strID.Replace(" ","");
			strID = strID.Replace("-","");
			
			// ��ҼԴ�������Թ��� ������ҹ
			if (!(strID[4] == strProductType[0] && strID[5] == strProductType[1]))
			{
				return false;
			}

			char[] cHex = strID.ToCharArray();			
			if (cHex[12] != checkChar(binTable(cHex[0]) + binTable(cHex[1])
				+ binTable(cHex[2]) + binTable(cHex[3])))
				return false;
			if (cHex[13] != checkChar(binTable(cHex[4]) + binTable(cHex[5])
				+ binTable(cHex[6]) + binTable(cHex[7])))
				return false;
			if (cHex[14] != checkChar(binTable(cHex[8]) + binTable(cHex[9])
				+ binTable(cHex[10]) + binTable(cHex[11])))
				return false;
			int x1 = binTable(cHex[0]) + binTable(cHex[4]) + binTable(cHex[8]);
			int x2 = binTable(cHex[1]) + binTable(cHex[5]) + binTable(cHex[9]);
			int x3 = binTable(cHex[2]) + binTable(cHex[6]) + binTable(cHex[10]);
			int x4 = binTable(cHex[3]) + binTable(cHex[7]) + binTable(cHex[11]);			
			if (cHex[15] != checkChar(x1+x2+x3+x4)) return false;
			return true;
		}

		public bool deleteSubKeyTree(string strSubKey)
		{
			bool bReturn = true;
			try
			{
				Registry.ClassesRoot.DeleteSubKeyTree(strSubKey);
			}
			catch(Exception ee)
			{
				bReturn = false;
			}
			return bReturn;
		}

		// save to Register
		protected bool saveReg(string strRegName,string strValue)
		{	
			try
			{
				// reg defalut
				string strRegKey = decodeTxt(__DEFAULT_REGKEY__);
				RegistryKey reg = Registry.ClassesRoot.OpenSubKey(strRegKey,true); // for write
				if (reg == null)
				{
					Registry.ClassesRoot.CreateSubKey(strRegKey);
					reg = Registry.ClassesRoot.OpenSubKey(strRegKey,true); // for write
				}

				//reg.SetValue("stringName","�Ţ�ӹǹ��� 32 Bit, String ���� Array Byte ��ҹ��");
				reg.SetValue(strRegName,strValue);			
				reg.Close(); // will hang if don't use
				return true;
			}
			catch(Exception ex)
			{
				string strErr = ex.Message;
				return false;
			}
		}

		public bool saveRegEncode(string strRegName,string strValue)
		{
			return saveReg(strRegName,bin2hex(encode(System.Text.ASCIIEncoding.ASCII.GetBytes(strValue))));
		}
		
		public string getRegDecode(string strRegName)
		{	
			string strReturn = "";
			try
			{
				strReturn = System.Text.ASCIIEncoding.ASCII.GetString(decode(hex2bin(System.Text.ASCIIEncoding.ASCII.GetBytes(getReg(strRegName)))));
			}
			catch(Exception ex)
			{
			}
			return strReturn;
		}

		// get value from register
		protected string getReg(string strRegName)
		{
			try
			{
				// reg defalut 
				string strRegKey = decodeTxt(__DEFAULT_REGKEY__);
				RegistryKey reg = Registry.ClassesRoot.OpenSubKey(strRegKey); // I thing It's read
				if (reg == null)
				{
					return "";
				}
			
				string strReturn = "";
				strReturn = reg.GetValue(strRegName) as string;
				reg.Close(); // will hang if don't use
				return strReturn;
			}
			catch(Exception ex)
			{
				string strErr = ex.Message;
				return "";
			}
		}

		public string makeCodeSum(string strIn)
		{
			int iTmp = 0;
			for (int x = 0; x < strIn.Length; x++)
			{
				iTmp = (strIn[x] ^ iTmp);
			}
			return strIn + "-" + iTmp.ToString();
		}

		public bool checkCodeSum(string strIn)
		{
			bool bReturn = false;
			try
			{
				string[]strArr = strIn.Split('-');
				if (makeCodeSum(strArr[0]) == strIn)
				{				
					bReturn = true;
				}
			}
			catch(Exception ex)
			{
			}
			return bReturn;
		}

		public string encodeTxt(string str)
		{
			return bin2hex(encode(System.Text.ASCIIEncoding.ASCII.GetBytes(str)));
		}

		public string decodeTxt(string str)
		{
			return System.Text.ASCIIEncoding.ASCII.GetString(decode(hex2bin(System.Text.ASCIIEncoding.ASCII.GetBytes(str))));
		}

		public static string reverseString(string str)
		{
			string strReturn = "";
			for (int x = str.Length; x > 0; x--)
			{
				strReturn += str[x-1].ToString();
			}
			return strReturn;
		}
	}
}
