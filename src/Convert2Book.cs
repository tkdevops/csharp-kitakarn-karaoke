using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.IO;

namespace MyMP3_X1
{
	/// <summary>
	/// Summary description for Convert2Book.
	/// </summary>
	public class Convert2Book : System.Windows.Forms.Form
	{
		private ListViewColumnSorter lvwColumnSorter = null;
		System.Collections.IDictionaryEnumerator idic = null;
		int m_Count = 0;

		private System.Windows.Forms.ListView listView1;
		private System.Windows.Forms.ColumnHeader columnHeader1;
		private System.Windows.Forms.ColumnHeader columnHeader3;
		private System.Windows.Forms.ColumnHeader columnHeader4;
		private System.Windows.Forms.ColumnHeader columnHeader5;
		private System.Windows.Forms.ColumnHeader columnHeader6;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox textBox2;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.Button button4;
		private System.Windows.Forms.ProgressBar progressBar1;
		private System.Windows.Forms.Timer timer1;
		private System.Windows.Forms.TextBox textBox6;
		private System.Windows.Forms.Label label10;
		private System.ComponentModel.IContainer components;

		public Convert2Book()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			this.TransparencyKey = System.Drawing.Color.FromArgb(((System.Byte)(76)), ((System.Byte)(75)), ((System.Byte)(54)));

			lvwColumnSorter = new ListViewColumnSorter();
			this.listView1.ListViewItemSorter = lvwColumnSorter;
			this.listView1.Sorting = SortOrder.Ascending;
			this.listView1.AutoArrange = true;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.listView1 = new System.Windows.Forms.ListView();
			this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader4 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader5 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader6 = new System.Windows.Forms.ColumnHeader();
			this.button2 = new System.Windows.Forms.Button();
			this.button1 = new System.Windows.Forms.Button();
			this.label4 = new System.Windows.Forms.Label();
			this.textBox2 = new System.Windows.Forms.TextBox();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.button3 = new System.Windows.Forms.Button();
			this.button4 = new System.Windows.Forms.Button();
			this.progressBar1 = new System.Windows.Forms.ProgressBar();
			this.timer1 = new System.Windows.Forms.Timer(this.components);
			this.textBox6 = new System.Windows.Forms.TextBox();
			this.label10 = new System.Windows.Forms.Label();
			this.groupBox3.SuspendLayout();
			this.SuspendLayout();
			// 
			// listView1
			// 
			this.listView1.BackColor = System.Drawing.Color.LightCyan;
			this.listView1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.listView1.CheckBoxes = true;
			this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
																						this.columnHeader1,
																						this.columnHeader3,
																						this.columnHeader4,
																						this.columnHeader5,
																						this.columnHeader6});
			this.listView1.ForeColor = System.Drawing.Color.Blue;
			this.listView1.FullRowSelect = true;
			this.listView1.HideSelection = false;
			this.listView1.Location = new System.Drawing.Point(48, 48);
			this.listView1.MultiSelect = false;
			this.listView1.Name = "listView1";
			this.listView1.Size = new System.Drawing.Size(448, 176);
			this.listView1.TabIndex = 49;
			this.listView1.View = System.Windows.Forms.View.Details;
			this.listView1.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.onColumnClick);
			// 
			// columnHeader1
			// 
			this.columnHeader1.Text = "�ӴѺ���";
			// 
			// columnHeader3
			// 
			this.columnHeader3.Text = "����";
			// 
			// columnHeader4
			// 
			this.columnHeader4.Text = "�����ŧ";
			this.columnHeader4.Width = 100;
			// 
			// columnHeader5
			// 
			this.columnHeader5.Text = "������ŻԹ";
			this.columnHeader5.Width = 100;
			// 
			// columnHeader6
			// 
			this.columnHeader6.Text = "��ź��";
			this.columnHeader6.Width = 100;
			// 
			// button2
			// 
			this.button2.BackColor = System.Drawing.Color.DimGray;
			this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button2.ForeColor = System.Drawing.Color.White;
			this.button2.Location = new System.Drawing.Point(508, 112);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(72, 20);
			this.button2.TabIndex = 51;
			this.button2.Text = "select all";
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// button1
			// 
			this.button1.BackColor = System.Drawing.Color.DimGray;
			this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button1.ForeColor = System.Drawing.Color.White;
			this.button1.Location = new System.Drawing.Point(584, 112);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(72, 20);
			this.button1.TabIndex = 50;
			this.button1.Text = "�ѹ�֡";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// label4
			// 
			this.label4.BackColor = System.Drawing.Color.Transparent;
			this.label4.ForeColor = System.Drawing.Color.FloralWhite;
			this.label4.Location = new System.Drawing.Point(16, 24);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(40, 16);
			this.label4.TabIndex = 88;
			this.label4.Text = "�ѹ�֡";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// textBox2
			// 
			this.textBox2.BackColor = System.Drawing.Color.LightCyan;
			this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.textBox2.Location = new System.Drawing.Point(60, 20);
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new System.Drawing.Size(124, 20);
			this.textBox2.TabIndex = 87;
			this.textBox2.Text = "";
			// 
			// groupBox3
			// 
			this.groupBox3.BackColor = System.Drawing.Color.Transparent;
			this.groupBox3.Controls.AddRange(new System.Windows.Forms.Control[] {
																					this.textBox2,
																					this.label4,
																					this.button3});
			this.groupBox3.ForeColor = System.Drawing.Color.Yellow;
			this.groupBox3.Location = new System.Drawing.Point(508, 44);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(220, 56);
			this.groupBox3.TabIndex = 89;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "[ �к��Ѵ����ª����ŧ ]";
			// 
			// button3
			// 
			this.button3.BackColor = System.Drawing.Color.DimGray;
			this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button3.ForeColor = System.Drawing.Color.White;
			this.button3.Location = new System.Drawing.Point(184, 20);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(28, 20);
			this.button3.TabIndex = 90;
			this.button3.Text = "...";
			this.button3.Click += new System.EventHandler(this.button3_Click);
			// 
			// button4
			// 
			this.button4.BackColor = System.Drawing.Color.DimGray;
			this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button4.ForeColor = System.Drawing.Color.White;
			this.button4.Location = new System.Drawing.Point(660, 112);
			this.button4.Name = "button4";
			this.button4.Size = new System.Drawing.Size(68, 20);
			this.button4.TabIndex = 101;
			this.button4.Text = "¡��ԡ";
			this.button4.Click += new System.EventHandler(this.button4_Click);
			// 
			// progressBar1
			// 
			this.progressBar1.Location = new System.Drawing.Point(64, 240);
			this.progressBar1.Name = "progressBar1";
			this.progressBar1.Size = new System.Drawing.Size(676, 12);
			this.progressBar1.Step = 1;
			this.progressBar1.TabIndex = 102;
			this.progressBar1.Visible = false;
			// 
			// timer1
			// 
			this.timer1.Interval = 1;
			this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
			// 
			// textBox6
			// 
			this.textBox6.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(76)), ((System.Byte)(75)), ((System.Byte)(54)));
			this.textBox6.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.textBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold);
			this.textBox6.ForeColor = System.Drawing.Color.LightCyan;
			this.textBox6.Location = new System.Drawing.Point(48, 8);
			this.textBox6.Name = "textBox6";
			this.textBox6.ReadOnly = true;
			this.textBox6.Size = new System.Drawing.Size(144, 31);
			this.textBox6.TabIndex = 106;
			this.textBox6.TabStop = false;
			this.textBox6.Text = "�Ѵ����ª���";
			// 
			// label10
			// 
			this.label10.BackColor = System.Drawing.Color.Transparent;
			this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(222)));
			this.label10.ForeColor = System.Drawing.Color.Red;
			this.label10.Location = new System.Drawing.Point(772, 24);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(12, 12);
			this.label10.TabIndex = 107;
			this.label10.Text = "X";
			this.label10.Click += new System.EventHandler(this.label10_Click);
			// 
			// Convert2Book
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(76)), ((System.Byte)(75)), ((System.Byte)(54)));
			this.ClientSize = new System.Drawing.Size(800, 264);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.label10,
																		  this.textBox6,
																		  this.progressBar1,
																		  this.button4,
																		  this.groupBox3,
																		  this.button2,
																		  this.button1,
																		  this.listView1});
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.KeyPreview = true;
			this.Name = "Convert2Book";
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.Text = "Convert2Book";
			this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.onKeyDown);
			this.Closing += new System.ComponentModel.CancelEventHandler(this.onClosing);
			this.Load += new System.EventHandler(this.Convert2Book_Load);
			this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.onKeyUp);
			this.groupBox3.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void button4_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}

		private void button2_Click(object sender, System.EventArgs e)
		{
			for (int x = 0; x < listView1.Items.Count; x++)
			{
				listView1.Items[x].Checked = true;
			}
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			if (textBox2.Text == "")
			{
				MessageBox.Show("��سҵ�駪���������ͺѹ�֡","����͹",MessageBoxButtons.OK,MessageBoxIcon.Information);
				textBox2.Focus();
				return;
			}

			// progress bar
			progressBar1.Value = 0;
			progressBar1.Maximum = listView1.Items.Count;
			progressBar1.Visible = true;
			
			System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;

			StreamWriter sw = new StreamWriter(textBox2.Text,false,System.Text.Encoding.GetEncoding(874));
			sw.WriteLine("��ª����ŧ Nueng Karaoke");
			sw.WriteLine("=====================");
			sw.WriteLine("");

			for (int x = 0; x < listView1.Items.Count; x++)
			{
				if (listView1.Items[x].Checked)
				{
					string strReturn = "";
					strReturn += listView1.Items[x].SubItems[1].Text + "\t"; //No
					strReturn += listView1.Items[x].SubItems[2].Text + "\t"; //SongName
					strReturn += listView1.Items[x].SubItems[3].Text + "\t"; //ArtistName
					strReturn += listView1.Items[x].SubItems[4].Text + "\t"; //AlbumName
					sw.WriteLine(strReturn);
				}

				// refresh progress bar
				progressBar1.Increment(1);
				progressBar1.Refresh();
			}
			sw.Close();

			progressBar1.Visible = false;
			System.Windows.Forms.Cursor.Current = Cursors.Default;
			MessageBox.Show("�ѹ�֡���º��������..","Success..",MessageBoxButtons.OK,MessageBoxIcon.Information);
		}

		private void Convert2Book_Load(object sender, System.EventArgs e)
		{
			// progress bar
			progressBar1.Value = 0;
			progressBar1.Maximum = MainForm.m_hashSongIndex.Count;
			progressBar1.Visible = true;

			// ��Ҥ�Ң����Ũҡ mem ���� list
			listView1.Items.Clear();

			m_Count = 0;
			idic = MainForm.m_hashSongIndex.GetEnumerator();
			idic.Reset();

			timer1.Enabled = true;
		}

		private void button3_Click(object sender, System.EventArgs e)
		{
			SaveFileDialog sf = new SaveFileDialog();
			if (DialogResult.Cancel != sf.ShowDialog())
			{
				textBox2.Text = sf.FileName;
			}
		}

		private void onKeyUp(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			switch(e.KeyData)
			{
				case Keys.Escape:
				case Keys.F8:
				{
					this.Close();
					break;
				}
			}
		}

		private void onColumnClick(object sender, System.Windows.Forms.ColumnClickEventArgs e)
		{
			ListView myListView = (ListView)sender;

			// Determine if clicked column is already the column that is being sorted.
			if ( e.Column == lvwColumnSorter.SortColumn )
			{
				// Reverse the current sort direction for this column.
				if (lvwColumnSorter.Order == SortOrder.Ascending)
				{
					lvwColumnSorter.Order = SortOrder.Descending;
				}
				else
				{
					lvwColumnSorter.Order = SortOrder.Ascending;
				}
			}
			else
			{
				// Set the column number that is to be sorted; default to ascending.
				lvwColumnSorter.SortColumn = e.Column;
				lvwColumnSorter.Order = SortOrder.Ascending;
			}

			// Perform the sort with these new sort options.
			myListView.Sort();
		}

		private void onClosing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			this.timer1.Enabled = false;
		}

		private void timer1_Tick(object sender, System.EventArgs e)
		{
			if (idic == null)
			{
				return;
			}

			timer1.Stop();

			// cursor
			System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;

			
			int divideChk = 0;
			if (MainForm.m_hashSongIndex.Count / 2 > 0)
			{
                // ¡��ԡ����觤�����Ŵ 24 �.�. 2550 
                //(MainForm.m_hashSongIndex.Count) / 2;
                divideChk = MainForm.m_hashSongIndex.Count;
			}
			else
			{
				return;
			}
			int count = 0; // �ͺ�� 100 ��¡��
			ListViewItem [] lvArr = new ListViewItem[divideChk];

//			while(count < divideChk)
//			{
				while(idic.MoveNext())
				{
			
					m_Count++;
					SongStructure ss = (SongStructure)idic.Value;
					lvArr[count] = new ListViewItem(new string[]{string.Format("{0}",m_Count)
																	,ss.m_strNo
																	,ss.m_strSongName
																	,ss.m_strArtistName
																	,ss.m_strAlbumName
																});
					// refresh progress bar
					progressBar1.Increment(1);
					progressBar1.Refresh();
					if (++count < divideChk){continue;}
					else{break;}
				}
//			}

			try
			{
				listView1.Items.AddRange(lvArr);
			}
			catch(System.NullReferenceException exnull)
			{
				// progressbar and cursor
				progressBar1.Visible = false;
				System.Windows.Forms.Cursor.Current = Cursors.Default;
				MessageBox.Show("��Ŵ��ª������º��������","����͹..",MessageBoxButtons.OK,MessageBoxIcon.Information);
				return;
			}

			// ����ѧ����������������Ŵ�ա����
			if (listView1.Items.Count < MainForm.m_hashSongIndex.Count)
			{
				timer1.Start();
			}
			else
			{
				// progressbar and cursor
				progressBar1.Visible = false;
				System.Windows.Forms.Cursor.Current = Cursors.Default;
				MessageBox.Show("��Ŵ��ª������º��������","����͹..",MessageBoxButtons.OK,MessageBoxIcon.Information);
			}
		}

		private void label10_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}

		private void onKeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			if (e.KeyData == Keys.Escape ||
				e.KeyData == Keys.F1 ||
				e.KeyData == Keys.F2 ||				
				e.KeyData == Keys.F3 ||				

				e.KeyData == Keys.F5 ||				
				e.KeyData == Keys.F6 ||
				e.KeyData == Keys.F7 ||
				// e.KeyData == Keys.F8 || // Menu ������˹�Դ ��һ�������͡
				e.KeyData == Keys.F9 ||
				e.KeyData == Keys.F10)
			{
				this.Close();
			}		
		}
	}
}
