using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using System.IO;

namespace MyMP3_X1
{
	/// <summary>
	/// Summary description for MCIControl.
	/// </summary>
	public class MCIControl : System.Windows.Forms.UserControl
	{
		int m_iOwnerSpeed = 0;
		int m_iOwnerVolume = 0;

		MCI m_mci = null;
		string m_strFilename = "";

		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.Button button5;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.TrackBar trackBar3;
		private System.Windows.Forms.TrackBar trackBar2;
		private System.Windows.Forms.TrackBar trackBar1;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public MCIControl()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();
			
		}

		public void initMCI(int ino)
		{
			m_mci = new MCI(ino);			
			m_mci.PositionChanged += new PositionEventHandler(Position_Tick);
			this.groupBox1.Text += ino.ToString();
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.button3 = new System.Windows.Forms.Button();
			this.button5 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.trackBar3 = new System.Windows.Forms.TrackBar();
			this.trackBar2 = new System.Windows.Forms.TrackBar();
			this.trackBar1 = new System.Windows.Forms.TrackBar();
			this.label5 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.trackBar3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.trackBar2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.AddRange(new System.Windows.Forms.Control[] {
																					this.button3,
																					this.button5,
																					this.button2,
																					this.trackBar3,
																					this.trackBar2,
																					this.trackBar1,
																					this.label5,
																					this.label4,
																					this.label3,
																					this.label2,
																					this.label1});
			this.groupBox1.ForeColor = System.Drawing.Color.Aqua;
			this.groupBox1.Location = new System.Drawing.Point(8, 8);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(344, 184);
			this.groupBox1.TabIndex = 1;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "����ͧ��蹷�� ";
			// 
			// button3
			// 
			this.button3.Location = new System.Drawing.Point(96, 152);
			this.button3.Name = "button3";
			this.button3.TabIndex = 5;
			this.button3.Text = "Play";
			this.button3.Click += new System.EventHandler(this.button3_Click);
			// 
			// button5
			// 
			this.button5.Location = new System.Drawing.Point(256, 152);
			this.button5.Name = "button5";
			this.button5.TabIndex = 15;
			this.button5.Text = "Next";
			this.button5.Click += new System.EventHandler(this.button5_Click);
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(176, 152);
			this.button2.Name = "button2";
			this.button2.TabIndex = 10;
			this.button2.Text = "Stop";
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// trackBar3
			// 
			this.trackBar3.LargeChange = 50;
			this.trackBar3.Location = new System.Drawing.Point(96, 120);
			this.trackBar3.Name = "trackBar3";
			this.trackBar3.Size = new System.Drawing.Size(240, 45);
			this.trackBar3.SmallChange = 50;
			this.trackBar3.TabIndex = 30;
			this.trackBar3.TickFrequency = 100;
			this.trackBar3.TickStyle = System.Windows.Forms.TickStyle.None;
			this.trackBar3.Scroll += new System.EventHandler(this.onPositionScroll);
			// 
			// trackBar2
			// 
			this.trackBar2.LargeChange = 50;
			this.trackBar2.Location = new System.Drawing.Point(96, 88);
			this.trackBar2.Maximum = 1000;
			this.trackBar2.Name = "trackBar2";
			this.trackBar2.Size = new System.Drawing.Size(240, 45);
			this.trackBar2.SmallChange = 50;
			this.trackBar2.TabIndex = 25;
			this.trackBar2.TickFrequency = 100;
			this.trackBar2.TickStyle = System.Windows.Forms.TickStyle.None;
			this.trackBar2.Scroll += new System.EventHandler(this.onVolumeScroll);
			// 
			// trackBar1
			// 
			this.trackBar1.LargeChange = 50;
			this.trackBar1.Location = new System.Drawing.Point(96, 56);
			this.trackBar1.Maximum = 2000;
			this.trackBar1.Name = "trackBar1";
			this.trackBar1.Size = new System.Drawing.Size(240, 45);
			this.trackBar1.SmallChange = 50;
			this.trackBar1.TabIndex = 20;
			this.trackBar1.TickFrequency = 1000;
			this.trackBar1.Value = 1000;
			this.trackBar1.Scroll += new System.EventHandler(this.onSpeedScroll);
			// 
			// label5
			// 
			this.label5.ForeColor = System.Drawing.Color.White;
			this.label5.Location = new System.Drawing.Point(96, 24);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(232, 23);
			this.label5.TabIndex = 9;
			this.label5.Text = "�ŧ�Ѩ�غѹ";
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(24, 24);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(64, 23);
			this.label4.TabIndex = 8;
			this.label4.Text = "�ŧ�Ѩ�غѹ";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(24, 120);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(64, 23);
			this.label3.TabIndex = 2;
			this.label3.Text = "���˹�";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(24, 88);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(64, 23);
			this.label2.TabIndex = 1;
			this.label2.Text = "�����ѧ���§";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(24, 56);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(64, 23);
			this.label1.TabIndex = 0;
			this.label1.Text = "��������";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// MCIControl
			// 
			this.BackColor = System.Drawing.Color.DarkBlue;
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.groupBox1});
			this.Name = "MCIControl";
			this.Size = new System.Drawing.Size(360, 200);
			this.groupBox1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.trackBar3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.trackBar2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		/////
		/// My code here
		/// 
		private void Position_Tick(object sender, PositionChangedEventArgs e)
		{
			this.trackBar3.Value = e.newPosition;
		}

		public void setMixerTitle(string strName)
		{
			this.groupBox1.Text = strName;
		}

		public void setSongName(string strName)
		{
			label5.Text = strName;
		}

		public void setFilename(string strFilename)
		{
			m_strFilename = strFilename;
		}

		private void button3_Click(object sender, System.EventArgs e)
		{
			// play
			//test
			m_strFilename = @"d:\mp3\01.mp3";

			m_mci.OGG_Free();
			if (File.Exists(m_strFilename))
			{
				m_mci.OGG_Load(m_strFilename);
				m_mci.OGG_Play();
				this.trackBar3.Maximum = m_mci.OGG_GetLength();
				this.trackBar2.Value = m_mci.OGG_Volume;
			}
		}

		private void button2_Click(object sender, System.EventArgs e)
		{
			// stop
			// ���ͨ� fade �ç���
			//m_mci.OGG_Stop();

			// play
			//test
			m_strFilename = @"d:\mp3\02.mp3";

			m_mci.OGG_Free();
			if (File.Exists(m_strFilename))
			{
				m_mci.OGG_Load(m_strFilename);
				m_mci.OGG_Play();
				this.trackBar3.Maximum = m_mci.OGG_GetLength();
				this.trackBar2.Value = m_mci.OGG_Volume;				
			}
		}

		private void button5_Click(object sender, System.EventArgs e)
		{
			// next

			// ���ͨ� fade �ç���
			m_mci.OGG_Free();

			// ���ѭ�ҳ仺͡���� �����ŧ����
		}

		public void decVolume(int vol)
		{
			if (m_mci.OGG_IsPlaying() == 1)
			{
				if (vol > m_mci.OGG_GetVolume())
				{
					m_mci.OGG_SetVolume(0);
					trackBar2.Value = m_mci.OGG_GetVolume();
				}
				else
				{				
					m_mci.OGG_SetVolume(m_mci.OGG_GetVolume() - vol);
					trackBar2.Value = m_mci.OGG_GetVolume();
				}
			}
		}

		public void incVolume(int vol)
		{
			if (m_mci.OGG_IsPlaying() == 1)
			{
				m_mci.OGG_SetVolume(m_mci.OGG_GetVolume() + vol);
				trackBar2.Value = m_mci.OGG_GetVolume();
			}
		}

		private void onSpeedScroll(object sender, System.EventArgs e)
		{
			m_mci.OGG_Speed = this.trackBar1.Value;
		}

		private void onVolumeScroll(object sender, System.EventArgs e)
		{
			m_mci.OGG_Volume = this.trackBar2.Value;
		}

		private void onPositionScroll(object sender, System.EventArgs e)
		{		
			m_mci.OGG_Position = this.trackBar3.Value;
		}
	}
}
