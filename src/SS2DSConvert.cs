using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Windows.Forms;
using System.Collections;

namespace MyMP3_X1
{
	/// <summary>
	/// Summary description for SS2DSConvert.
	/// </summary>
	public class SS2DSConvert
	{
		DataSet m_ds = new DataSet();

		public SS2DSConvert(Hashtable hash)
		{
			MakeDataSetAndConvert(hash);
		}

		private void MakeDataSetAndConvert(Hashtable hash)
		{
			DataTable myDataTable = new DataTable("SongStructureSet");
			// Declare DataColumn and DataRow variables.
			DataColumn myColumn;
			DataRow myRow;
			
			// Create new DataColumn, set DataType, ColumnName and add to DataTable.    
			myColumn = new DataColumn();
			myColumn.DataType = System.Type.GetType("System.String");
			myColumn.ColumnName = "m_strNo";
			myColumn.Unique = true;
			myDataTable.Columns.Add(myColumn);

			// Create m_strSongName column.
			myColumn = new DataColumn();
			myColumn.DataType = System.Type.GetType("System.String");
			myColumn.ColumnName = "m_strSongName";
			myDataTable.Columns.Add(myColumn);

			//
			// Create m_strArtistName
			myColumn = new DataColumn();
			myColumn.DataType = System.Type.GetType("System.String");
			myColumn.ColumnName = "m_strArtistName";
			myDataTable.Columns.Add(myColumn);

			// m_strAlbumName = "";
			myColumn = new DataColumn();
			myColumn.DataType = System.Type.GetType("System.String");
			myColumn.ColumnName = "m_strAlbumName";
			myDataTable.Columns.Add(myColumn);

			// m_strFileName = "";
			myColumn = new DataColumn();
			myColumn.DataType = System.Type.GetType("System.String");
			myColumn.ColumnName = "m_strFileName";
			myDataTable.Columns.Add(myColumn);

			// m_strTimejump = "";
			myColumn = new DataColumn();
			myColumn.DataType = System.Type.GetType("System.String");
			myColumn.ColumnName = "m_strTimejump";
			myDataTable.Columns.Add(myColumn);

			// m_strTimeEnd = "";
			myColumn = new DataColumn();
			myColumn.DataType = System.Type.GetType("System.String");
			myColumn.ColumnName = "m_strTimeEnd";
			myDataTable.Columns.Add(myColumn);

			// m_strFilePlayer = "";
			myColumn = new DataColumn();
			myColumn.DataType = System.Type.GetType("System.String");
			myColumn.ColumnName = "m_strFilePlayer";
			myDataTable.Columns.Add(myColumn);

			// m_strEct1 = "";
			myColumn = new DataColumn();
			myColumn.DataType = System.Type.GetType("System.String");
			myColumn.ColumnName = "m_strEct1";
			myDataTable.Columns.Add(myColumn);

			// m_strEct2 = "";
			myColumn = new DataColumn();
			myColumn.DataType = System.Type.GetType("System.String");
			myColumn.ColumnName = "m_strEct2";
			myDataTable.Columns.Add(myColumn);

			// m_strEct3 = "";
			myColumn = new DataColumn();
			myColumn.DataType = System.Type.GetType("System.String");
			myColumn.ColumnName = "m_strEct3";
			myDataTable.Columns.Add(myColumn);

			// m_strEct4 = "";
			myColumn = new DataColumn();
			myColumn.DataType = System.Type.GetType("System.String");
			myColumn.ColumnName = "m_strEct4";
			myDataTable.Columns.Add(myColumn);

			// m_strEct5 = "";
			myColumn = new DataColumn();
			myColumn.DataType = System.Type.GetType("System.String");
			myColumn.ColumnName = "m_strEct5";
			myDataTable.Columns.Add(myColumn);

			// Create new DataRow objects and add to DataTable.
			// ��Ң����Ũҡ hashtable ��ҵç���
			
			System.Collections.IDictionaryEnumerator idic = hash.GetEnumerator();
			idic.Reset();

			if (idic == null)
			{
				return;
			}
		
			// cursor
			// System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;

			while(idic.MoveNext())
			{	
				SongStructure ss = (SongStructure)idic.Value;
				myRow = myDataTable.NewRow();
				myRow["m_strNo"] = ss.m_strNo == null ? "" : ss.m_strNo.Trim();
				myRow["m_strSongName"] = ss.m_strSongName == null ? "" : ss.m_strSongName.Trim();
				myRow["m_strArtistName"] = ss.m_strArtistName == null ? "" : ss.m_strArtistName.Trim();
				myRow["m_strAlbumName"] = ss.m_strAlbumName == null ? "" : ss.m_strAlbumName.Trim();
				myRow["m_strFileName"] = ss.m_strFileName == null ? "" : ss.m_strFileName.Trim();
				myRow["m_strTimejump"] = ss.m_strTimejump == null ? "" : ss.m_strTimejump.Trim();
				myRow["m_strTimeEnd"] = ss.m_strTimeEnd == null ? "" : ss.m_strTimeEnd.Trim();
				myRow["m_strFilePlayer"] = ss.m_strFilePlayer == null ? "" : ss.m_strFilePlayer.Trim();
				myRow["m_strEct1"] = ss.m_strEct1 == null ? "" : ss.m_strEct1.Trim();
				myRow["m_strEct2"] = ss.m_strEct2 == null ? "" : ss.m_strEct2.Trim();
				myRow["m_strEct3"] = ss.m_strEct3 == null ? "" : ss.m_strEct3.Trim();
				myRow["m_strEct4"] = ss.m_strEct4 == null ? "" : ss.m_strEct4.Trim();
				myRow["m_strEct5"] = ss.m_strEct5 == null ? "" : ss.m_strEct5.Trim();
				myDataTable.Rows.Add(myRow);
			}
			
			m_ds.Tables.Add(myDataTable);

			// ��ǹ��ä���
			//DataRow []dr = m_ds.Tables[0].Select("m_strNo > ''","m_strNo Asc,m_strArtistName Desc");
			//int x = dr.Length;

			
		
			//			// Create a DataView using the DataTable.
			//			myDataView = new DataView(myDataTable);
			//			
			//			// Set a DataGrid control's DataSource to the DataView.
			//			dataGrid.DataSource = myDataView;
		}

		public DataRow[] select(string strExp,string strSorted)
		{
			return m_ds.Tables[0].Select(strExp,strSorted);
		}
	}
}
