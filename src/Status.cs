using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace MyMP3_X1
{
	/// <summary>
	/// Summary description for Status.
	/// </summary>
	public class Status : System.Windows.Forms.Form
	{
		private string m_strTxt = "";
		private System.Windows.Forms.Timer timer1;
		private System.ComponentModel.IContainer components;

		public Status()
		{
			InitializeComponent();
			this.TransparencyKey = System.Drawing.Color.FromArgb(((System.Byte)(76)), ((System.Byte)(75)), ((System.Byte)(54)));
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.timer1 = new System.Windows.Forms.Timer(this.components);
			// 
			// timer1
			// 
			this.timer1.Enabled = true;
			this.timer1.Interval = 7000;
			this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
			// 
			// Status
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(76)), ((System.Byte)(75)), ((System.Byte)(54)));
			this.ClientSize = new System.Drawing.Size(216, 56);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Location = new System.Drawing.Point(50, 25);
			this.Name = "Status";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.Text = "Status";
			this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.onKeyDown);
			this.Closing += new System.ComponentModel.CancelEventHandler(this.onClosing);
			this.Load += new System.EventHandler(this.Status_Load);
			this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.onKeyUp);
			this.Paint += new System.Windows.Forms.PaintEventHandler(this.onPaint);

		}
		#endregion

		private void changeTxt(string strTxt)
		{
			m_strTxt = strTxt;
			Invalidate();
		}

		public void setTxtStatus(string strTxt)
		{
			this.timer1.Stop();
			this.timer1.Start();
			//label2.Text = strTxt;
			changeTxt(strTxt);
            if (!this.Modal)
            {
                this.ShowDialog();
            }
		}
		
		private void onClosing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			//((MainForm)(this.Owner)).m_strKey = "";
			this.timer1.Enabled = false;
			((MainForm)(this.Owner)).m_bStatusShowDialog = false;
		}

		private void onKeyUp(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			
		}

		private void onKeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			if (e.KeyData == Keys.Escape ||
				e.KeyData == Keys.F1 ||
				e.KeyData == Keys.F2 ||				
				e.KeyData == Keys.F3 ||				

				e.KeyData == Keys.F5 ||				
				e.KeyData == Keys.F6 ||
				e.KeyData == Keys.F7 ||
				e.KeyData == Keys.F8 ||
				e.KeyData == Keys.F9 ||
				e.KeyData == Keys.F10)
			{
				this.Close();
                System.Diagnostics.Debug.Print("status::Keydown->hide");
			}
			else
			{
				((MainForm)(this.Owner)).onKeyUp(null,e);
			}			
		}

		private void timer1_Tick(object sender, System.EventArgs e)
		{
			this.Hide();
            System.Diagnostics.Debug.Print("status::Timer->hide");
		}

		private void onPaint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			Graphics g = e.Graphics;
			if (!m_strTxt.Trim().StartsWith("���§"))
			{
				Font font = new System.Drawing.Font("IrisUPC", 32F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(222)));
				SolidBrush drawBrush = new SolidBrush(Color.Black);
				g.DrawString(m_strTxt,font,drawBrush,1.0f,1.0f);

				drawBrush = new SolidBrush(Color.White);
				g.DrawString(m_strTxt,font,drawBrush,4.0f,4.0f);
			}
			else
			{
				m_strTxt = m_strTxt.Trim().Replace("���§","");
				try
				{
					int volLevel = Convert.ToInt32(m_strTxt);
					drawBar(g,volLevel);
				}
				catch(Exception ex)
				{
				}
			}
		}

		private void drawBar(Graphics g,int count)
		{
			if (count < 0)
			{
				count = 0;
			}
			else if (count > 13)
			{
				count = 13;
			}

			SolidBrush drawBrush = new SolidBrush(Color.Aqua);
			SolidBrush delBrush = new SolidBrush(this.BackColor);
			Pen pen = new Pen(drawBrush);
			for (int x = 0; x < count; x++)
			{
				g.FillRectangle(drawBrush,x*15+15,10,10,36);
				g.FillRectangle(delBrush,x*15+15,10,10,36 - (x*3+3)); // ź
			}			
		}

		private void Status_Load(object sender, System.EventArgs e)
		{
			this.Size = new Size(216, 56);
		}

	}
}
