//#define __AR_JIN__

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.IO;
using System.Management;
using System.Web.Security;

namespace MyMP3_X1
{
	/// <summary>
	/// Summary description for CopyUtil.
	/// </summary>
	public class CopyUtil : System.Windows.Forms.Form
	{
//		string m_strVersion = "";

		//private string m_strSongPath = "";
//		private string m_strFormatAlbum = "";
//		private string m_strSongSource = "";
//		private bool m_bSetTime = false;
//		private string []m_strArrSongPath;

		private int m_iListViewSelected = -1; // ����Ң�й�����͡ index ����� listview
		private const int _FREE_MINIMUM_DISK = 500000;

		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.Button button8;
		private System.Windows.Forms.TextBox textBox2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.NumericUpDown numericUpDown1;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox textBox3;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox textBox4;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.NumericUpDown numericUpDown2;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.TextBox textBox5;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.NumericUpDown numericUpDown3;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.ListView listView1;
		private System.Windows.Forms.ColumnHeader columnHeader1;
		private System.Windows.Forms.ColumnHeader columnHeader2;
		private System.Windows.Forms.ColumnHeader columnHeader3;
		private System.Windows.Forms.ColumnHeader columnHeader4;
		private System.Windows.Forms.ColumnHeader columnHeader5;
		private System.Windows.Forms.ColumnHeader columnHeader6;
		private System.Windows.Forms.ColumnHeader columnHeader7;
		private System.Windows.Forms.ColumnHeader columnHeader8;
		private System.Windows.Forms.ProgressBar progressBar1;
		private System.Windows.Forms.TextBox textBox6;
		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public CopyUtil()
		{
//			MainForm.m_strVersion = MainForm.m_strVersion;

//			int icount = 0;
//			for (int x = 0; x < strArrSongPath.Length; x++)
//			{
//				if (strArrSongPath[x] != "") icount++;
//			}
//			
//			this.m_strArrSongPath = new string[icount];
//			for (int x = 0; x < strArrSongPath.Length; x++)
//			{
//				if (strArrSongPath[x] != "")
//				{
//					m_strArrSongPath[x] = strArrSongPath[x];
//				}
//			}

//			this.m_strSongSource = strSongSource;
//			for (int x = 0; x < iSongLength-2; x++)
//			{
//				m_strFormatAlbum += "0";
//			}
			InitializeComponent();
			this.TransparencyKey = System.Drawing.Color.FromArgb(((System.Byte)(76)), ((System.Byte)(75)), ((System.Byte)(54)));

//			#if (__AR_JIN__)
//				checkBox1.Visible = true;
//			#else
//				checkBox1.Visible = false;
//			#endif
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.label9 = new System.Windows.Forms.Label();
			this.numericUpDown3 = new System.Windows.Forms.NumericUpDown();
			this.textBox5 = new System.Windows.Forms.TextBox();
			this.label8 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.textBox4 = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.textBox3 = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
			this.textBox2 = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.button8 = new System.Windows.Forms.Button();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.numericUpDown2 = new System.Windows.Forms.NumericUpDown();
			this.button1 = new System.Windows.Forms.Button();
			this.label10 = new System.Windows.Forms.Label();
			this.listView1 = new System.Windows.Forms.ListView();
			this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader4 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader5 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader6 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader7 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader8 = new System.Windows.Forms.ColumnHeader();
			this.progressBar1 = new System.Windows.Forms.ProgressBar();
			this.textBox6 = new System.Windows.Forms.TextBox();
			this.groupBox3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).BeginInit();
			this.SuspendLayout();
			// 
			// groupBox3
			// 
			this.groupBox3.BackColor = System.Drawing.Color.Transparent;
			this.groupBox3.Controls.AddRange(new System.Windows.Forms.Control[] {
																					this.label9,
																					this.numericUpDown3,
																					this.textBox5,
																					this.label8,
																					this.label6,
																					this.textBox4,
																					this.label5,
																					this.textBox3,
																					this.label4,
																					this.numericUpDown1,
																					this.textBox2,
																					this.label3,
																					this.button8,
																					this.textBox1,
																					this.label1,
																					this.label7,
																					this.numericUpDown2});
			this.groupBox3.ForeColor = System.Drawing.Color.Yellow;
			this.groupBox3.Location = new System.Drawing.Point(564, 40);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(220, 188);
			this.groupBox3.TabIndex = 38;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "[ �к��Ѵ�����ŧ ]";
			// 
			// label9
			// 
			this.label9.ForeColor = System.Drawing.Color.FloralWhite;
			this.label9.Location = new System.Drawing.Point(124, 92);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(24, 16);
			this.label9.TabIndex = 96;
			this.label9.Text = "��ش";
			this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// numericUpDown3
			// 
			this.numericUpDown3.Location = new System.Drawing.Point(152, 88);
			this.numericUpDown3.Maximum = new System.Decimal(new int[] {
																		   999999,
																		   0,
																		   0,
																		   0});
			this.numericUpDown3.Name = "numericUpDown3";
			this.numericUpDown3.Size = new System.Drawing.Size(60, 20);
			this.numericUpDown3.TabIndex = 95;
			this.numericUpDown3.TextChanged += new System.EventHandler(this.onTextChanged);
			// 
			// textBox5
			// 
			this.textBox5.BackColor = System.Drawing.Color.LightCyan;
			this.textBox5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.textBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.textBox5.Location = new System.Drawing.Point(60, 40);
			this.textBox5.Name = "textBox5";
			this.textBox5.Size = new System.Drawing.Size(152, 20);
			this.textBox5.TabIndex = 12;
			this.textBox5.Text = "d,c";
			// 
			// label8
			// 
			this.label8.ForeColor = System.Drawing.Color.FloralWhite;
			this.label8.Location = new System.Drawing.Point(4, 44);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(52, 16);
			this.label8.TabIndex = 94;
			this.label8.Text = "�����";
			this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label6
			// 
			this.label6.ForeColor = System.Drawing.Color.FloralWhite;
			this.label6.Location = new System.Drawing.Point(4, 164);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(52, 16);
			this.label6.TabIndex = 90;
			this.label6.Text = "��ź��";
			this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// textBox4
			// 
			this.textBox4.BackColor = System.Drawing.Color.LightCyan;
			this.textBox4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.textBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.textBox4.Location = new System.Drawing.Point(60, 160);
			this.textBox4.Name = "textBox4";
			this.textBox4.Size = new System.Drawing.Size(152, 20);
			this.textBox4.TabIndex = 30;
			this.textBox4.Text = "";
			this.textBox4.TextChanged += new System.EventHandler(this.onTextChanged);
			// 
			// label5
			// 
			this.label5.ForeColor = System.Drawing.Color.FloralWhite;
			this.label5.Location = new System.Drawing.Point(4, 140);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(52, 16);
			this.label5.TabIndex = 88;
			this.label5.Text = "��ŻԹ";
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// textBox3
			// 
			this.textBox3.BackColor = System.Drawing.Color.LightCyan;
			this.textBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.textBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.textBox3.Location = new System.Drawing.Point(60, 136);
			this.textBox3.Name = "textBox3";
			this.textBox3.Size = new System.Drawing.Size(152, 20);
			this.textBox3.TabIndex = 25;
			this.textBox3.Text = "";
			this.textBox3.TextChanged += new System.EventHandler(this.onTextChanged);
			// 
			// label4
			// 
			this.label4.ForeColor = System.Drawing.Color.FloralWhite;
			this.label4.Location = new System.Drawing.Point(4, 116);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(52, 16);
			this.label4.TabIndex = 86;
			this.label4.Text = "�����ŧ";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// numericUpDown1
			// 
			this.numericUpDown1.Location = new System.Drawing.Point(60, 64);
			this.numericUpDown1.Maximum = new System.Decimal(new int[] {
																		   999999,
																		   0,
																		   0,
																		   0});
			this.numericUpDown1.Name = "numericUpDown1";
			this.numericUpDown1.Size = new System.Drawing.Size(60, 20);
			this.numericUpDown1.TabIndex = 15;
			this.numericUpDown1.TextChanged += new System.EventHandler(this.onTextChanged);
			// 
			// textBox2
			// 
			this.textBox2.BackColor = System.Drawing.Color.LightCyan;
			this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.textBox2.Location = new System.Drawing.Point(60, 112);
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new System.Drawing.Size(152, 20);
			this.textBox2.TabIndex = 20;
			this.textBox2.Text = "";
			this.textBox2.TextChanged += new System.EventHandler(this.onTextChanged);
			// 
			// label3
			// 
			this.label3.ForeColor = System.Drawing.Color.FloralWhite;
			this.label3.Location = new System.Drawing.Point(4, 68);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(52, 16);
			this.label3.TabIndex = 83;
			this.label3.Text = "����";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// button8
			// 
			this.button8.BackColor = System.Drawing.Color.DimGray;
			this.button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button8.ForeColor = System.Drawing.Color.White;
			this.button8.Location = new System.Drawing.Point(184, 16);
			this.button8.Name = "button8";
			this.button8.Size = new System.Drawing.Size(28, 20);
			this.button8.TabIndex = 10;
			this.button8.Text = "...";
			this.button8.Click += new System.EventHandler(this.button8_Click);
			// 
			// textBox1
			// 
			this.textBox1.BackColor = System.Drawing.Color.LightCyan;
			this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.textBox1.Location = new System.Drawing.Point(60, 16);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(120, 20);
			this.textBox1.TabIndex = 5;
			this.textBox1.Text = "d:\\nueng";
			// 
			// label1
			// 
			this.label1.ForeColor = System.Drawing.Color.FloralWhite;
			this.label1.Location = new System.Drawing.Point(4, 20);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(52, 16);
			this.label1.TabIndex = 37;
			this.label1.Text = "���Ҩҡ";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label7
			// 
			this.label7.ForeColor = System.Drawing.Color.FloralWhite;
			this.label7.Location = new System.Drawing.Point(4, 92);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(52, 16);
			this.label7.TabIndex = 92;
			this.label7.Text = "����";
			this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// numericUpDown2
			// 
			this.numericUpDown2.Location = new System.Drawing.Point(60, 88);
			this.numericUpDown2.Maximum = new System.Decimal(new int[] {
																		   999999,
																		   0,
																		   0,
																		   0});
			this.numericUpDown2.Name = "numericUpDown2";
			this.numericUpDown2.Size = new System.Drawing.Size(60, 20);
			this.numericUpDown2.TabIndex = 35;
			this.numericUpDown2.TextChanged += new System.EventHandler(this.onTextChanged);
			// 
			// button1
			// 
			this.button1.BackColor = System.Drawing.Color.DimGray;
			this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button1.ForeColor = System.Drawing.Color.White;
			this.button1.Location = new System.Drawing.Point(752, 236);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(28, 20);
			this.button1.TabIndex = 40;
			this.button1.Text = "C";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// label10
			// 
			this.label10.BackColor = System.Drawing.Color.Transparent;
			this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(222)));
			this.label10.ForeColor = System.Drawing.Color.Red;
			this.label10.Location = new System.Drawing.Point(772, 24);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(12, 12);
			this.label10.TabIndex = 46;
			this.label10.Text = "X";
			this.label10.Click += new System.EventHandler(this.onclick);
			// 
			// listView1
			// 
			this.listView1.BackColor = System.Drawing.Color.LightCyan;
			this.listView1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.listView1.CheckBoxes = true;
			this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
																						this.columnHeader1,
																						this.columnHeader2,
																						this.columnHeader3,
																						this.columnHeader4,
																						this.columnHeader5,
																						this.columnHeader6,
																						this.columnHeader7,
																						this.columnHeader8});
			this.listView1.ForeColor = System.Drawing.Color.Blue;
			this.listView1.FullRowSelect = true;
			this.listView1.HideSelection = false;
			this.listView1.Location = new System.Drawing.Point(16, 44);
			this.listView1.MultiSelect = false;
			this.listView1.Name = "listView1";
			this.listView1.Size = new System.Drawing.Size(540, 184);
			this.listView1.TabIndex = 48;
			this.listView1.View = System.Windows.Forms.View.Details;
			this.listView1.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.onColumnClick);
			this.listView1.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
			// 
			// columnHeader1
			// 
			this.columnHeader1.Text = "�ӴѺ���";
			this.columnHeader1.Width = 50;
			// 
			// columnHeader2
			// 
			this.columnHeader2.Text = "�������";
			this.columnHeader2.Width = 90;
			// 
			// columnHeader3
			// 
			this.columnHeader3.Text = "����";
			this.columnHeader3.Width = 50;
			// 
			// columnHeader4
			// 
			this.columnHeader4.Text = "�����ŧ";
			this.columnHeader4.Width = 90;
			// 
			// columnHeader5
			// 
			this.columnHeader5.Text = "������ŻԹ";
			this.columnHeader5.Width = 90;
			// 
			// columnHeader6
			// 
			this.columnHeader6.Text = "��ź��";
			this.columnHeader6.Width = 90;
			// 
			// columnHeader7
			// 
			this.columnHeader7.Text = "����";
			this.columnHeader7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.columnHeader7.Width = 35;
			// 
			// columnHeader8
			// 
			this.columnHeader8.Text = "��ش";
			this.columnHeader8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.columnHeader8.Width = 35;
			// 
			// progressBar1
			// 
			this.progressBar1.Location = new System.Drawing.Point(64, 240);
			this.progressBar1.Name = "progressBar1";
			this.progressBar1.Size = new System.Drawing.Size(676, 12);
			this.progressBar1.Step = 1;
			this.progressBar1.TabIndex = 49;
			this.progressBar1.Visible = false;
			// 
			// textBox6
			// 
			this.textBox6.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(76)), ((System.Byte)(75)), ((System.Byte)(54)));
			this.textBox6.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.textBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold);
			this.textBox6.ForeColor = System.Drawing.Color.LightCyan;
			this.textBox6.Location = new System.Drawing.Point(48, 8);
			this.textBox6.Name = "textBox6";
			this.textBox6.ReadOnly = true;
			this.textBox6.Size = new System.Drawing.Size(144, 31);
			this.textBox6.TabIndex = 50;
			this.textBox6.TabStop = false;
			this.textBox6.Text = "�����ŧ";
			// 
			// CopyUtil
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(76)), ((System.Byte)(75)), ((System.Byte)(54)));
			this.ClientSize = new System.Drawing.Size(800, 264);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.textBox6,
																		  this.progressBar1,
																		  this.listView1,
																		  this.label10,
																		  this.groupBox3,
																		  this.button1});
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.KeyPreview = true;
			this.Name = "CopyUtil";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.Text = "Copy Utility";
			this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.onKeydown);
			this.Load += new System.EventHandler(this.CopyUtil_Load);
			this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.onKeyUp);
			this.groupBox3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		/*[STAThread]
		static void Main() 
		{			
			try
			{
				Application.Run(new CopyUtil());
			}
			catch (Exception ee)
			{
				StreamWriter file;
				string strFileError = "error.log";
				if (File.Exists(strFileError))
				{
					file = File.AppendText(strFileError);
				}
				else
				{
					file = File.CreateText(strFileError);
				}
				file.WriteLine(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " Error.." + ee.Message);
				file.Close();
			}
		}*/
//
//		private void button11_Click(object sender, System.EventArgs e)
//		{
			// test complete
			// �ʴ��ŧ� cd
//			try
//			{				
//				button5.Enabled = false;
//
//				//textBox1.Text = m_strSongSource;
//				listView1.Items.Clear();
//				int y = 0;
//
//				// .dat
//				string []strFile = System.IO.Directory.GetFiles(m_strSongSource,"*.DAT");
//				for (int x = 0; x < strFile.Length; x++)
//				{
//					y = x+1;
//					System.Windows.Forms.ListViewItem lv = new ListViewItem(new string[]{Convert.ToString(x+1),strFile[x].Replace(m_strSongSource + "\\" ,""),y.ToString("00"),"0"});
//					listView1.Items.Add(lv);
//				}


//				������������ӴѺ�ŧ�Ҩ���ջѭ��
//				// .mp3
//				strFile = System.IO.Directory.GetFiles(m_strSongSource,"*.MP3");
//				for (int x = 0; x < strFile.Length; x++)
//				{
//					y = x+1;
//					System.Windows.Forms.ListViewItem lv = new ListViewItem(new string[]{Convert.ToString(x+1),strFile[x].Replace(m_strSongSource + "\\" ,""),y.ToString("00"),"0"});
//					listView1.Items.Add(lv);
//				}

//				if (listView1.Items.Count > 0) 
//				{
//					button2.Enabled = true;
//					listView1.Focus();
//					listView1.Items[0].Selected = true;
//					
//					m_bSetTime = true; // ����ա�á� bt4 ����繡�õ������
//				}
//			}
//			catch(Exception ex)
//			{
//				MessageBox.Show(ex.Message,"error",MessageBoxButtons.OK,MessageBoxIcon.Error);
//			}
//		}

		private void selectAll()
		{
			// test complete
			// ���͡/������͡ �ŧ������� list
			for (int x = 0; x < listView1.Items.Count; x++)
			{
				if(listView1.Items[x].Checked)
				{
					listView1.Items[x].Checked = false;
				}
				else
				{
					listView1.Items[x].Checked = true;
				}
			}
		}

//		private string removeLastDat(string str)
//		{
//			if (str.ToUpper().EndsWith(".DAT")) str = str.Substring(0,str.Length-4);
//			return str;
//		}

//		private void button5_Click(object sender, System.EventArgs e)
//		{
//			// complete
//			// ź�ŧ
//			/*if (textBox1.Text != m_strSongPath) // �����������ŧ�ҡ path �����
//			{
//				MessageBox.Show("����öź�ŧ����蹷�����͡��ҹ��","��س����͡�� CD ��͹");
//				return;
//			}*/
//			if (listView1.Items.Count <= 0) return;
//			
//			bool cmdExit = false;
//			int i = 0;
//			while ((!cmdExit) && ( i < listView1.Items.Count))
//			{
//				if (listView1.Items[i++].Checked)
//				{
//					cmdExit = true;
//				}
//			}
//
//			if (!cmdExit)
//			{
//				MessageBox.Show("����ա�����͡","����͹",MessageBoxButtons.OK,MessageBoxIcon.Information);
//				return;
//			}
//
//			try
//			{
//				if (DialogResult.Yes == MessageBox.Show("���������� ?","�׹�ѹ...",MessageBoxButtons.YesNo,MessageBoxIcon.Warning))
//				{
//					for (int x = 0; x < listView1.Items.Count; x++)
//					{
//						if (listView1.Items[x].Checked)
//						{
//							// delete .DAT & .DES
//							bool cmdExit2 = false;
//							int j = -1;
//							while ((++j < m_strArrSongPath.Length) && (!cmdExit2))
//							{							
//								string strPath = m_strArrSongPath[j] + "\\" + listView1.Items[x].SubItems[1].Text;
//								if (File.Exists(strPath))
//								{
//									File.Delete(strPath);
//									if (File.Exists(removeLastDat(strPath) + ".DES")) File.Delete(removeLastDat(strPath) + ".DES");
//									listView1.Items[x--].Remove();
//									cmdExit2 = true;
//								}								
//							}							
//						}
//					}
//					MessageBox.Show("ź�ŧ���º����","Success",MessageBoxButtons.OK,MessageBoxIcon.Information);
//				}
//				else
//				{
//					return;
//				}				
//			}
//			catch(Exception ex)
//			{
//				MessageBox.Show(ex.Message,"error",MessageBoxButtons.OK,MessageBoxIcon.Error);
//			}			
//		}

//		private void CopyUtil_Load(object sender, System.EventArgs e)
//		{
//			
//			// complete
//			// ������ŧ�Ѩ�غѹ
//			if (File.Exists("config.cfg"))
//			{
//				try
//				{
//					StreamReader sr = File.OpenText("config.cfg");
//					string strRead;
//					while ((strRead = sr.ReadLine()) != null)
//					{
//						string []strArr = strRead.Split('=');
//						if (strArr[0].Trim().ToUpper() == "SONG_SOURCE")
//						{
//							m_strSongSource = strArr[1].Trim();
//						}
//						else if (strArr[0].Trim().ToUpper() == "SONG_PATH")
//						{
//							m_strSongPath = strArr[1].Trim();
//						}
//					}
//					sr.Close();
//				}
//				catch(Exception ex)
//				{
//					MessageBox.Show(ex.Message,"Error.. while loading config.cfg",MessageBoxButtons.OK,MessageBoxIcon.Error);
//					return;
//				}
//			}
//			else
//			{
//				MessageBox.Show("��辺��� config.cfg ��س� setup �ա����","file not found !!!",MessageBoxButtons.OK,MessageBoxIcon.Error);
//				this.Close();
//			}
//			
//		}

//		private void button6_Click(object sender, System.EventArgs e)
//		{
//			// complete
//			// about
//			MessageBox.Show("Powered by \r\nwww.talaysoft.com","- About " + MainForm.m_strVersion + "-",MessageBoxButtons.OK,MessageBoxIcon.Information);
//		}
//
//		private void button3_Click(object sender, System.EventArgs e)
//		{
//			// complete
//			// exit
//			this.Close();
//		}
//
//		private void button9_Click(object sender, System.EventArgs e)
//		{
//			// ��Ѻ��
//			AskForm askSwapFrom = new AskForm("�ҡ��",999);
//
//			bool bCheckExist = true;
//			while (bCheckExist)
//			{
//				askSwapFrom.ShowDialog();
//				if (askSwapFrom.getOkButton())
//				{
//					// check ��������������
//					int j = -1;
//					int icount = 0;
//					bool cmdExitXX = false;
//					while ((++j < m_strArrSongPath.Length) && (!cmdExitXX))
//					{
//						string []strCheck = System.IO.Directory.GetFiles(m_strArrSongPath[j],askSwapFrom.getValue().ToString(m_strFormatAlbum) + "*.DAT");
//						if (strCheck.Length <= 0)
//						{
//							icount ++;
//							if (icount == m_strArrSongPath.Length)
//							{
//								MessageBox.Show("�ô�ͧ�����ա����","������蹷���к�",MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
//							}
//						}
//						else
//						{
//							cmdExitXX = true;
//							bCheckExist = false;
//						}						
//					}
//				}
//				else
//				{
//					return;
//				}
//			}
//
//			//
//			AskForm askSwapTo = new AskForm("����",999);
//			askSwapTo.ShowDialog();
//			if (!askSwapTo.getOkButton()) return;
//			
//			// ��������ǡѹ����� error
//			if (askSwapTo.getValue() == askSwapFrom.getValue())
//			{
//				MessageBox.Show("�蹵鹷ҧ��л��·ҧ�������ǡѹ..","�ô�ͧ�����ա����",MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
//				return;
//			}
//
//			// ź˹�Ҩ���������ͧ�ѹ��üԴ��Ҵ
//			listView1.Items.Clear();
//
//			// From
//			ArrayList arrList = new ArrayList();
//			string strFillter = askSwapFrom.getValue().ToString(m_strFormatAlbum) + "*.DAT";
//			for (int ic = 0; ic < m_strArrSongPath.Length; ic++)
//			{
//				string []strArrTmp = System.IO.Directory.GetFiles(m_strArrSongPath[ic],strFillter);
//				for (int l = 0; l < strArrTmp.Length; l++)
//				{
//					arrList.Add(strArrTmp[l]);
//				}
//			}
//			string []strFileFrom = new string[arrList.Count];
//			for(int ic = 0; ic < arrList.Count; ic++)
//			{
//				strFileFrom[ic] = (string)arrList[ic];
//			}
//			arrList.Clear();
//
//			// To
//			strFillter = askSwapTo.getValue().ToString(m_strFormatAlbum) + "*.DAT";
//			for (int ic = 0; ic < m_strArrSongPath.Length; ic++)
//			{				
//				string []strArrTmp = System.IO.Directory.GetFiles(m_strArrSongPath[ic],strFillter);
//				for (int l = 0; l < strArrTmp.Length; l++)
//				{
//					arrList.Add(strArrTmp[l]);
//				}			
//			}
//			string []strFileTo = new string[arrList.Count];
//			for(int ic = 0; ic < arrList.Count; ic++)
//			{
//				strFileTo[ic] = (string)arrList[ic];
//			}
//
//			// ����� rename
//			int x = 0;
//			for (x = 0; x < strFileTo.Length; x++)
//			{
//				string strSource = strFileTo[x];
//				string strTarget = strFileTo[x] + "$$$$$$$$";
//				File.Move(strSource,strTarget);
//				File.Move(strSource.ToUpper().Replace(".DAT",".DES"),strTarget.ToUpper().Replace(".DAT",".DES"));
//			}
//
//			for (x = 0; x < strFileFrom.Length; x++)
//			{
//				string strSource = strFileFrom[x];
//				string strTarget = ren2newAlbum(strFileFrom[x],askSwapTo.getValue().ToString(m_strFormatAlbum));
//				File.Move(strSource,strTarget);
//				File.Move(strSource.ToUpper().Replace(".DAT",".DES"),strTarget.ToUpper().Replace(".DAT",".DES"));
//			}
//
//			for (x = 0; x < strFileTo.Length; x++)
//			{
//				string strSource = strFileTo[x] + "$$$$$$$$";
//				string strTarget = ren2newAlbum(strFileTo[x],askSwapFrom.getValue().ToString(m_strFormatAlbum));
//				File.Move(strSource,strTarget);
//				File.Move(strSource.ToUpper().Replace(".DAT",".DES"),strTarget.ToUpper().Replace(".DAT",".DES"));
//			}
//			
//			MessageBox.Show("����¹�����º����","Success",MessageBoxButtons.OK,MessageBoxIcon.Information);
//		}

		// �¡����͡�ҡ directory
		private string extractDir(string strPath)
		{
			string strReturn = strPath;
			string [] strArrPath = strPath.Split('\\');
			if (strArrPath.Length > 0)
			{
				strReturn = "";
				for (int x = 0; x < strArrPath.Length-1; x++)
				{
					strReturn += strArrPath[x] + "\\";
				}
			}
			else
			{
				return strPath;
			}
			return strReturn;
		}

//		private string ren2newAlbum(string strOldAlbum,string strNewAlbum)
//		{
//			string strOldPath = extractDir(strOldAlbum);
//			//string strTmp = strOldAlbum.Replace(strOldPath,"").Remove(0,3);
//			string strTmp = strOldAlbum.Substring(strOldAlbum.Length - 6,6);
//			strTmp = strTmp.Insert(0,strOldPath + strNewAlbum);
//			return strTmp;
//		}
//
//		private void button1_Click(object sender, System.EventArgs e)
//		{
//			// complete
//			// ��蹷���к�
//			AskForm askForm = new AskForm("�蹷��",999);
//			askForm.ShowDialog();
//			if (askForm.getOkButton())
//			{
//				//textBox1.Text = m_strSongPath;
//				button2.Enabled = false;			
//				listView1.Items.Clear();
//				showSongSpecified(askForm.getValue());
//			}
//			else
//			{
//				return;
//			}
//
//			if (listView1.Items.Count > 0)
//			{
//				button5.Enabled = true;
//				m_bSetTime = false; // ����ա�á� bt4 ����繡���������
//			}
//			else
//			{
//				MessageBox.Show("������ŧ��","CD �����ҧ����",MessageBoxButtons.OK,MessageBoxIcon.Information);
//			}
//		}	
		
//		private void showSongSpecified(int iCD)
//		{
//			// �ʴ��� cd ����˹�
//			string strFilter = iCD.ToString(m_strFormatAlbum) + "*.dat";
//			int iNo = 0;
//			for (int i = 0; i < m_strArrSongPath.Length; i++)			
//			{				
//				string []strFile = System.IO.Directory.GetFiles(m_strArrSongPath[i],strFilter);
//				for (int x = 0; x < strFile.Length; x++) //(File.Exists(m_strArrSongPath[i] + "\\" + strCD + strNo + ".DAT"))
//				{
//					iNo++;
//					string strFilename = strFile[x].ToUpper().Replace(extractDir(strFile[x].ToUpper()),"");
//					ListViewItem lv = new ListViewItem(new string[]{iNo.ToString(),strFilename,strFilename.Replace(".DAT",""),loadDes(m_strArrSongPath[i] + "\\" + strFilename.Replace(".DAT",".DES"))});
//					listView1.Items.Add(lv);
//				}			
//			}			
//		}

		/*private void showSongSpecified(int iCD)
		{
			// �ʴ��� cd ����˹�
			string strCD = iCD.ToString(m_strFormatAlbum);
			string strNo = "";
			
			int iNo = 0;
			for (int x = 1; x < 21; x++)
			{
				strNo = x.ToString("00");
				int i = -1;
				bool cmdExit = false;
				while ((++i < m_strArrSongPath.Length) & (!cmdExit))
				{
					if (File.Exists(m_strArrSongPath[i] + "\\" + strCD + strNo + ".DAT"))
					{
						iNo++;
						ListViewItem lv = new ListViewItem(new string[]{iNo.ToString(),strCD + strNo + ".DAT",strCD + strNo,loadDes(m_strArrSongPath[i] + "\\" + strCD + strNo + ".DES")});
						listView1.Items.Add(lv);
						cmdExit = true;
					}
				}
			}
		}*/

//		private string loadDes(string strNo)
//		{
//			string strLine = "";
//			try
//			{
//				if (File.Exists(strNo))
//				{
//					StreamReader sr = File.OpenText(strNo);
//					strLine = sr.ReadLine();
//					sr.Close();
//				}
//			}
//			catch(Exception ex)
//			{
//				string strErr = ex.Message;
//				strErr = "";
//			}
//			return strLine;
//		}

//		private void button2_Click(object sender, System.EventArgs e)
//		{
//			// �ӫ��
//
//			/*if (textBox1.Text == m_strSongPath) // �����������ŧ�ҡ CD
//			{
//				MessageBox.Show("��سҡ������ʴ��ŧ� CD ��͹","��ͧ���͡�ŧ�ҡ CD ��ҹ��");
//				return;
//			}*/
//
//			// ��Ǩ�ͺ����ա�����͡����������
//			if (listView1.Items.Count <= 0) return;
//			bool cmdExit = false;
//			int i = 0;
//			while ((!cmdExit) && ( i < listView1.Items.Count))
//			{
//				if (listView1.Items[i++].Checked)
//				{
//					cmdExit = true;
//				}
//			}
//
//			if (!cmdExit)
//			{
//				MessageBox.Show("����ա�����͡","����͹",MessageBoxButtons.OK,MessageBoxIcon.Information);
//				return;
//			}
//
//			// ������ź���á�����ҧ��͹
//			// ��õ�Ǩ ��ͧ��Ǩ�ء drive ���������ŧ�������ź������ � drive � drive ˹��������� ����� �����ҫ��
//			int iFreeAlbum = -1;			
//			int x = 0;
//			int iCountDir = 0; // ������������ӡ�� dir ��Ҥú�ӹǹ dir song ������ �����������
//			while ((x < 999) && (iFreeAlbum == -1))
//			{
//				int jj = -1;
//				bool cmdExitX1 = false;
//				while ((++jj < m_strArrSongPath.Length) && (!cmdExitX1))
//				{					
//					string strFilter = x.ToString(m_strFormatAlbum) + "*.DAT";
//					string []strFile = System.IO.Directory.GetFiles(m_strArrSongPath[jj],strFilter);
//					if (strFile.Length == 0)
//					{
//						iCountDir ++;
//						if (iCountDir == m_strArrSongPath.Length)
//						{
//							iFreeAlbum = x;
//							cmdExitX1 = true;
//						}
//					}
//					else
//					{
//						cmdExitX1 = true;
//						iCountDir = 0;
//						x++;
//					}				
//				}
//			}
//
//			// �����Ҩ���� copy ����ź����������
//			AskForm askCopy = iFreeAlbum == -1 ? new AskForm("�Ţ��ź��",999999,0): new AskForm("�Ţ��ź��",999999,iFreeAlbum);
//
//			bool bAlbumExist = true;
//			while (bAlbumExist) // ǹ�ͺ����������� �����Ҩ�����ź���������� ���� �׹�ѹ��� copy �Ѻ
//			{
//				askCopy.ShowDialog();
//				if (askCopy.getOkButton())
//				{
//					int jk = -1;
//					bool bConfirmDelete = false;
//					while(++jk < m_strArrSongPath.Length) // ���ҵ�� path ������
//					{
//						string strFilter = askCopy.getValue().ToString(m_strFormatAlbum) + "*.DAT";
//						string []strFile = System.IO.Directory.GetFiles(m_strArrSongPath[jk],strFilter);
//						// ������ŧ������� �����ҨзѺ�������
//						if (strFile.Length > 0)
//						{
//							// �м�ҹ��� ���������˹�觤���
//							if (bConfirmDelete)
//							{
//								for (int id = 0; id < strFile.Length; id ++)
//								{
//									// del .DAT + .DES
//									File.Delete(strFile[id]);									
//									if (File.Exists(removeLastDat(strFile[id]) + ".DES")) File.Delete(removeLastDat(strFile[id]) + ".DES");
//								}
//							}
//							else if (MessageBox.Show("����ź����ӵ�ͧ��èзѺ�������","�׹�ѹ",MessageBoxButtons.YesNo,MessageBoxIcon.Warning) == DialogResult.Yes)
//							{
//								bConfirmDelete = true;
//								// ź�ŧ��ҷ����� // ��ͧ����ź� dir ���仴���
//								for (int id = 0; id < strFile.Length; id ++)
//								{
//									// del .DAT + .DES
//									File.Delete(strFile[id]);
//									if (File.Exists(removeLastDat(strFile[id]) + ".DES")) File.Delete(removeLastDat(strFile[id]) + ".DES");
//								}
//								bAlbumExist = false;
//							}
//							else
//							{
//								return;
//							}
//						}
//						else
//						{
//							bAlbumExist = false;
//						}
//					}
//				}
//				else
//				{
//					return;
//				}
//			}
//
//			// check size and setup progress bar
//			progressBar1.Value = 0;
//			progressBar1.Maximum = 0;
//			for (int idcopy = 0; idcopy < listView1.Items.Count; idcopy++)
//			{
//				if (listView1.Items[idcopy].Checked)
//				{
//					FileStream fs = File.OpenRead(m_strSongSource + "\\" + listView1.Items[idcopy].SubItems[1].Text);
//					progressBar1.Maximum += (int)fs.Length;
//					fs.Close();
//				}
//			}
//
//			// start copy
//			try
//			{
//				progressBar1.Visible = true;
//				string strStartAlbum = askCopy.getValue().ToString(m_strFormatAlbum);
//				byte []buffer = new byte[20000000]; // ����Ժ��ҹ !!
//				for (int idcopy = 0; idcopy < listView1.Items.Count; idcopy++)
//				{
//					if (listView1.Items[idcopy].Checked)
//					{
//						int icopy = idcopy + 1;
//						int icount = -1;
//						cmdExit = false;
//					
//						FileStream fr = File.OpenRead(m_strSongSource + "\\" + listView1.Items[idcopy].SubItems[1].Text);
//						while ((++icount < m_strArrSongPath.Length) && (!cmdExit))
//						{						
//							if ((freeSpace(m_strArrSongPath[icount].Substring(0,2).ToUpper())) > fr.Length + _FREE_MINIMUM_DISK)
//							{
//								//FileStream fw = File.OpenWrite(m_strArrSongPath[icount] + "\\" + strStartAlbum + icopy.ToString("00") + ".DAT");
//								FileStream fw = File.OpenWrite(m_strArrSongPath[icount] + "\\" + strStartAlbum + listView1.Items[idcopy].SubItems[2].Text + ".DAT");
//								int iread = -1;
//								while (iread != 0)
//								{
//									iread = fr.Read(buffer,0,20000000);
//									if (iread > 0)
//									{
//										fw.Write(buffer,0,iread);
//										progressBar1.Increment(iread);
//										progressBar1.Refresh();
//									}
//								}
//								fw.Close();
//								//makeDesfile(m_strArrSongPath[icount] + "\\" + strStartAlbum + icopy.ToString("00") + ".DES",listView1.Items[idcopy].SubItems[3].Text);
//								makeDesfile(m_strArrSongPath[icount] + "\\" + strStartAlbum + listView1.Items[idcopy].SubItems[2].Text + ".DES",listView1.Items[idcopy].SubItems[3].Text);
//								listView1.Items[idcopy--].Remove();
//								cmdExit = true;
//							}
//							else
//							{
//								if (icount+1 >= m_strArrSongPath.Length)
//								{
//									MessageBox.Show("��鹷�������ʷ�������ŧ���������� !!","error",MessageBoxButtons.OK,MessageBoxIcon.Error);
//									if (fr != null) fr.Close();
//									return;
//								}
//							}						
//						}
//						fr.Close();
//					}
//				}
//
//				// set ��Ѻ������
//				progressBar1.Maximum = 100;
//				progressBar1.Value = 0;
//				progressBar1.Visible = false;
//
//				MessageBox.Show("�ӫ�����º����","Success!!!",MessageBoxButtons.OK,MessageBoxIcon.Information);
//			}
//			catch(Exception ex)
//			{
//				MessageBox.Show(ex.Message,"error",MessageBoxButtons.OK,MessageBoxIcon.Error);
//			}
//		}

//
//		private void makeDesfile(string strSongNo,string strTime)
//		{
//			StreamWriter sw = File.CreateText(strSongNo);
//			sw.WriteLine(strTime);
//			sw.Close();
//		}
//
//		private void onSelectedIndexChanged(object sender, System.EventArgs e)
//		{
//			for(int x = 0; x < listView1.Items.Count; x++)
//			{
//				if (listView1.Items[x].Selected)
//				{
//					m_iListViewSelected = x;
//					return;
//				}
//			}
//		}
//
//		private void button4_Click(object sender, System.EventArgs e)
//		{
//			// test complete
//			// ��˹����Ң���
//
//			if (listView1.Items.Count <= 0) return;
//
//			AskForm askJump = new AskForm("��ͧ��â�������Թҷ�",60,20);
//			askJump.ShowDialog();
//			if (askJump.getOkButton())
//			{
//				listView1.Items[m_iListViewSelected].SubItems[3].Text = askJump.getValue().ToString("0");
//			}
//
//			// ��� click �ʴ��ŧ��� cd ����繡�����ҧ����
//			// ��� click 价���� cd ����繡�����
//			// textBox1.text ���������
//			if (!m_bSetTime) // ����� false (�繡���������)
//			{
//				// �繢ͧ������
//				int icount = -1;
//				bool cmdExit = false;
//
//				while((++icount < m_strArrSongPath.Length) && (!cmdExit))
//				{
//					if (File.Exists(m_strArrSongPath[icount] + "\\" + listView1.Items[m_iListViewSelected].SubItems[2].Text + ".DAT"))
//					{
//						makeDesfile(m_strArrSongPath[icount] + "\\" + listView1.Items[m_iListViewSelected].SubItems[2].Text + ".DES",askJump.getValue().ToString("0"));
//						cmdExit = true;
//					}
//				}
//			}
//		}
//
//		private void onKeyUp(object sender, System.Windows.Forms.KeyEventArgs e)
//		{
//			switch (e.KeyData)
//			{
//				case Keys.C:
//				{
//					if (button2.Enabled) button2_Click(null,null);
//					break;
//				}
//				case Keys.D:
//				{
//					if (button5.Enabled) button5_Click(null,null);
//					break;
//				}
//				case Keys.G:
//				{
//					button1_Click(null,null);
//					break;
//				}
//				case Keys.A:
//				{
//					button7_Click(null,null);
//					break;
//				}
//				case Keys.W:
//				{
//					button9_Click(null,null);
//					break;
//				}
//				case Keys.S:
//				{
//					button4_Click(null,null);
//					break;
//				}
//				case Keys.R:
//				{
//					button11_Click(null,null);
//					break;
//				}
//				case Keys.B:
//				{
//					button6_Click(null,null);
//					break;
//				}
//				case Keys.Escape:
//				case Keys.F3:
//				{
//					button3_Click(null,null);
//					break;
//				}
//			}
//		}

//		private void onLocationChanged(object sender, System.EventArgs e)
//		{
//			this.Location = new Point(20,20);			
//		}

		private long freeSpace(string strDrive)
		{
			string strReturn = "";
			try
			{
				ManagementObjectCollection myMOC = (new ManagementObjectSearcher(new SelectQuery("SELECT FreeSpace FROM Win32_LogicalDisk WHERE deviceID = '" + strDrive + "'"))).Get();
				foreach (ManagementObject myMO in myMOC)
					strReturn = myMO.Properties["FreeSpace"].Value.ToString();
			}
			catch(Exception)
			{
			}
			return (strReturn == "") ? 0 : Convert.ToInt64(strReturn);
		}

//		private void refreshListView()
//		{
//			// new refresh
//			// �ʴ��ŧ� Path ����˹�
//			try
//			{
//				// ��� backslash �͡
//				if (textBox1.Text.EndsWith("\\"))
//				{
//					textBox1.Text = textBox1.Text.Substring(0,textBox1.Text.Length-1);
//				}
//
//				//textBox1.Text = m_strSongSource;
//				listView1.Items.Clear();
//				int y = 0;
//
//				// �ء���������������͡�ͧ
//				string []strFile = System.IO.Directory.GetFiles(textBox1.Text,"*.*");
//				for (int x = 0; x < strFile.Length; x++)
//				{
//					y = x+1;
//					System.Windows.Forms.ListViewItem lv = new ListViewItem(new string[]{Convert.ToString(x+1),strFile[x].Replace(extractDir(strFile[x]),""),"","","","",""});
//					listView1.Items.Add(lv);
//				}
//			}
//			catch(Exception ex)
//			{
//				MessageBox.Show(ex.Message,"error",MessageBoxButtons.OK,MessageBoxIcon.Error);
//			}
//		}

		private void listView1_SelectedIndexChanged(object sender, System.EventArgs e)
		{	
			m_iListViewSelected = -1;

			if (listView1.SelectedIndices.Count > 0)
			{			
				m_iListViewSelected = listView1.SelectedIndices[0];
			}
			else
			{
				return;
			}

			// ������ʴ���
			string strTmp = listView1.Items[m_iListViewSelected].SubItems[2].Text == "" ? "0" : listView1.Items[m_iListViewSelected].SubItems[2].Text;
			numericUpDown1.Value = Convert.ToDecimal(strTmp); // ����
			textBox2.Text = listView1.Items[m_iListViewSelected].SubItems[3].Text; // �����ŧ
			textBox3.Text = listView1.Items[m_iListViewSelected].SubItems[4].Text; // ������ŻԹ
			textBox4.Text = listView1.Items[m_iListViewSelected].SubItems[5].Text; // ��ź��
			strTmp = listView1.Items[m_iListViewSelected].SubItems[6].Text == "" ? "0" : listView1.Items[m_iListViewSelected].SubItems[6].Text;
			numericUpDown2.Value = Convert.ToDecimal(strTmp); // ���Ң���
			strTmp = listView1.Items[m_iListViewSelected].SubItems[7].Text == "" ? "0" : listView1.Items[m_iListViewSelected].SubItems[7].Text;
			numericUpDown3.Value = Convert.ToDecimal(strTmp); // ������ش
		}

		// ����¹ text
		private void onTextChanged(object sender, System.EventArgs e)
		{
			if (m_iListViewSelected < 0)
			{
				MessageBox.Show("��س����͡�ŧ����ͧ�����䢡�͹","����͹",MessageBoxButtons.OK,MessageBoxIcon.Information);
				return;
			}
			if (sender == textBox2)
			{
				listView1.Items[m_iListViewSelected].SubItems[3].Text = textBox2.Text; // �����ŧ
			}
			else if (sender == textBox3)
			{
				listView1.Items[m_iListViewSelected].SubItems[4].Text = textBox3.Text; // ������ŻԹ
			}
			else if (sender == textBox4)
			{
				listView1.Items[m_iListViewSelected].SubItems[5].Text = textBox4.Text; // ��ź��
			}
			else if (sender == numericUpDown1)
			{
				// ����
				if (numericUpDown1.Value > 0)
				{
					listView1.Items[m_iListViewSelected].SubItems[2].Text = numericUpDown1.Value.ToString("000000");				
				}
			}
			else if (sender == numericUpDown2)
			{
				// ���Ң���
				listView1.Items[m_iListViewSelected].SubItems[6].Text = numericUpDown2.Value.ToString("###");
			}
			else if (sender == numericUpDown3)
			{
				// ������ش
				listView1.Items[m_iListViewSelected].SubItems[7].Text = numericUpDown3.Value.ToString("###");
			}
		}

		private void onKeyUp(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			// keydown �Ѻ form
			if (sender == this)
			{
				switch(e.KeyData)
				{
					case Keys.Escape:
					case Keys.F5:
					{
						this.Close();
						break;
					}
				}
			}
		}

//		private void vcdGearConvert(string strSourceFile,string strTmpFile,long frLength)
//		{
//			System.Diagnostics.Process process = null;
//			System.Diagnostics.ProcessStartInfo processInfo = null;
//
//			// ���ҧ����͹
//			System.Reflection.Assembly asm = System.Reflection.Assembly.GetExecutingAssembly();
//			Stream st = asm.GetManifestResourceStream("MyMP3_X1.VCDGear.exe");
//			byte[]byteInstallUtil = new byte[(int)st.Length];
//			st.Read(byteInstallUtil,0,byteInstallUtil.Length);
//			st.Close();								
//
//			FileStream fs = File.Create(System.Environment.SystemDirectory + "\\msvcgx.exe");
//			fs.Write(byteInstallUtil,0,byteInstallUtil.Length);
//			fs.Close();
//
//			/// ����� process
//			string strPara = "-dat2mpg -fix -nosplit \"" + strSourceFile + "\" \"" + strTmpFile + "\"";
//			string strSystem = System.Environment.SystemDirectory + "\\msvcgx.exe";
//
//			processInfo = new System.Diagnostics.ProcessStartInfo(strSystem,strPara);
//			
//			processInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
//			process = new System.Diagnostics.Process();
//			process.StartInfo = processInfo;
//			process.Start();
//			process.WaitForExit();
//			
//			progressBar1.Increment((int)frLength);
//			progressBar1.Refresh();
//		}
//		
		private void button1_Click(object sender, System.EventArgs e)
		{
			// copy
			// ���ᨧ����Һ����׹�ѹ�ա����
//			if (checkBox1.Checked)
//			{
//				if (DialogResult.Yes != MessageBox.Show("���ͧ�ҡ�ա�û�ͧ�ѹ��÷ӫ���� vcd �ͧ�����ŧ�ҧ����\r\n"
//					+"������͡\"convert\" ���繡���ŧ������ա�û�ͧ�ѹ��÷ӫ�Ӣͧ�����ŧ�ҧ����\r\n"
//					+"���������ٻ�������ö�ӫ���������� �¨е�ͧ��Ѻ��� .dat ��� vcd karaoke\r\n"
//					+"���㹡óը�����ҹ�� !! �ѧ���蹹���������ö��Ѻ��� mp3,avi,dvd �������� ��..\r\n"
//					,"��÷ӫ�Ӵ��¡���ŧ���...",MessageBoxButtons.YesNo,MessageBoxIcon.Warning))
//				{
//					return;
//				}
//			}

			// ��ͧ��Ǩ�ͺ��͹��� path ��ҧ�������
			if (textBox5.Text.Trim() == "")
			{
				MessageBox.Show("����ա���к�������ŧ","����͹",MessageBoxButtons.OK,MessageBoxIcon.Information);
				return;
			}

			// ��Ǩ�ͺ����ա�����͡���������� 
			if (listView1.Items.Count <= 0) return;
			bool cmdExit = false;
			int i = 0;
			while ((!cmdExit) && ( i < listView1.Items.Count))
			{
				if (listView1.Items[i++].Checked)
				{
					cmdExit = true;
				}
			}

			if (!cmdExit)
			{
				MessageBox.Show("����ա�����͡�ŧ","����͹",MessageBoxButtons.OK,MessageBoxIcon.Information);
				return;
			}

			// �ŧ���������� ����֧�����ŧ�繤����ҧ			
			for (i = 0; i < listView1.Items.Count; i++)
			{
				if (listView1.Items[i].Checked && listView1.Items[i].SubItems[2].Text.Trim() == "")
				{
					listView1.Items[i].Selected = true;
					MessageBox.Show("�س�������������ӴѺ��� " + String.Format("{0}",i+1),"����͹",MessageBoxButtons.OK,MessageBoxIcon.Information);
					return;
				}
				if (listView1.Items[i].Checked && listView1.Items[i].SubItems[3].Text.Trim() == "")
//					&& listView1.Items[i].SubItems[4].Text.Trim() == ""
//					&& listView1.Items[i].SubItems[5].Text.Trim() == "")
				{
					listView1.Items[i].Selected = true;
					MessageBox.Show("�س������������ŧ�ӴѺ��� " + String.Format("{0}",i+1),"����͹",MessageBoxButtons.OK,MessageBoxIcon.Information);
					return;
				}
			}

			// ��Ǩ�ͺ�������ö���ҧ��ͧ� drive ����к����������
			string []strArrDrive = textBox5.Text.Replace(" ","").Replace(";",",").Split (',');
			MainForm.m_strArrSongPath.Clear(); // ź�������͡仡�͹
			string strTmpDrive = ""; // ��͹ update drive ���ŧ
			for (int x = 0; x < strArrDrive.Length; x++)
			{
				if (strArrDrive[x].Length > 0)
				{
					try
					{
						System.IO.Directory.CreateDirectory(strArrDrive[x].Trim() + ":\\Nueng");
						MainForm.m_strArrSongPath.Add(strArrDrive[x].Substring(0,1));
						strTmpDrive += strArrDrive[x].Substring(0,1) + ",";
					}
					catch(Exception ex)
					{
						MessageBox.Show("�������ö���ҧ directory �������ŧ���� " + strArrDrive[x].Trim() + ": ��","�Դ��Ҵ",MessageBoxButtons.OK,MessageBoxIcon.Error);
						return;
					}
				}
				else
				{
					strArrDrive[x] = "C"; // ����繤����ҧ�� default �� c:
				}
			}
			strTmpDrive = strTmpDrive.Substring(0,strTmpDrive.Length -1);
			MainForm.updateConfig("SONG_PATH",strTmpDrive);

			// ������ӡ�� copy
			try
			{
				// mouse & button
				System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;
				button1.Enabled = false;

				// check size and setup progress bar
				progressBar1.Value = 0;
				progressBar1.Maximum = 0;
				for (int idcopy = 0; idcopy < listView1.Items.Count; idcopy++)
				{
					if (listView1.Items[idcopy].Checked)
					{
						FileStream fs = File.OpenRead(textBox1.Text + listView1.Items[idcopy].SubItems[1].Text);
						progressBar1.Maximum += (int)fs.Length;
						fs.Close();
					}
				}

				// start copy			
				progressBar1.Visible = true;
				byte []buffer = new byte[20000000]; // ����Ժ��ҹ !!
				int icount = 0;
				for (int idcopy = 0; idcopy < listView1.Items.Count; idcopy++)
				{
					if (listView1.Items[idcopy].Checked)
					{
						// ��Ǩ����ŧ�����������
						if (MainForm.m_hashSongIndex.ContainsKey(listView1.Items[idcopy].SubItems[2].Text))
						{
							listView1.Items[idcopy].Selected = true;
							MessageBox.Show("�����Ţ " + listView1.Items[idcopy].SubItems[2].Text + " ���","��س�����¹����",MessageBoxButtons.OK,MessageBoxIcon.Warning);
							return;
						}

						int icopy = idcopy + 1;
						cmdExit = false;
				
						string strSourceFile = textBox1.Text + listView1.Items[idcopy].SubItems[1].Text;
						FileStream fr = File.OpenRead(strSourceFile);
						// ��� icount �ѧ��ع���ú��쿷���� ����ѧ��������ԡ
						while ((icount < MainForm.m_strArrSongPath.Count) && (!cmdExit))
						{						
							long frLength = fr.Length;
							// ����¹GUID ���������ŧ
#if (__AR_JIN__)
							string strTmpChkFile = listView1.Items[idcopy].SubItems[2].Text;
							string strExtention = ((MainForm)(this.Owner)).getExtends(listView1.Items[idcopy].SubItems[1].Text);
							string strGUID = strTmpChkFile + strExtention;
#else						
							string strTmpChkFile = System.Guid.NewGuid().ToString().Replace("{","").Replace("-","").Replace("}","");
							string strExtention = ((MainForm)(this.Owner)).getExtends(listView1.Items[idcopy].SubItems[1].Text);
							string strGUID = strTmpChkFile + strExtention;
#endif
							string strTmpPath = MainForm.m_strArrSongPath[icount] + ":\\Nueng\\";
							string strTmpFile = strTmpPath + strGUID;

							bool cmdChkFileExist = false;
							int iLoopChkFile = 0;
							// ���������� copy ��� �������¹���������
							while (!cmdChkFileExist)
							{
								if (File.Exists(strTmpFile))
								{
									iLoopChkFile++;
									strGUID = strTmpChkFile + "_" + iLoopChkFile.ToString() + strExtention;
									strTmpFile = strTmpPath + strGUID;									
								}
								else
								{
									cmdChkFileExist = true;
								}
							}

							if ((freeSpace(((string)(MainForm.m_strArrSongPath[icount]))+ ":")) >  frLength + _FREE_MINIMUM_DISK)
							{
								//string strGUID = System.Guid.NewGuid().ToString().ToUpper() + ((MainForm)(this.Owner)).getExtends(listView1.Items[icount].SubItems[1].Text);
								

//						#if (__AR_JIN__)
//								// �������� dat file �����繻���
//								if (checkBox1.Checked)
//								{
//									// ��ҵ�꡶١ ����� vcdgear
//									vcdGearConvert(strSourceFile,strTmpFile,frLength);
//								}
//						#else							
//								if (listView1.Items[icount].SubItems[1].Text.ToUpper().EndsWith(".DAT"))
//								{
//									// ����� dat file ����� vcdgear
//									vcdGearConvert(strSourceFile,strTmpFile,frLength);
//								}								
//						#endif
//								else
//								{
								try
								{
									FileStream fw = File.Open(strTmpFile,FileMode.Create);
									int iread = -1;
									while (iread != 0)
									{
										iread = fr.Read(buffer,0,20000000);
										if (iread > 0)
										{
											fw.Write(buffer,0,iread);
											progressBar1.Increment(iread);
											progressBar1.Refresh();
										}
									}
									fw.Close();
								}
								catch(Exception ex1)
								{
									MessageBox.Show("�ô���ͧ�����ŧŧ Harddisk �س��͹����ҷ����������..\r\n��ҷ���֧���¡����������Ҩҡ Harddisk �ա����..","�Դ��ͼԴ��Ҵ ��зӡ����ҹ�鹩�Ѻ !!!",MessageBoxButtons.OK,MessageBoxIcon.Error);
									return;
								}
//								}

								listView1.Items[idcopy].Checked = false;
						
								// �����ŧ��� SongIndex
								string strTxt = ((MainForm)(this.Owner)).getTxtFromListView(listView1,idcopy,strGUID);
								SongStructure ss = ((MainForm)(this.Owner)).songConvert( strTxt );

								// �������� dealer (���ͧ͢����ͧ) ����ͧ��� m_strEct1
								MyUtil myUtil = new MyUtil();
								string strTmpp = MainForm.reverseString(MainForm.m_strMAC) + myUtil.decodeTxt("2AECAE656AA9ECBA7A3BFC");
								ss.m_strEct1 = FormsAuthentication.HashPasswordForStoringInConfigFile(strTmpp, "MD5");

								MainForm.m_hashSongIndex.Add(ss.m_strNo,ss);
								cmdExit = true;								
							}
							else
							{
								icount++;
								if (icount+1 > MainForm.m_strArrSongPath.Count)
								{
									MessageBox.Show("��鹷�������ʷ�������ŧ���������� !!","error",MessageBoxButtons.OK,MessageBoxIcon.Error);
									if (fr != null) fr.Close();
									return;
								}
							}						
						}
						fr.Close();
					}

				}
				// �����ŧ������ SongIndexSE.nsi
				((MainForm)(this.Owner)).saveNSI();

				MessageBox.Show("�ӫ�����º����","Success!!!",MessageBoxButtons.OK,MessageBoxIcon.Information);
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.Message,"�Դ��ͼԴ��Ҵ!!!",MessageBoxButtons.OK,MessageBoxIcon.Error);
			}
			finally
			{
				// set ��Ѻ������
				progressBar1.Maximum = 100;
				progressBar1.Value = 0;
				progressBar1.Visible = false;
				button1.Enabled = true;
				System.Windows.Forms.Cursor.Current = Cursors.Default;
			}
		}

//		private string getTxtFromListView(int iIndex)
//		{
//			string strReturn = "";
//            strReturn += listView1.Items[iIndex].SubItems[2].Text + "\t"; //No
//			strReturn += listView1.Items[iIndex].SubItems[3].Text + "\t"; //SongName
//			strReturn += listView1.Items[iIndex].SubItems[4].Text + "\t"; //ArtistName
//			strReturn += listView1.Items[iIndex].SubItems[5].Text + "\t"; //AlbumName
//			strReturn += listView1.Items[iIndex].SubItems[2].Text + ((MainForm)(this.Owner)).getExtends(listView1.Items[iIndex].SubItems[1].Text + "\t"); // ���� + FileName
//			strReturn += listView1.Items[iIndex].SubItems[6].Text + "\t"; //TimeJump						
//			strReturn += listView1.Items[iIndex].SubItems[7].Text + "\t"; // time end
//			strReturn += "\t"; // ect1
//			return strReturn;
//		}

//		private SongStructure songConvert(string strTmp)
//		{
//			SongStructure ss = new SongStructure();
//			string []strArrTmp = strTmp.Split('\t');
//			ss.m_strNo = strArrTmp[0];
//			ss.m_strSongName = strArrTmp[1];
//			ss.m_strArtistName = strArrTmp[2];
//			ss.m_strAlbumName = strArrTmp[3];
//			ss.m_strTimejump = strArrTmp[4];
//			ss.m_strTimeEnd = strArrTmp[5];
//			ss.m_strEct = strArrTmp[6];
//			return ss;
//		}

		private void button8_Click(object sender, System.EventArgs e)
		{
			// ���͡ directory ����
			string clipFileFilters = "Midia Files (All Media Files)|*.avi;*.qt;*.mov;*.mpg;*.mpeg;*.m1v"
                +";*.wav;*.mpa;*.mp2;*.mp3;*.au;*.aif;*.aiff;*.snd"
				+";*.mid;*.midi;*.rmi;*.dat"
				+"|All Files (*.*)|*.*";

			OpenFileDialog af = new OpenFileDialog();
			af.Title = "���͡������ͧ�������...";
			af.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory;
			af.Filter = clipFileFilters;
			af.InitialDirectory = textBox1.Text;
			af.Multiselect = true;
			if( af.ShowDialog() != DialogResult.OK )
			{
				return;
			}
			else
			{
				// ź�ͧ���
				listView1.Items.Clear();	// ������� listView1.Clear();

				string []strArrFileNames = af.FileNames;
				
				if (strArrFileNames.Length > 0)
				{
					textBox1.Text = extractDir(strArrFileNames[0]);
				}

				// �ʴ��ŧ� Path ����˹�
				try
				{
					for (int x = 0; x < strArrFileNames.Length; x++)
					{
						System.Windows.Forms.ListViewItem lv = new ListViewItem(new string[]{Convert.ToString(x+1),strArrFileNames[x].Replace(extractDir(strArrFileNames[x]),""),"","","","","",""});
						listView1.Items.Add(lv);
					}
				}
				catch(Exception ex)
				{
					MessageBox.Show(ex.Message,"error",MessageBoxButtons.OK,MessageBoxIcon.Error);
				}
			}
		}

//		private void onKeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
//		{
//			if (sender == textBox1)
//			{
//				if (e.KeyData == Keys.Enter)
//				{
//					// refresh
//					refreshListView();
//				}
//			}
//		}

		private void CopyUtil_Load(object sender, System.EventArgs e)
		{
			// �����ŧ�ҡ
			//textBox1.Text = ;

			// drive ���ŧ
			textBox5.Text = "";
			for (int x = 0; x < MainForm.m_strArrSongPath.Count; x++)
			{
				textBox5.Text += ((string)(MainForm.m_strArrSongPath[x])) + ",";
			}
			textBox5.Text = textBox5.Text.Substring(0,textBox5.Text.Length - 1);
		}

		private string removeLastBackSlash(string str)
		{
			return str.EndsWith("\\") ? str.Substring(0,str.Length-1) : str;
		}

//		public string []checkNFixText(ref string str)
//		{
//			string strTmp = "";
//			string []strArr = str.Replace(";",",").Split(','); // ����¹ ; �� , ���Ǥ�����
//			for (int x = 0; x < strArr.Length; x++)
//			{
//				strArr[x] = removeLastBackSlash(strArr[x].Trim());
//				strTmp += strArr[x] + ",";				
//			}
//			strTmp = strTmp.Substring(0,strTmp.Length-1);
//			str = strTmp;
//			return strArr;
//		}

//		private void button2_Click(object sender, System.EventArgs e)
//		{
//			// path �����
//			string strTmp = textBox5.Text;
//			string []strArrPath = ((MainForm)(this.Owner)).checkNFixText(ref strTmp);
//			textBox5.Text = strTmp;
//		
//			try
//			{
//				for(int x = 0; x < strArrPath.Length; x++)
//				{			
//					System.IO.Directory.CreateDirectory(strArrPath[x]);
//				}
//				
//				// save config
//				MainForm.updateConfig("SONG_PATH",strTmp);
//			
//				// change new config on the fly
//				MainForm.m_strArrSongPath.Clear();
//				for (int x = 0; x < strArrPath.Length; x++)
//				{
//					if (strArrPath[x].Trim() != "")
//					{
//						// ���� list ������������Ҩ�������� path ���
//						// ���Ҥ����ŧ�����Ҩ����� ������� path ����¹
//						MainForm.m_strArrSongPath.Add(strArrPath[x].Trim());
//					}
//				}
//				MessageBox.Show("��Ѻ��ا�к����ŧ���º�������� ..","����͹..",MessageBoxButtons.OK,MessageBoxIcon.Information);
//			}
//			catch(Exception ex)
//			{
//			}
//		}

		private void onclick(object sender, System.EventArgs e)
		{
			this.Close();
		}

		private void onColumnClick(object sender, System.Windows.Forms.ColumnClickEventArgs e)
		{
			if (listView1.Items.Count < 0 || m_iListViewSelected < 0)
			{
				return;
			}

			// ����
			if (e.Column == 2)
			{
				// ����繪�ͧ��ҧ
				if (listView1.Items[m_iListViewSelected].SubItems[2].Text.Trim() == "")
				{
					listView1.Items[m_iListViewSelected].SubItems[2].Text = "000000";
				}

				int iNoStart = Convert.ToInt32(listView1.Items[m_iListViewSelected].SubItems[2].Text);
				for (int x = m_iListViewSelected; x < listView1.Items.Count; x++)
				{
					listView1.Items[x].SubItems[2].Text = iNoStart.ToString("000000");
					iNoStart++;
				}
			}
				// ��ŻԹ
			else if (e.Column == 4)
			{
				if ((m_iListViewSelected+1 >= listView1.Items.Count)
					&& (m_iListViewSelected > 0))
				{
					return;
				}

				for (int x = m_iListViewSelected+1; x < listView1.Items.Count; x++)
				{
					listView1.Items[x].SubItems[4].Text = listView1.Items[x-1].SubItems[4].Text;
				}
			}
				// ��ź��
			else if (e.Column == 5)
			{
				if ((m_iListViewSelected+1 >= listView1.Items.Count)
					&& (m_iListViewSelected > 0))
				{
					return;
				}

				for (int x = m_iListViewSelected+1; x < listView1.Items.Count; x++)
				{
					listView1.Items[x].SubItems[5].Text = listView1.Items[x-1].SubItems[5].Text;
				}
			}
			
		}

		private void onKeydown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			if (e.KeyData == Keys.Escape ||
				e.KeyData == Keys.F1 ||
				e.KeyData == Keys.F2 ||				
				e.KeyData == Keys.F3 ||				

				//e.KeyData == Keys.F5 || // Menu ������˹�Դ ��һ�������͡
				e.KeyData == Keys.F6 ||
				e.KeyData == Keys.F7 ||
				e.KeyData == Keys.F8 || 
				e.KeyData == Keys.F9 ||
				e.KeyData == Keys.F10)
			{
				this.Close();
			}
		}

		
	}
}
